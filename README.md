# GreenLab Plugin

The GreenLab Plugin adds a support interface for Greenlab projects in GroIMP. The interface includes GUI elements to create an interactive workbench with Greenlab tools.

The plugin also provides a support for the file type:

- ".smb" (AMAPSymbol)
