/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.grogra.greenlab.conf.GreenLabAtributes;
import de.grogra.greenlab.conf.GreenLabPropertyFileReader;
import de.grogra.greenlab.ui.elements.GreenLabBooleanCheckBox;
import de.grogra.greenlab.ui.elements.GreenLabJLabel;
import de.grogra.greenlab.ui.elements.GreenLabNumberField;

/**
 * The Environment Panel
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabEnvironmentPanel extends JPanel {

	private static final long serialVersionUID = -2073432007086110708L;
	private GreenLabNumberField soilText[] = new GreenLabNumberField[GreenLabAtributes.ENVIRONMENT_SOIL_NUM];
	private JTextField tempText[] = new JTextField[GreenLabAtributes.ENVIRONMENT_TEMP_NUM];
	private GreenLabNumberField lightText[] = new GreenLabNumberField[GreenLabAtributes.ENVIRONMENT_LIGHT_NUM];
	private GreenLabBooleanCheckBox soilFlag, tempFlag, lightFlag, sumFlag;
	private JPanel soilPanel, tempPanel, lightPanel;

	GreenLabEnvironmentPanel() {
		soilPanel = new JPanel(new BorderLayout());
		JPanel soilPanelLeft = new JPanel(new GridLayout(
				GreenLabAtributes.ENVIRONMENT_SOIL_NUM - 1, 1));
		JPanel soilPanelRight = new JPanel(new GridLayout(
				GreenLabAtributes.ENVIRONMENT_SOIL_NUM - 1, 1));
		for (int i = 0; i < GreenLabAtributes.ENVIRONMENT_SOIL_NUM - 1; i++) {
			soilText[i] = new GreenLabNumberField(soilName(i + 2).replaceAll(
					"[\\(\\)]", "_"));
			final int index = i;
			final GreenLabNumberField indexText = soilText[i];
			soilText[i].getDocument().addDocumentListener(
					new DocumentListener() {

						public void actionPerformed() {
							GreenLabPropertyFileReader.get().setProperty(
									soilName(index + 2), indexText.getText());
						}

						@Override
						public void changedUpdate(DocumentEvent e) {
							actionPerformed();
						}

						@Override
						public void insertUpdate(DocumentEvent e) {
							actionPerformed();
						}

						@Override
						public void removeUpdate(DocumentEvent e) {
							actionPerformed();
						}
					});
			soilPanelLeft.add(new GreenLabJLabel((soilName(i + 2).replaceAll(
					"[\\(\\)]", "_"))));
			soilPanelRight.add(soilText[i]);
		}
		soilPanel.add(soilPanelLeft, BorderLayout.WEST);
		soilPanel.add(soilPanelRight, BorderLayout.EAST);

		tempPanel = new JPanel(new BorderLayout());
		JPanel tempPanelLeft = new JPanel(new GridLayout(
				GreenLabAtributes.ENVIRONMENT_TEMP_NUM - 2 - 2, 1));
		JPanel tempPanelRight = new JPanel(new GridLayout(
				GreenLabAtributes.ENVIRONMENT_TEMP_NUM - 2 - 2, 1));
		for (int i = 1; i < GreenLabAtributes.ENVIRONMENT_TEMP_NUM - 1; i++) {
			tempText[i] = new GreenLabNumberField(tempName(i + 2).replaceAll(
					"[\\(\\)]", "_"));
			final int index = i;
			final JTextField indexText = tempText[i];
			tempText[i].getDocument().addDocumentListener(
					new DocumentListener() {

						public void actionPerformed() {
							GreenLabPropertyFileReader.get().setProperty(
									tempName(index + 2), indexText.getText());
						}

						@Override
						public void changedUpdate(DocumentEvent e) {
							actionPerformed();
						}

						@Override
						public void insertUpdate(DocumentEvent e) {
							actionPerformed();
						}

						@Override
						public void removeUpdate(DocumentEvent e) {
							actionPerformed();
						}
					});
			// remove the two items but not delete
			if (i != 3 && i != 4) {
				tempPanelLeft.add(new GreenLabJLabel((tempName(i + 2)
						.replaceAll("[\\(\\)]", "_"))));
				tempPanelRight.add(tempText[i]);
			}
		}
		tempPanel.add(tempPanelLeft, BorderLayout.WEST);
		tempPanel.add(tempPanelRight, BorderLayout.EAST);

		lightPanel = new JPanel(new BorderLayout());
		JPanel lightPanelLeft = new JPanel(new GridLayout(
				GreenLabAtributes.ENVIRONMENT_LIGHT_NUM - 1, 1));
		JPanel lightPanelRight = new JPanel(new GridLayout(
				GreenLabAtributes.ENVIRONMENT_LIGHT_NUM - 1, 1));
		for (int i = 0; i < GreenLabAtributes.ENVIRONMENT_LIGHT_NUM - 1; i++) {
			lightText[i] = new GreenLabNumberField(lightName(i + 2).replaceAll(
					"[\\(\\)]", "_"));
			final int index = i;
			final GreenLabNumberField indexText = lightText[i];
			lightText[i].getDocument().addDocumentListener(
					new DocumentListener() {

						public void actionPerformed() {

							GreenLabPropertyFileReader.get().setProperty(
									lightName(index + 2), indexText.getText());
						}

						@Override
						public void changedUpdate(DocumentEvent e) {
							actionPerformed();
						}

						@Override
						public void insertUpdate(DocumentEvent e) {
							actionPerformed();
						}

						@Override
						public void removeUpdate(DocumentEvent e) {
							actionPerformed();
						}
					});
			lightPanelLeft.add(new GreenLabJLabel((lightName(i + 2).replaceAll(
					"[\\(\\)]", "_"))));
			lightPanelRight.add(lightText[i]);
		}
		lightPanel.add(lightPanelLeft, BorderLayout.WEST);
		lightPanel.add(lightPanelRight, BorderLayout.EAST);

		soilFlag = new GreenLabBooleanCheckBox(soilName(1).replaceAll(
				"[\\(\\)]", "_"));
		soilFlag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean soilBool = soilFlag.getBoolean();
				for (int i = 0; i < GreenLabAtributes.ENVIRONMENT_SOIL_NUM - 1; i++) {
					soilText[i].setEditable(soilBool);
				}
				GreenLabPropertyFileReader.get().setProperty(soilName(1),
						soilFlag.getTextValue());
			}
		});
		soilFlag.setTextValue(GreenLabPropertyFileReader.get().getProperty(
				soilName(1)));
		JPanel flagSoilPanel = new JPanel(new BorderLayout());
		JPanel flagSoil = new JPanel();
		flagSoil.add(soilFlag);
		flagSoil.add(new GreenLabJLabel((soilName(1)
				.replaceAll("[\\(\\)]", "_"))));
		flagSoilPanel.add(flagSoil, BorderLayout.WEST);
		Box soilWholePanel = Box.createVerticalBox();
		soilWholePanel.add(flagSoilPanel);
		soilWholePanel.add(soilPanel);
		soilWholePanel.setBorder(new TitledBorder(GreenLabGUI
				.getString(soilName(0).replaceAll("[\\(\\)]", "_"))));

		tempFlag = new GreenLabBooleanCheckBox(tempName(1).replaceAll(
				"[\\(\\)]", "_"));
		sumFlag = new GreenLabBooleanCheckBox(tempName(2).replaceAll(
				"[\\(\\)]", "_"));
		tempFlag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean tempBool = tempFlag.getBoolean();
				for (int i = 1; i < GreenLabAtributes.ENVIRONMENT_TEMP_NUM - 1; i++) {
					tempText[i].setEditable(tempBool);
				}
				sumFlag.setEnabled(tempBool);
				boolean sumBool = sumFlag.getBoolean();
				if (sumFlag.isEnabled())
					for (int i = 3; i <= 4; i++) {
						tempText[i].setEditable(sumBool);
					}
				GreenLabPropertyFileReader.get().setProperty(tempName(1),
						tempFlag.getTextValue());
			}
		});
		sumFlag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean sumBool = sumFlag.getBoolean();
				for (int i = 3; i <= 4; i++) {
					tempText[i].setEditable(sumBool);
				}
				GreenLabPropertyFileReader.get().setProperty(tempName(2),
						tempFlag.getTextValue());
			}
		});
		tempFlag.setTextValue(GreenLabPropertyFileReader.get().getProperty(
				tempName(1)));
		sumFlag.setTextValue(GreenLabPropertyFileReader.get().getProperty(
				tempName(2)));
		JPanel flagTempPanel = new JPanel(new BorderLayout());
		JPanel flagTemp = new JPanel();
		flagTemp.add(tempFlag);
		flagTemp.add(new GreenLabJLabel((tempName(1)
				.replaceAll("[\\(\\)]", "_"))));
		flagTempPanel.add(flagTemp, BorderLayout.WEST);

		JPanel flagSumPanel = new JPanel(new BorderLayout());
		JPanel flagSum = new JPanel();
		flagSum.add(sumFlag);
		flagSum.add(new GreenLabJLabel(
				(tempName(2).replaceAll("[\\(\\)]", "_"))));
		flagSumPanel.add(flagSum, BorderLayout.WEST);

		Box tempWholePanel = Box.createVerticalBox();
		tempWholePanel.add(flagTempPanel);
		// tempWholePanel.add(flagSumPanel);
		tempWholePanel.add(tempPanel);
		tempWholePanel.setBorder(new TitledBorder(GreenLabGUI
				.getString(tempName(0).replaceAll("[\\(\\)]", "_"))));

		lightFlag = new GreenLabBooleanCheckBox(lightName(1).replaceAll(
				"[\\(\\)]", "_"));
		lightFlag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean lightBool = lightFlag.getBoolean();
				for (int i = 0; i < GreenLabAtributes.ENVIRONMENT_LIGHT_NUM - 2; i++) {
					lightText[i].setEditable(lightBool);
				}
				GreenLabPropertyFileReader.get().setProperty(lightName(1),
						lightFlag.getTextValue());
			}
		});
		lightFlag.setTextValue(GreenLabPropertyFileReader.get().getProperty(
				lightName(1)));
		JPanel flagLightPanel = new JPanel(new BorderLayout());
		JPanel flagLight = new JPanel();
		flagLight.add(lightFlag);
		flagLight.add(new GreenLabJLabel((lightName(1).replaceAll("[\\(\\)]",
				"_"))));
		flagLightPanel.add(flagLight, BorderLayout.WEST);
		Box lightWholePanel = Box.createVerticalBox();
		lightWholePanel.add(flagLightPanel);
		lightWholePanel.add(lightPanel);
		lightWholePanel.setBorder(new TitledBorder(GreenLabGUI
				.getString(lightName(0).replaceAll("[\\(\\)]", "_"))));

		Box box = Box.createVerticalBox();
		box.add(soilWholePanel);
		box.add(tempWholePanel);
		box.add(lightWholePanel);

		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(box, BorderLayout.NORTH);
		this.setLayout(new BorderLayout());
		this.add(mainPanel, BorderLayout.WEST);
		updateValue();
	}

	public void updateValue() {
		soilFlag.setTextValue(GreenLabPropertyFileReader.get().getProperty(
				soilName(1)));
		tempFlag.setTextValue(GreenLabPropertyFileReader.get().getProperty(
				tempName(1)));
		sumFlag.setTextValue(GreenLabPropertyFileReader.get().getProperty(
				tempName(2)));
		lightFlag.setTextValue(GreenLabPropertyFileReader.get().getProperty(
				lightName(1)));
		for (int i = 0; i < GreenLabAtributes.ENVIRONMENT_SOIL_NUM - 1; i++) {
			soilText[i].setValue(GreenLabPropertyFileReader.get().getProperty(
					soilName(i + 2)));
		}
		for (int i = 1; i < GreenLabAtributes.ENVIRONMENT_TEMP_NUM - 1; i++) {
			tempText[i].setText(GreenLabPropertyFileReader.get().getProperty(
					tempName(i + 2)));
		}
		for (int i = 0; i < GreenLabAtributes.ENVIRONMENT_LIGHT_NUM - 1; i++) {
			lightText[i].setValue(GreenLabPropertyFileReader.get().getProperty(
					lightName(i + 2)));
		}
		boolean soilBool = soilFlag.getBoolean();
		boolean tempBool = tempFlag.getBoolean();
		boolean sumBool = sumFlag.getBoolean();
		boolean lightBool = lightFlag.getBoolean();
		for (int i = 0; i < GreenLabAtributes.ENVIRONMENT_SOIL_NUM - 1; i++) {
			soilText[i].setEditable(soilBool);
		}
		for (int i = 1; i < GreenLabAtributes.ENVIRONMENT_TEMP_NUM - 1; i++) {
			tempText[i].setEditable(tempBool);
		}
		sumFlag.setEnabled(tempBool);
		if (sumFlag.isEnabled())
			for (int i = 3; i <= 4; i++) {
				tempText[i].setEditable(sumBool);
			}
		for (int i = 0; i < GreenLabAtributes.ENVIRONMENT_LIGHT_NUM - 2; i++) {
			lightText[i].setEditable(lightBool);
		}
	}

	private String soilName(int n) {
		String str;
		switch (n) {
		case 0:
			str = "soil_water_budget";
			break;
		case 1:
			str = "Flag_climate";
			break;
		case 2:
			str = "SoilWaterEffect(K1)";
			break;
		case 3:
			str = "SoilWaterEffect(K2)";
			break;
		case 4:
			str = "Soil_budget_para(C1)";
			break;
		case 5:
			str = "Soil_budget_para(C2)";
			break;
		case 6:
			str = "InitialSoilWater(QSW0)";
			break;
		case 7:
			str = "MaxPlantSoilWater(QSWMAX)";
			break;
		case 8:
			str = "MinPlantSoilWater(QSWMIN)";
			break;
		case 9:
			str = "MaxEnvFSoilWater(QSW_MAX)";
			break;
		case 10:
			str = "MinEnvFSoilWater(QSW_MIN)";
			break;
		default:
			str = "";
			break;
		}
		return str;
	}

	private String tempName(int n) {
		String str;
		switch (n) {
		case 0:
			str = "temperature_effect";
			break;
		case 1:
			str = "Flag_temperature";
			break;
		case 2:
			str = "SumTEffect(Flag_sumt)";
			break;
		case 3:
			str = "Temperature(E_ALPHA)";
			break;
		case 4:
			str = "Temperature(E_BETA)";
			break;
		case 5:
			str = "SumTEffect(KSUMT)";
			break;
		case 6:
			str = "SumTEffect(THETA_BASE)";
			break;
		case 7:
			str = "MaxEnvFTemp(THETA_MAX)";
			break;
		case 8:
			str = "MinEnvFTemp(THETA_MIN)";
			break;
		default:
			str = "";
			break;
		}
		return str;
	}

	private String lightName(int n) {
		String str;
		switch (n) {
		case 0:
			str = "light_effect";
			break;
		case 1:
			str = "Flag_light";
			break;
		case 2:
			str = "Light(E_A)";
			break;
		case 3:
			str = "Light(E_B)";
			break;
		case 4:
			str = "MaxEnvFLight(LIGHT_MAX)";
			break;
		case 5:
			str = "MinEnvFLight(LIGHT_MIN)";
			break;
		case 6:
			str = "PotentialCondition(E0)";
			break;
		default:
			str = "";
			break;
		}
		return str;
	}
}
