/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import de.grogra.graph.impl.GraphManager;
import de.grogra.greenlab.conf.GreenLabAtributes;
import de.grogra.greenlab.conf.GreenLabPropertyFileReader;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.TypeItem;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.FileChooserResult;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.UIToolkit;
import de.grogra.pf.ui.Window;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.swing.PanelSupport;
import de.grogra.pf.ui.swing.SwingPanel;
import de.grogra.pf.ui.swing.WindowSupport;
import de.grogra.reflect.Type;
import de.grogra.rgg.Reference;
import de.grogra.util.I18NBundle;
import de.grogra.util.Map;
import de.grogra.vfs.MemoryFileSystem;

/**
 * Main class for the GreenLab GUI. Includes the buttons (apply, reset, load,
 * export) and the tab panels.
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabGUI {

	private GreenLabTopologyPanel topologyPanel;
	private GreenLabGrowthPanel growthPanel;
	private GreenLabGeometryPanel geomatryPanel;
	private GreenLabEnvironmentPanel environmentPanel;
	private GreenLabOutputPanel outputPanel;
	private GreenLabGlobalPanel globalPanel;
	private static I18NBundle thisI18NBundle;

	public static GreenLabTooltipPanel tooltipPanel;

	public GreenLabGUI(final Context ctx) {
		// maize.sci
		GreenLabPropertyFileReader.get().setFile(ctx);
		// parameter.rgg
		HashMap<?, ?> parameter = getParameter(ctx);
		GreenLabPropertyFileReader.get().reloadRgg(parameter);
		thisI18NBundle = ctx.getWorkbench().getRegistry()
				.getPluginDescriptor("de.grogra.greenlab").getI18NBundle();
	}

	@SuppressWarnings({ "unchecked", "unused", "rawtypes" })
	public static HashMap getParameter(final Context ctx) {
		Item dir = ctx.getWorkbench().getRegistry().getItem("/classes");
		Field[] f = null;
		Class<?> instance = null;
		// search for the wanted class
		for (Item c = (Item) dir.getBranch(); c != null; c = (Item) c
				.getSuccessor()) {
			if (c.getName().endsWith("Parameter")) {
				// read all parameters from this class
				Type<?> t = (Type<?>) ((TypeItem) c).getObject();
				instance = t.getImplementationClass();
				f = instance.getDeclaredFields();
				break;
			}
		}

		// extract values and names from the field array
		// and save it to a hashmap
		HashMap map = new HashMap();
		DecimalFormat df = new DecimalFormat("0.0");
		df.setMaximumFractionDigits(GreenLabAtributes.DOUBLE_DIGITS);
		df.setMinimumFractionDigits(0);
		try {
			for (int i = 0; i < f.length; i++) {
				Field tmpField = f[i];
				Class<?> tmpFieldType = f[i].getType();
				String value = "";
				String simpleName = tmpFieldType.getSimpleName();
				if (simpleName.contains("[][]")) {
					Object obj = f[i].get(instance);
					int dim = Array.getLength(obj);
					for (int k = 0; k < dim; k++) {
						Object elementObject = Array.get(obj, k);
						int dim2 = Array.getLength(elementObject);
						for (int kk = 0; kk < dim2; kk++) {
							Object elementObject2 = Array
									.get(elementObject, kk);
							String tmpStr = "";
							try {
								tmpStr = df.format(elementObject2);
							} catch (Exception e) {
								tmpStr = elementObject2.toString();
							} finally {
								value += tmpStr + " ";
							}
						}
					}
				} else if (simpleName.contains("[]")) {
					Object obj = f[i].get(instance);
					String ss2 = obj.toString();
					int dim = Array.getLength(obj);
					for (int k = 0; k < dim; k++) {
						Object elementObject = Array.get(obj, k);
						String tmpStr = "";
						try {
							tmpStr = df.format(elementObject);
						} catch (Exception e) {
							tmpStr = elementObject.toString();
						} finally {
							value += tmpStr + " ";
						}
					}
				} else {
					if (simpleName.contains("Reference")) {
						Reference ref = (Reference) f[i].get(instance);
						value = ref.getName();
					} else {
						Object elementObject = f[i].get(instance);
						String tmpStr = "";
						try {
							tmpStr = df.format(elementObject);
						} catch (Exception e) {
							tmpStr = elementObject.toString();
						} finally {
							value = tmpStr;
						}
					}
				}
				String[] vv = value.replaceAll("\\s+$", "")
						.replaceAll("^\\s+", "").split("\\s+");
				value = "";
				for (int k = 0; k < vv.length; k++) {
					try {
						String v = df.format(Double.parseDouble(vv[k].replace(
								",", "."))) + " ";
						value += v;
					} catch (Exception e) {
						value += vv[k] + " ";
					}
				}
				map.put(f[i].getName(), value.replaceAll("\\s+$", "")
						.replaceAll("^\\s+", "").replaceAll("false", "0")
						.replaceAll("true", "1"));
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return map;
	}

	private void createObjectInspector(Panel panel, final Context ctx,
			GraphManager graph, Map params) {
		Container c = ((SwingPanel) panel.getComponent()).getContentPane();
		((PanelSupport) panel).initialize((WindowSupport) ctx.getWindow(),
				params);

		UIToolkit ui = UIToolkit.get(ctx);

		// parameter panels
		topologyPanel = new GreenLabTopologyPanel();
		growthPanel = new GreenLabGrowthPanel();
		geomatryPanel = new GreenLabGeometryPanel();
		environmentPanel = new GreenLabEnvironmentPanel();
		outputPanel = new GreenLabOutputPanel();
		globalPanel = new GreenLabGlobalPanel(topologyPanel, growthPanel,
				geomatryPanel, environmentPanel, outputPanel);

		// main pane
		JTabbedPane tabPane = (JTabbedPane) ui.createTabbedPane(
				new String[] {
						thisI18NBundle.getString("tab0.Name"),
						thisI18NBundle.getString("tab1.Name"),
						thisI18NBundle.getString("tab2.Name"),
						thisI18NBundle.getString("tab3.Name"),
						thisI18NBundle.getString("tab4.Name")}, new Object[] {
						globalPanel, topologyPanel, growthPanel, geomatryPanel,
						environmentPanel});
// old version, with output panel 
//		JTabbedPane tabPane = (JTabbedPane) ui.createTabbedPane(
//				new String[] {
//						thisI18NBundle.getString("tab0.Name"),
//						thisI18NBundle.getString("tab1.Name"),
//						thisI18NBundle.getString("tab2.Name"),
//						thisI18NBundle.getString("tab3.Name"),
//						thisI18NBundle.getString("tab4.Name"),
//						thisI18NBundle.getString("tab5.Name") }, new Object[] {
//						globalPanel, topologyPanel, growthPanel, geomatryPanel,
//						environmentPanel, outputPanel });

		
		
		// Buttons
		JButton applyButton = new JButton(
				thisI18NBundle.getString("applyButton.Name"));
		applyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						writeParameterToParameterFile(ctx);
					}
				});
			}
		});

		JButton resetButton = new JButton(
				thisI18NBundle.getString("resetButton.Name"));
		resetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						// HashMap<?, ?> parameter = getParameter(ctx);
						// GreenLabPropertyFileReader.get().reloadRgg(parameter);
						// GreenLabPropertyFileReader.get().setFile(ctx);
						GreenLabPropertyFileReader.get().reset();
						globalPanel.updateValue();
						topologyPanel.updateValue();
						environmentPanel.updateValue();
						growthPanel.updateValue();
						outputPanel.updateValue();
						geomatryPanel.updateValue();
					}
				});
			}
		});

		JButton openParameterFileButton = new JButton(
				thisI18NBundle.getString("openParameterFileButton.Name"));
		openParameterFileButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Workbench wb = ctx.getWorkbench();
				FileChooserResult fr = 	ui.chooseFile(
						thisI18NBundle.getString("filedialog.openparameterfile","Open Parameter File"), 
						IO.getReadableFileTypes(new IOFlavor[] { IOFlavor.SCI_FLAVOR }),
						Window.OPEN_FILE, 
						true, null, wb, null);
				if (fr != null) {
					// read values from file
					FileSource fs = fr.createFileSource(wb.getRegistry(), null);
					GreenLabPropertyFileReader.get().setFile(fs);
					globalPanel.updateValue();
					topologyPanel.updateValue();
					environmentPanel.updateValue();
					growthPanel.updateValue();
					outputPanel.updateValue();
					geomatryPanel.updateValue();
				}
			}
		});

		JButton exportParameterButton = new JButton(
				thisI18NBundle.getString("exportParameterButton.Name"));
		exportParameterButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Workbench wb = ctx.getWorkbench();

				FileChooserResult fr = wb.chooseFileToSave(UI.I18N.getString(
						"filedialog.openparameterfile", "Export Parameter"),
						IOFlavor.SCI_FLAVOR, null);
				if (fr != null) {
					FileSource fs = fr.createFileSource(wb.getRegistry(), null);
					try {
						OutputStream out = fs.getOutputStream(false);
						out.write(GreenLabPropertyFileReader.get().getSci()
								.getBytes());
						out.flush();
						fs.streamClosed();

						// out.close();
						// Exception in thread "AWT-EventQueue-0"
						// java.lang.IllegalArgumentException: path in TreePath
						// must be non null.
						// be course close will trigger an treeevent but the
						// treepath is null for this file
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		// Button Panel
		JPanel button0Panel = new JPanel();
		button0Panel.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
		button0Panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		button0Panel.add(applyButton);
		button0Panel.add(resetButton);
		button0Panel.add(Box.createHorizontalStrut(23));
		button0Panel.add(openParameterFileButton);
		button0Panel.add(exportParameterButton);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		buttonPanel.add(Box.createVerticalStrut(5));
		buttonPanel.add(button0Panel);
		buttonPanel.add(Box.createVerticalStrut(5));

		// main scroll pane
		JScrollPane scrollPane = new JScrollPane(tabPane);
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);

		tooltipPanel = new GreenLabTooltipPanel();

		// Layout
		c.setLayout(new BorderLayout());
		c.add(buttonPanel, BorderLayout.NORTH);
		c.add(scrollPane, BorderLayout.CENTER);
		c.add(tooltipPanel, BorderLayout.SOUTH);
	}

	public static Panel createPanel(Context ctx, Map params) {
		GraphManager graph = ctx.getWorkbench().getRegistry().getProjectGraph();
		UIToolkit ui = UIToolkit.get(ctx);
		Panel p = ui.createPanel(ctx, null, params);
		GreenLabGUI mgr = new GreenLabGUI(ctx);
		mgr.createObjectInspector(p, ctx, graph, params);
		return p;
	}

	private void writeParameterToParameterFile(final Context ctx) {
		Registry r = ctx.getWorkbench().getRegistry();
		MemoryFileSystem fs = (MemoryFileSystem) r.getFileSystem();
		OutputStream out = fs.getOutputStream(fs.getFile("Parameter.rgg"),
				false);
		try {
			out.write(GreenLabPropertyFileReader.get().getRgg().getBytes());
			out.flush();
			out.close();
		} catch (IOException e) {
			System.out
					.println("Error in GreenLabGUI (writeParameterToParameterFile)");
			e.printStackTrace();
		}

		//refresh JEdit text editor
		Workbench.refreshJEdit(ctx.getWorkbench(), "Parameter.rgg");
	}

	public static String getString(String key) {
		String ret = key;
		try {
			ret = thisI18NBundle.getString(key);
		} catch (Exception e) {
		}
		return ret;
	}

}
