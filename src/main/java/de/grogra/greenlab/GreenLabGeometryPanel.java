/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

import de.grogra.greenlab.conf.GreenLabAtributes;
import de.grogra.greenlab.conf.GreenLabPropertyFileReader;
import de.grogra.greenlab.ui.elements.GreenLabBooleanCheckBox;
import de.grogra.greenlab.ui.elements.GreenLabColorBox;
import de.grogra.greenlab.ui.elements.GreenLabJLabel;
import de.grogra.greenlab.ui.elements.GreenLabNumberField;
import de.grogra.greenlab.ui.elements.GreenLabTextField;

/**
 * The Geometry Panel
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabGeometryPanel extends JPanel {

	private static final long serialVersionUID = -2073432007086110708L;
	private GreenLabTextField fieldLeafShape, fieldFemaleShape, fieldMaleShape;
	private JTabbedPane jtp = new JTabbedPane();
	private JPanel pap[] = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
	private GreenLabNumberField metaTextField[][][] = new GreenLabNumberField[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][4];
	private JTextField paraTextField[][] = new JTextField[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][GreenLabAtributes.GEOMETRY_PARAMETER_NUM];
	private GreenLabBooleanCheckBox paraCheckBox[][] = new GreenLabBooleanCheckBox[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][GreenLabAtributes.GEOMETRY_PARAMETER_NUM];
	private GreenLabColorBox color1[][] = new GreenLabColorBox[4][2];
	private GreenLabTextField shader[][] = new GreenLabTextField[4][2];
	private GreenLabColorBox color2[] = new GreenLabColorBox[6];

	GreenLabGeometryPanel() {
		JPanel shapePanel = new JPanel(new GridLayout(2, 6));
		fieldLeafShape = new GreenLabTextField("Leaf_shape_smb_L_");
		fieldFemaleShape = new GreenLabTextField("Female_shape_smb_Ff_");
		fieldMaleShape = new GreenLabTextField("Male_shape_smb_Fm_");
		shapePanel.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "blade_trans")));
		JPanel tmpLabel = new JPanel(new BorderLayout());
		tmpLabel.add(new GreenLabJLabel(("Leaf_shape_smb_L_")),
				BorderLayout.SOUTH);
		shapePanel.add(tmpLabel);
		tmpLabel = new JPanel(new BorderLayout());
		tmpLabel.add(fieldLeafShape, BorderLayout.SOUTH);
		shapePanel.add(tmpLabel);
		shapePanel
				.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "malefruit_trans")));
		tmpLabel = new JPanel(new BorderLayout());
		tmpLabel.add(new GreenLabJLabel(("Female_shape_smb_Ff_")),
				BorderLayout.SOUTH);
		shapePanel.add(tmpLabel);
		tmpLabel = new JPanel(new BorderLayout());
		tmpLabel.add(fieldFemaleShape, BorderLayout.SOUTH);
		shapePanel.add(tmpLabel);
		shapePanel
				.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "femalefruit_trans")));
		tmpLabel = new JPanel(new BorderLayout());
		tmpLabel.add(new GreenLabJLabel(("Male_shape_smb_Fm_")),
				BorderLayout.SOUTH);
		shapePanel.add(tmpLabel);
		tmpLabel = new JPanel(new BorderLayout());
		tmpLabel.add(fieldMaleShape, BorderLayout.SOUTH);
		shapePanel.add(tmpLabel);
		shapePanel.add(new GreenLabJLabel(""));
		shapePanel.add(new GreenLabJLabel(""));
		shapePanel.add(new GreenLabJLabel(""));
		fieldLeafShape.getDocument().addDocumentListener(
				new DocumentListener() {
					public void actionPerformed() {
						GreenLabPropertyFileReader.get().setProperty(
								"Leaf_shape(smb_L)", fieldLeafShape.getText());
					}

					@Override
					public void changedUpdate(DocumentEvent e) {
						actionPerformed();
					}

					@Override
					public void insertUpdate(DocumentEvent e) {
						actionPerformed();
					}

					@Override
					public void removeUpdate(DocumentEvent e) {
						actionPerformed();
					}
				});
		fieldFemaleShape.getDocument().addDocumentListener(
				new DocumentListener() {
					public void actionPerformed() {
						GreenLabPropertyFileReader.get().setProperty(
								"Female_shape(smb_Ff)",
								fieldFemaleShape.getText());
					}

					@Override
					public void changedUpdate(DocumentEvent e) {
						actionPerformed();
					}

					@Override
					public void insertUpdate(DocumentEvent e) {
						actionPerformed();
					}

					@Override
					public void removeUpdate(DocumentEvent e) {
						actionPerformed();
					}
				});
		fieldMaleShape.getDocument().addDocumentListener(
				new DocumentListener() {
					public void actionPerformed() {
						GreenLabPropertyFileReader.get().setProperty(
								"Male_shape(smb_Fm)", fieldMaleShape.getText());
					}

					@Override
					public void changedUpdate(DocumentEvent e) {
						actionPerformed();
					}

					@Override
					public void insertUpdate(DocumentEvent e) {
						actionPerformed();
					}

					@Override
					public void removeUpdate(DocumentEvent e) {
						actionPerformed();
					}
				});
		shapePanel.setBorder(new TitledBorder(GreenLabGUI.getString("Shape")));

		JPanel colorPanel1 = getColorPanel1();
		JPanel colorPanel2 = new JPanel(new GridLayout(6, 3));
		
		for (int i = 0; i < 6; i++) {
			colorPanel2.add(new GreenLabJLabel(""));
			colorPanel2.add(new GreenLabJLabel(GreenLabGUI
					.getString(colorName(4).replaceAll("[\\(\\)]", "_"))
					+ " " + GreenLabGUI.getString("PA") + (i +1),
					colorName(4)));
			color2[i] = new GreenLabColorBox(colorName(4).replaceAll(
					"[\\(\\)]", "_"), colorName(4), i);
			colorPanel2.add(color2[i]);
		}
		
		Box box = Box.createHorizontalBox();		
		box.add(colorPanel1);
		box.add(colorPanel2);
		box.setBorder(new TitledBorder(GreenLabGUI.getString("Color")));

		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			pap[i] = new JPanel(new BorderLayout());
			JPanel leftPanel = new JPanel(new GridLayout(
					GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE + 2, 1));
			// JPanel rightPanel = new JPanel(new BorderLayout());
			JPanel rightPanel0 = new JPanel(new BorderLayout());
			JPanel rightPanel1 = new JPanel(new BorderLayout());
			JPanel rightPanel2 = new JPanel(new BorderLayout());
			JPanel rightPanel3 = new JPanel(new BorderLayout());
			JPanel rightPanel4 = new JPanel(new BorderLayout());
			// JPanel rightLeftPanel = new JPanel(new GridLayout(paraN, 1));
			// JPanel rightRightPanel = new JPanel(new GridLayout(paraN, 1));
			JPanel rightLeftPanel0 = new JPanel(new GridLayout(1, 1));
			JPanel rightRightPanel0 = new JPanel(new GridLayout(1, 1));
			JPanel rightPicPanel0 = new JPanel(new GridLayout(1, 1));
			JPanel rightLeftPanel1 = new JPanel(new GridLayout(7, 1));
			JPanel rightRightPanel1 = new JPanel(new GridLayout(7, 1));
			JPanel rightPicPanel1 = new JPanel(new GridLayout(7, 1));
			JPanel rightLeftPanel2 = new JPanel(new GridLayout(9, 1));
			JPanel rightRightPanel2 = new JPanel(new GridLayout(9, 1));
			JPanel rightPicPanel2 = new JPanel(new GridLayout(9, 1));
			JPanel rightLeftPanel3 = new JPanel(new GridLayout(4, 1));
			JPanel rightRightPanel3 = new JPanel(new GridLayout(4, 1));
			JPanel rightPicPanel3 = new JPanel(new GridLayout(4, 1));
			JPanel rightLeftPanel4 = new JPanel(new GridLayout(6, 1));
			JPanel rightRightPanel4 = new JPanel(new GridLayout(6, 1));
			JPanel rightPicPanel4 = new JPanel(new GridLayout(6, 1));
			JPanel leftTopPanel = new JPanel();
			leftTopPanel.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "pa" + (i + 1)
									+ "_trans")));
			leftTopPanel
					.add(new GreenLabJLabel(("Phy_ages_types_of_metamers")));
			leftPanel.add(leftTopPanel);
			JPanel leftTopTPanel = new JPanel(new GridLayout(1, 5));
			leftTopTPanel.add(new GreenLabJLabel(" "));
			leftTopTPanel.add(new GreenLabJLabel(("Male") + "  ",
					SwingConstants.LEFT));
			leftTopTPanel.add(new GreenLabJLabel(("Female") + "  ",
					SwingConstants.LEFT));
			leftTopTPanel.add(new GreenLabJLabel(("Blade") + "  ",
					SwingConstants.CENTER));
			leftTopTPanel.add(new GreenLabJLabel(("Axillary") + "  ",
					SwingConstants.CENTER));
			leftPanel.add(leftTopTPanel);
			for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE - i; j++) {
				JPanel itemPanel = new JPanel();
				itemPanel.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "met" + (i + 1)
										+ (j + 1) + "_trans")));

				metaTextField[i][j][0] = new GreenLabNumberField(
						"Angle_Male_Ang_Fm_", true);
				metaTextField[i][j][1] = new GreenLabNumberField(
						"Angle_Female_Ang_Ff_", true);
				metaTextField[i][j][2] = new GreenLabNumberField(
						"Angle_Blade_Ang_B_", true);
				metaTextField[i][j][3] = new GreenLabNumberField(
						"Angle_Axillary_Ang_A_", true);

				itemPanel.add(metaTextField[i][j][0]);
				itemPanel.add(metaTextField[i][j][1]);
				itemPanel.add(metaTextField[i][j][2]);
				itemPanel.add(metaTextField[i][j][3]);
				leftPanel.add(itemPanel);
				final int indexI = i;
				final int indexJ = j;
				metaTextField[i][j][0].getDocument().addDocumentListener(
						new DocumentListener() {

							public void actionPerformed() {
								GreenLabPropertyFileReader.get().setProperty(
										"Angle_Male(Ang_Fm)",
										metaTextField[indexI][indexJ][0]
												.getText(), indexI + 1,
										indexJ + 1);
							}

							@Override
							public void changedUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void insertUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void removeUpdate(DocumentEvent e) {
								actionPerformed();
							}
						});
				metaTextField[i][j][1].getDocument().addDocumentListener(
						new DocumentListener() {

							public void actionPerformed() {
								GreenLabPropertyFileReader.get().setProperty(
										"Angle_Female(Ang_Ff)",
										metaTextField[indexI][indexJ][1]
												.getText(), indexI + 1,
										indexJ + 1);
							}

							@Override
							public void changedUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void insertUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void removeUpdate(DocumentEvent e) {
								actionPerformed();
							}
						});
				metaTextField[i][j][2].getDocument().addDocumentListener(
						new DocumentListener() {

							public void actionPerformed() {
								GreenLabPropertyFileReader.get().setProperty(
										"Angle_Blade(Ang_B)",
										metaTextField[indexI][indexJ][2]
												.getText(), indexI + 1,
										indexJ + 1);
							}

							@Override
							public void changedUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void insertUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void removeUpdate(DocumentEvent e) {
								actionPerformed();
							}
						});
				metaTextField[i][j][3].getDocument().addDocumentListener(
						new DocumentListener() {

							public void actionPerformed() {
								GreenLabPropertyFileReader.get().setProperty(
										"Angle_Axillary(Ang_A)",
										metaTextField[indexI][indexJ][3]
												.getText(), indexI + 1,
										indexJ + 1);
							}

							@Override
							public void changedUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void insertUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void removeUpdate(DocumentEvent e) {
								actionPerformed();
							}
						});
			}
			// rightPanel.setBorder(new TitledBorder(GreenLabGUI
			// .getString("topological_parameter")));
			leftPanel.setBorder(new TitledBorder(GreenLabGUI
					.getString("organ_angles")));
			for (int j = 0; j < GreenLabAtributes.GEOMETRY_PARAMETER_NUM; j++) {
				if (j == 2)
					paraTextField[i][j] = new GreenLabNumberField(paraName(j)
							.replaceAll("[\\(\\)]", "_"));
				else if (j == 8 || j == 11 || j == 15 || j == 16 || j == 17
						|| j == 19 || j == 20)
					paraCheckBox[i][j] = new GreenLabBooleanCheckBox(
							paraName(j).replaceAll("[\\(\\)]", "_"));
				else if (j >= 21 && j <= 23)
					paraTextField[i][j] = new GreenLabNumberField(paraName(j)
							.replaceAll("[\\(\\)]", "_"));
				else
					paraTextField[i][j] = new GreenLabNumberField(paraName(j)
							.replaceAll("[\\(\\)]", "_"));
				JPanel label = new JPanel(new BorderLayout());
				if (j == 8 || j == 11 || j == 15 || j == 16 || j == 17
						|| j == 19 || j == 20) {
					JPanel tmpPanel = new JPanel(new BorderLayout());
					tmpPanel.add(paraCheckBox[i][j], BorderLayout.WEST);
					tmpPanel.add(
							new GreenLabJLabel((paraName(j).replaceAll(
									"[\\(\\)]", "_"))), BorderLayout.CENTER);
					label.add(tmpPanel, BorderLayout.SOUTH);
				} else {
					label.add(
							new GreenLabJLabel((paraName(j).replaceAll(
									"[\\(\\)]", "_"))), BorderLayout.SOUTH);
				}
				if (j == 0)
					rightLeftPanel0.add(label);
				else if (j < 8)
					rightLeftPanel1.add(label);
				else if (j < 17)
					rightLeftPanel2.add(label);
				else if (j < 21)
					rightLeftPanel3.add(label);
				else
					rightLeftPanel4.add(label);
				final int indexI = i;
				final int indexJ = j;
				if (j == 8 || j == 11 || j == 15 || j == 16 || j == 17
						|| j == 19 || j == 20) {
					JPanel check = new JPanel(new BorderLayout());
					check.add(new GreenLabJLabel(""), BorderLayout.SOUTH);
					if (j == 0)
						rightRightPanel0.add(check);
					else if (j < 8)
						rightRightPanel1.add(check);
					else if (j < 17)
						rightRightPanel2.add(check);
					else if (j < 21)
						rightRightPanel3.add(check);
					else
						rightRightPanel4.add(check);
					paraCheckBox[i][j].addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {
							GreenLabPropertyFileReader
									.get()
									.setProperty(
											paraName(indexJ),
											paraCheckBox[indexI][indexJ]
													.getTextValue(), indexI + 1);
							if (indexJ == 11) {
								boolean v = paraCheckBox[indexI][indexJ]
										.isSelected();
								for (int k = 12; k < 15; k++)
									paraTextField[indexI][k].setEditable(v);
							}
						}
					});
				} else {
					paraTextField[i][j].getDocument().addDocumentListener(
							new DocumentListener() {

								public void actionPerformed() {
									GreenLabPropertyFileReader
											.get()
											.setProperty(
													paraName(indexJ),
													paraTextField[indexI][indexJ]
															.getText(),
													indexI + 1);
								}

								@Override
								public void changedUpdate(DocumentEvent e) {
									actionPerformed();
								}

								@Override
								public void insertUpdate(DocumentEvent e) {
									actionPerformed();
								}

								@Override
								public void removeUpdate(DocumentEvent e) {
									actionPerformed();
								}
							});
					JPanel text = new JPanel(new BorderLayout());
					text.add(paraTextField[i][j], BorderLayout.SOUTH);
					if (j == 0)
						rightRightPanel0.add(text);
					else if (j < 8)
						rightRightPanel1.add(text);
					else if (j < 17)
						rightRightPanel2.add(text);
					else if (j < 21)
						rightRightPanel3.add(text);
					else
						rightRightPanel4.add(text);
				}
				if (j == 0) {
					rightPicPanel0.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "phyllo2")));
				} else if (j == 1) {
					rightPicPanel1.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "YoungM_trans")));
				} else if (j == 2) {
					rightPicPanel1.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "force_point_trans")));
				} else if (j == 3) {
					rightPicPanel1.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "term_angle_trans")));
				} else if (j == 4) {
					rightPicPanel1.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "righting_point_trans")));
				} else if (j < 8) {
					rightPicPanel1.add(new GreenLabJLabel(""));
				} else if (j == 8) {
					rightPicPanel2.add(new GreenLabJLabel(""));
				} else if (j == 9) {
					rightPicPanel2.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "max_angle_trans")));
				} else if (j == 10) {
					rightPicPanel2.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "openning_cycle_trans")));
				} else if (j < 17) {
					rightPicPanel2.add(new GreenLabJLabel(""));
				} else if (j < 21) {
					rightPicPanel3.add(new GreenLabJLabel(""));
				} else {
					rightPicPanel4.add(new GreenLabJLabel(""));
				}
			}

			rightPanel0.add(rightPicPanel0, BorderLayout.WEST);
			rightPanel0.add(rightLeftPanel0, BorderLayout.CENTER);
			rightPanel0.add(rightRightPanel0, BorderLayout.EAST);
			rightPanel0.add(Box.createRigidArea(new Dimension(0,10)), BorderLayout.SOUTH);
			rightPanel1.add(
					new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "Axis_red_trans"),
							SwingConstants.CENTER), BorderLayout.NORTH);
			rightPanel1.add(rightPicPanel1, BorderLayout.WEST);
			rightPanel1.add(rightLeftPanel1, BorderLayout.CENTER);
			rightPanel1.add(rightRightPanel1, BorderLayout.EAST);
			rightPanel2.add(
					new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "metamer_trans"),
							SwingConstants.CENTER), BorderLayout.NORTH);
			rightPanel2.add(rightPicPanel2, BorderLayout.WEST);
			rightPanel2.add(rightLeftPanel2, BorderLayout.CENTER);
			rightPanel2.add(rightRightPanel2, BorderLayout.EAST);
			rightPanel3.add(rightPicPanel3, BorderLayout.WEST);
			rightPanel3.add(rightLeftPanel3, BorderLayout.CENTER);
			rightPanel3.add(rightRightPanel3, BorderLayout.EAST);
			rightPanel4.add(rightPicPanel4, BorderLayout.WEST);
			rightPanel4.add(rightLeftPanel4, BorderLayout.CENTER);
			rightPanel4.add(rightRightPanel4, BorderLayout.EAST);

			rightPanel0.setBorder(new TitledBorder(GreenLabGUI
					.getString("phyllotaxy_angle")));
			rightPanel1.setBorder(new TitledBorder(GreenLabGUI
					.getString("axis_angle")));
			rightPanel2.setBorder(new TitledBorder(GreenLabGUI
					.getString("organ_angle")));
			rightPanel3.setBorder(new TitledBorder(GreenLabGUI
					.getString("other_angle")));
			rightPanel4.setBorder(new TitledBorder(GreenLabGUI
					.getString("other_angle2")));

			Box box1 = Box.createVerticalBox();
			Box box2 = Box.createVerticalBox();
			Box boxM = Box.createHorizontalBox();
			//original version with: 
			//	left: organ angles, phyllotaxis, branch bending
			//  right: organ bending, pruning, initial size
//			box1.add(leftPanel);
//			box1.add(rightPanel0);
//			box1.add(rightPanel1);
//			box2.add(rightPanel2);
//			box2.add(rightPanel3);
//			box2.add(rightPanel4);

			//short version with:
			//	left: organ angles
			//  right: phyllotaxis			
			box1.add(leftPanel);
			box2.add(rightPanel0);
			box2.add(Box.createRigidArea(new Dimension(0,280)));
			
			boxM.add(box1);
			boxM.add(box2);
			// pap[i].add(leftPanel, BorderLayout.NORTH);
			pap[i].add(boxM);
			jtp.add(GreenLabGUI.getString("PA") + (i + 1), pap[i]);
		}

		Box mainBox = Box.createVerticalBox();
		mainBox.add(shapePanel);
		mainBox.add(box);
		mainBox.add(jtp);

		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(mainBox, BorderLayout.NORTH);
		this.setLayout(new BorderLayout());
		this.add(mainPanel, BorderLayout.WEST);

		updateValue();
	}

	private JPanel getColorPanel1() {
		JPanel colorPanel1 = new JPanel(new GridLayout(10, 3));
		colorPanel1.add(new GreenLabJLabel(""));
		colorPanel1.add(new GreenLabJLabel(("Alive"), SwingConstants.CENTER));
		colorPanel1.add(new GreenLabJLabel(("Dead"), SwingConstants.CENTER));
		
		// row 0
		//line for colors
		colorPanel1.add(new GreenLabJLabel((colorName(0).replaceAll("[\\(\\)]", "_"))));
		color1[0][0] = new GreenLabColorBox(colorName(0).replaceAll("[\\(\\)]", "_"), colorName(0), 0);
		color1[0][1] = new GreenLabColorBox(colorName(0).replaceAll("[\\(\\)]", "_"), colorName(0), 1);
		colorPanel1.add(color1[0][0]);
		colorPanel1.add(color1[0][1]);
//		if(GreenLabPropertyFileReader.get().getProperty(textureName(0, 0)).equals("null")) {
//			color1[0][0].setEnabled(true);
//		} else {
//			color1[0][0].setEnabled(false);
//		}
//		if(GreenLabPropertyFileReader.get().getProperty(textureName(0, 1)).equals("null")) {
//			color1[0][1].setEnabled(true);
//		} else {
//			color1[0][1].setEnabled(false);
//		}
		
		//second line for shaders
		colorPanel1.add(new GreenLabJLabel(""));
		shader[0][0] = new GreenLabTextField(textureName(0, 0));
		shader[0][1] = new GreenLabTextField(textureName(0, 1));
		((AbstractDocument)(shader[0][0].getDocument())).setDocumentFilter(
			new DocumentFilter() {			
			    public void remove(DocumentFilter.FilterBypass fb, int offset, int length)
			    	throws BadLocationException {
			    	//if empty write null
			    	if ((fb.getDocument().getLength()) == 1) {
			    		shader[0][0].setText("null");
			    		GreenLabPropertyFileReader.get().setProperty(textureName(0, 0), "null");
			    		color1[0][0].setEnabled(true);
			    	} else {
				    	if((fb.getDocument().getText(0, fb.getDocument().getLength()-1)).equals("null")) { 
							color1[0][0].setEnabled(true); 
						} else { 
							color1[0][0].setEnabled(false); 
						}
				    	super.remove(fb, offset, length);
				    	GreenLabPropertyFileReader.get().setProperty(textureName(0, 0), shader[0][0].getText());
			    	}
			    }

				public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a)
					throws BadLocationException {
					if((fb.getDocument().getText(0, fb.getDocument().getLength())+str).equals("null")) { 
						color1[0][0].setEnabled(true); 
					} else { 
						color1[0][0].setEnabled(false); 
					}
				    super.replace(fb, offs, length, str, a);
				    GreenLabPropertyFileReader.get().setProperty(textureName(0, 0), shader[0][0].getText());
				}
			}
		);
		((AbstractDocument)(shader[0][1].getDocument())).setDocumentFilter(
				new DocumentFilter() {			
				    public void remove(DocumentFilter.FilterBypass fb, int offset, int length)
				    	throws BadLocationException {
				    	//if empty write null
				    	if ((fb.getDocument().getLength()) == 1) {
				    		shader[0][1].setText("null");
				    		GreenLabPropertyFileReader.get().setProperty(textureName(0, 1), "null");
				    		color1[0][1].setEnabled(true);
				    	} else {
					    	if((fb.getDocument().getText(0, fb.getDocument().getLength()-1)).equals("null")) { 
								color1[0][1].setEnabled(true); 
							} else { 
								color1[0][1].setEnabled(false); 
							}
					    	super.remove(fb, offset, length);
					    	GreenLabPropertyFileReader.get().setProperty(textureName(0, 1), shader[0][1].getText());
				    	}
				    }

					public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a)
						throws BadLocationException {
						if((fb.getDocument().getText(0, fb.getDocument().getLength())+str).equals("null")) { 
							color1[0][1].setEnabled(true); 
						} else { 
							color1[0][1].setEnabled(false); 
						}
					    super.replace(fb, offs, length, str, a);
					    GreenLabPropertyFileReader.get().setProperty(textureName(0, 1), shader[0][1].getText());
					}
				}
			);
		colorPanel1.add(new JLabel(""));colorPanel1.add(new JLabel(""));
//		colorPanel1.add(shader[0][0]);
//		colorPanel1.add(shader[0][1]);
		
		// row 1
		//line for colors
		colorPanel1.add(new GreenLabJLabel((colorName(1).replaceAll("[\\(\\)]", "_"))));
		color1[1][0] = new GreenLabColorBox(colorName(1).replaceAll("[\\(\\)]", "_"), colorName(1), 0);
		color1[1][1] = new GreenLabColorBox(colorName(1).replaceAll("[\\(\\)]", "_"), colorName(1), 1);
		colorPanel1.add(color1[1][0]);
		colorPanel1.add(color1[1][1]);	

		//second line for shaders
		colorPanel1.add(new GreenLabJLabel(""));
		shader[1][0] = new GreenLabTextField(textureName(1, 0));
		shader[1][1] = new GreenLabTextField(textureName(1, 1));
		((AbstractDocument)(shader[1][0].getDocument())).setDocumentFilter(
				new DocumentFilter() {			
				    public void remove(DocumentFilter.FilterBypass fb, int offset, int length)
				    	throws BadLocationException {
				    	//if empty write null
				    	if ((fb.getDocument().getLength()) == 1) {
				    		shader[1][0].setText("null");
				    		GreenLabPropertyFileReader.get().setProperty(textureName(1, 0), "null");
				    		color1[1][0].setEnabled(true);
				    	} else {
					    	if((fb.getDocument().getText(0, fb.getDocument().getLength()-1)).equals("null")) { 
								color1[1][0].setEnabled(true); 
							} else { 
								color1[1][0].setEnabled(false); 
							}
					    	super.remove(fb, offset, length);
					    	GreenLabPropertyFileReader.get().setProperty(textureName(1, 0), shader[1][0].getText());
				    	}
				    }

					public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a)
						throws BadLocationException {
						if((fb.getDocument().getText(0, fb.getDocument().getLength())+str).equals("null")) { 
							color1[1][0].setEnabled(true); 
						} else { 
							color1[1][0].setEnabled(false); 
						}
					    super.replace(fb, offs, length, str, a);
					    GreenLabPropertyFileReader.get().setProperty(textureName(1, 0), shader[1][0].getText());
					}
				}
			);
		((AbstractDocument)(shader[1][1].getDocument())).setDocumentFilter(
				new DocumentFilter() {			
				    public void remove(DocumentFilter.FilterBypass fb, int offset, int length)
				    	throws BadLocationException {
				    	//if empty write null
				    	if ((fb.getDocument().getLength()) == 1) {
				    		shader[1][1].setText("null");
				    		GreenLabPropertyFileReader.get().setProperty(textureName(1, 1), "null");
				    		color1[1][1].setEnabled(true);
				    	} else {
					    	if((fb.getDocument().getText(0, fb.getDocument().getLength()-1)).equals("null")) { 
								color1[1][1].setEnabled(true); 
							} else { 
								color1[1][1].setEnabled(false); 
							}
					    	super.remove(fb, offset, length);
					    	GreenLabPropertyFileReader.get().setProperty(textureName(1, 1), shader[1][1].getText());
				    	}
				    }

					public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a)
						throws BadLocationException {
						if((fb.getDocument().getText(0, fb.getDocument().getLength())+str).equals("null")) { 
							color1[1][1].setEnabled(true); 
						} else { 
							color1[1][1].setEnabled(false); 
						}
					    super.replace(fb, offs, length, str, a);
					    GreenLabPropertyFileReader.get().setProperty(textureName(1, 1), shader[1][1].getText());
					}
				}
			);
		colorPanel1.add(new JLabel(""));colorPanel1.add(new JLabel(""));
//		colorPanel1.add(shader[1][0]);
//		colorPanel1.add(shader[1][1]);

		// row 2
		//line for colors
		colorPanel1.add(new GreenLabJLabel((colorName(2).replaceAll("[\\(\\)]", "_"))));
		color1[2][0] = new GreenLabColorBox(colorName(2).replaceAll("[\\(\\)]", "_"), colorName(2), 0);
		color1[2][1] = new GreenLabColorBox(colorName(2).replaceAll("[\\(\\)]", "_"), colorName(2), 1);
		colorPanel1.add(color1[2][0]);
		colorPanel1.add(color1[2][1]);

		//second line for shaders
		colorPanel1.add(new GreenLabJLabel(""));
		shader[2][0] = new GreenLabTextField(textureName(2, 0));
		shader[2][1] = new GreenLabTextField(textureName(2, 1));
		((AbstractDocument)(shader[2][0].getDocument())).setDocumentFilter(
				new DocumentFilter() {			
				    public void remove(DocumentFilter.FilterBypass fb, int offset, int length)
				    	throws BadLocationException {
				    	//if empty write null
				    	if ((fb.getDocument().getLength()) == 1) {
				    		shader[2][0].setText("null");
				    		GreenLabPropertyFileReader.get().setProperty(textureName(2, 0), "null");
				    		color1[2][0].setEnabled(true);
				    	} else {
					    	if((fb.getDocument().getText(0, fb.getDocument().getLength()-1)).equals("null")) { 
								color1[2][0].setEnabled(true); 
							} else { 
								color1[2][0].setEnabled(false); 
							}
					    	super.remove(fb, offset, length);
					    	GreenLabPropertyFileReader.get().setProperty(textureName(2, 0), shader[2][0].getText());
				    	}
				    }

					public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a)
						throws BadLocationException {
						if((fb.getDocument().getText(0, fb.getDocument().getLength())+str).equals("null")) { 
							color1[2][0].setEnabled(true); 
						} else { 
							color1[2][0].setEnabled(false); 
						}
					    super.replace(fb, offs, length, str, a);
					    GreenLabPropertyFileReader.get().setProperty(textureName(2, 0), shader[2][0].getText());
					}
				}
			);
		((AbstractDocument)(shader[2][1].getDocument())).setDocumentFilter(
				new DocumentFilter() {			
				    public void remove(DocumentFilter.FilterBypass fb, int offset, int length)
				    	throws BadLocationException {
				    	//if empty write null
				    	if ((fb.getDocument().getLength()) == 1) {
				    		shader[2][1].setText("null");
				    		GreenLabPropertyFileReader.get().setProperty(textureName(2, 1), "null");
				    		color1[2][1].setEnabled(true);
				    	} else {
					    	if((fb.getDocument().getText(0, fb.getDocument().getLength()-1)).equals("null")) { 
								color1[2][1].setEnabled(true); 
							} else { 
								color1[2][1].setEnabled(false); 
							}
					    	super.remove(fb, offset, length);
					    	GreenLabPropertyFileReader.get().setProperty(textureName(2, 1), shader[2][1].getText());
				    	}
				    }

					public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a)
						throws BadLocationException {
						if((fb.getDocument().getText(0, fb.getDocument().getLength())+str).equals("null")) { 
							color1[2][1].setEnabled(true); 
						} else { 
							color1[2][1].setEnabled(false); 
						}
					    super.replace(fb, offs, length, str, a);
					    GreenLabPropertyFileReader.get().setProperty(textureName(2, 1), shader[2][1].getText());
					}
				}
			);
		colorPanel1.add(new JLabel(""));colorPanel1.add(new JLabel(""));
//		colorPanel1.add(shader[2][0]);
//		colorPanel1.add(shader[2][1]);

		// row 3
		//line for colors
		colorPanel1.add(new GreenLabJLabel((colorName(3).replaceAll("[\\(\\)]", "_"))));
		color1[3][0] = new GreenLabColorBox(colorName(3).replaceAll("[\\(\\)]", "_"), colorName(3), 0);
		color1[3][1] = new GreenLabColorBox(colorName(3).replaceAll("[\\(\\)]", "_"), colorName(3), 1);
		colorPanel1.add(color1[3][0]);
		colorPanel1.add(color1[3][1]);

		//second line for shaders
		colorPanel1.add(new GreenLabJLabel(""));
		shader[3][0] = new GreenLabTextField(textureName(3, 0));
		shader[3][1] = new GreenLabTextField(textureName(3, 1));
		((AbstractDocument)(shader[3][0].getDocument())).setDocumentFilter(
				new DocumentFilter() {			
				    public void remove(DocumentFilter.FilterBypass fb, int offset, int length)
				    	throws BadLocationException {
				    	//if empty write null
				    	if ((fb.getDocument().getLength()) == 1) {
				    		shader[3][0].setText("null");
				    		GreenLabPropertyFileReader.get().setProperty(textureName(3, 0), "null");
				    		color1[3][0].setEnabled(true);
				    	} else {
					    	if((fb.getDocument().getText(0, fb.getDocument().getLength()-1)).equals("null")) { 
								color1[3][0].setEnabled(true); 
							} else { 
								color1[3][0].setEnabled(false); 
							}
					    	super.remove(fb, offset, length);
					    	GreenLabPropertyFileReader.get().setProperty(textureName(3, 0), shader[3][0].getText());
				    	}
				    }

					public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a)
						throws BadLocationException {
						if((fb.getDocument().getText(0, fb.getDocument().getLength())+str).equals("null")) { 
							color1[3][0].setEnabled(true); 
						} else { 
							color1[3][0].setEnabled(false); 
						}
					    super.replace(fb, offs, length, str, a);
					    GreenLabPropertyFileReader.get().setProperty(textureName(3, 0), shader[3][0].getText());
					}
				}
			);
		((AbstractDocument)(shader[3][1].getDocument())).setDocumentFilter(
				new DocumentFilter() {			
				    public void remove(DocumentFilter.FilterBypass fb, int offset, int length)
				    	throws BadLocationException {
				    	//if empty write null
				    	if ((fb.getDocument().getLength()) == 1) {
				    		shader[3][1].setText("null");
				    		GreenLabPropertyFileReader.get().setProperty(textureName(3, 1), "null");
				    		color1[3][1].setEnabled(true);
				    	} else {
					    	if((fb.getDocument().getText(0, fb.getDocument().getLength()-1)).equals("null")) { 
								color1[3][1].setEnabled(true); 
							} else { 
								color1[3][1].setEnabled(false); 
							}
					    	super.remove(fb, offset, length);
					    	GreenLabPropertyFileReader.get().setProperty(textureName(3, 1), shader[3][1].getText());
				    	}
				    }

					public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a)
						throws BadLocationException {
						if((fb.getDocument().getText(0, fb.getDocument().getLength())+str).equals("null")) { 
							color1[3][1].setEnabled(true); 
						} else { 
							color1[3][1].setEnabled(false); 
						}
					    super.replace(fb, offs, length, str, a);
					    GreenLabPropertyFileReader.get().setProperty(textureName(3, 1), shader[3][1].getText());
					}
				}
			);
		colorPanel1.add(new JLabel(""));colorPanel1.add(new JLabel(""));
//		colorPanel1.add(shader[3][0]);
//		colorPanel1.add(shader[3][1]);

		return colorPanel1;
	}

	public void activeTab(int n) {
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			if (i <= n) {
				jtp.setEnabledAt(i, true);
			} else {
				jtp.setEnabledAt(i, false);
			}
		}
		// jtp.setSelectedIndex(n);
	}

	public void updateValue() {
		fieldLeafShape.setText(GreenLabPropertyFileReader.get().getProperty(
				"Leaf_shape(smb_L)"));
		fieldFemaleShape.setText(GreenLabPropertyFileReader.get().getProperty(
				"Female_shape(smb_Ff)"));
		fieldMaleShape.setText(GreenLabPropertyFileReader.get().getProperty(
				"Male_shape(smb_Fm)"));
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE - i; j++) {
				metaTextField[i][j][0].setValue(GreenLabPropertyFileReader
						.get().getProperty("Angle_Male(Ang_Fm)", i + 1, j + 1));
				metaTextField[i][j][1].setValue(GreenLabPropertyFileReader
						.get()
						.getProperty("Angle_Female(Ang_Ff)", i + 1, j + 1));
				metaTextField[i][j][2].setValue(GreenLabPropertyFileReader
						.get().getProperty("Angle_Blade(Ang_B)", i + 1, j + 1));
				metaTextField[i][j][3].setValue(GreenLabPropertyFileReader
						.get().getProperty("Angle_Axillary(Ang_A)", i + 1,
								j + 1));
			}
			for (int j = 0; j < GreenLabAtributes.GEOMETRY_PARAMETER_NUM; j++) {
				if (j == 8 || j == 11 || j == 15 || j == 16 || j == 17
						|| j == 19 || j == 20) {
					paraCheckBox[i][j].setTextValue(GreenLabPropertyFileReader
							.get().getProperty(paraName(j), i + 1));
				} else {
					paraTextField[i][j].setText(GreenLabPropertyFileReader
							.get().getProperty(paraName(j), i + 1));
				}
			}
		}
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			int indexJ = 11;
			int indexI = i;
			boolean v = paraCheckBox[indexI][indexJ].isSelected();
			for (int k = 12; k < 15; k++)
				paraTextField[indexI][k].setEditable(v);
		}
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++) {
				color1[i][j].update();
				shader[i][j].setText(GreenLabPropertyFileReader.get().getProperty(textureName(i, j)));
				
				if(GreenLabPropertyFileReader.get().getProperty(textureName(i, j)).equals("null")) {
					color1[i][j].setEnabled(true);
				} else {
					color1[i][j].setEnabled(false);
				}
			}
		}
		for (int i = 0; i < 6; i++) {
			color2[i].update();
		}
	}

	private String colorName(int n) {
		String str;
		switch (n) {
		case 0:
			str = "Leaf_color(Color_B)";
			break;
		case 1:
			str = "Internode_color(Color_I)";
			break;
		case 2:
			str = "Female_color(Color_Ff)";
			break;
		case 3:
			str = "Male_color(Color_Fm)";
			break;
		case 4:
			str = "Phy_Color(Color_P)";
			break;
		default:
			str = "";
			break;
		}
		return str;
	}

	private String textureName(int n, int m) {
		String str;
		switch (n) {
		case 0:
			str = "Leaf_texture";
			break;
		case 1:
			str = "Internode_texture";
			break;
		case 2:
			str = "Female_texture";
			break;
		case 3:
			str = "Male_texture";
			break;
		default:
			str = "";
			break;
		}
		return str+ ((m==0)?"_alive":"_dead");
	}

	private String paraName(int n) {
		String str;
		switch (n) {
		case 0:
			str = "Angle_Phyllotaxy(Ang_Ph)";
			break;
		case 1:
			str = "Young_Modulus(Ey)";
			break;
		case 2:
			str = "Force_point(fp)";
			break;
		case 3:
			str = "Terminal_Angle(Eng_Ang)";
			break;
		case 4:
			str = "Righting_point(Eng_N)";
			break;
		case 5:
			str = "InitialMin_angle(InitMin)";
			break;
		case 6:
			str = "InitialMax_angle(InitMax)";
			break;
		case 7:
			str = "InitialNum_angle(InitNum)";
			break;
		case 8:
			str = "Flag_Bending_by_node";
			break;
		case 9:
			str = "Max_Angle(wb)";
			break;
		case 10:
			str = "Openning_cycle(wbn)";
			break;
		case 11:
			str = "Organ(Flag_organ_bending)";
			break;
		case 12:
			str = "Bending_branch(Theta_B)";
			break;
		case 13:
			str = "Bending_leaf(Theta_L)";
			break;
		case 14:
			str = "Bending_fruit(Theta_F)";
			break;
		case 15:
			str = "Flag_plagiotropic";
			break;
		case 16:
			str = "Leaf_direction(Flag)";
			break;
		case 17:
			str = "Pruning(Flag_pruning)";
			break;
		case 18:
			str = "Delay(Pruning_delay)";
			break;
		case 19:
			str = "Leaf_display(Flag_leaf)";
			break;
		case 20:
			str = "Fruit_display(Flag_fruit)";
			break;
		case 21:
			str = "Internode_Volume(VI)";
			break;
		case 22:
			str = "Blade_Volume(VB)";
			break;
		case 23:
			str = "Fruit_Volume(VF)";
			break;
		case 24:
			str = "Internode_C(CI)";
			break;
		case 25:
			str = "Blade_C(CB)";
			break;
		case 26:
			str = "Fruit_C(CF)";
			break;
		default:
			str = "";
			break;
		}
		return str;
	}
}
