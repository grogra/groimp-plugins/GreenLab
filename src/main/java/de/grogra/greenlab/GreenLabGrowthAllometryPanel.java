/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.grogra.greenlab.conf.GreenLabAtributes;
import de.grogra.greenlab.conf.GreenLabPropertyFileReader;
import de.grogra.greenlab.ui.elements.GreenLabJLabel;
import de.grogra.greenlab.ui.elements.GreenLabNumberField;

/**
 * The Allometry Panel in Growth Panel
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabGrowthAllometryPanel extends JPanel {

	private static final long serialVersionUID = 1199462343518132612L;
	private JTabbedPane jtp;
	private JPanel pa[];
	private GreenLabNumberField allText[][];

	GreenLabGrowthAllometryPanel() {
		pa = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];

		jtp = new JTabbedPane();
		allText = new GreenLabNumberField[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][10];

		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			JPanel leftPanel = new JPanel(new BorderLayout());
			leftPanel.add(
					new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "allometrie_trans")),
					BorderLayout.NORTH);
			JPanel leftMainPanel1 = new JPanel(new GridLayout(2, 2));
			JPanel leftMainPanel2 = new JPanel(new GridLayout(2, 2));
			JPanel leftMainPanel3 = new JPanel(new GridLayout(3, 2));
			Box box = Box.createVerticalBox();
			box.add(leftMainPanel1);
			box.add(leftMainPanel2);
			box.add(leftMainPanel3);
			leftPanel.add(box);
			JPanel rightPanel = new JPanel(new GridLayout(3, 1));

			pa[i] = new JPanel(new BorderLayout());
			for (int j = 0; j < 10; j++) {
				if (j < 7) {
					allText[i][j] = new GreenLabNumberField(getGroupName(j)
							.replaceAll("[\\(\\)]", "_"));
					if (j < 2) {
						leftMainPanel1.add(new GreenLabJLabel((getGroupName(j)
								.replaceAll("[\\(\\)]", "_"))));
						leftMainPanel1.add(allText[i][j]);
					} else if (j < 4) {
						leftMainPanel2.add(new GreenLabJLabel((getGroupName(j)
								.replaceAll("[\\(\\)]", "_"))));
						leftMainPanel2.add(allText[i][j]);
					} else {
						leftMainPanel3.add(new GreenLabJLabel((getGroupName(j)
								.replaceAll("[\\(\\)]", "_"))));
						leftMainPanel3.add(allText[i][j]);
					}
				} else {
					JPanel tmpPanel = new JPanel(new GridLayout(2, 2));
					tmpPanel.add(new GreenLabJLabel(""));
					if (j == 7)
						tmpPanel.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(),	"femalefruit_trans"),
								SwingConstants.CENTER));
					else if (j == 8)
						tmpPanel.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "malefruit_trans"),
								SwingConstants.CENTER));
					else if (j == 9)
						tmpPanel.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "internode_trans"),
								SwingConstants.CENTER));
					JPanel tmpPanel1 = new JPanel(new BorderLayout());
					tmpPanel1.add(
							new GreenLabJLabel((getGroupName(j).replaceAll(
									"[\\(\\)]", "_"))), BorderLayout.NORTH);
					tmpPanel.add(tmpPanel1);
					allText[i][j] = new GreenLabNumberField(getGroupName(j)
							.replaceAll("[\\(\\)]", "_"));

					JPanel tmpPanel2 = new JPanel(new BorderLayout());
					tmpPanel2.add(allText[i][j], BorderLayout.NORTH);

					tmpPanel.add(tmpPanel2);

					rightPanel.add(tmpPanel);
				}
			}
			leftMainPanel1.setBorder(new TitledBorder(GreenLabGUI
					.getString("Allometry_Internode")));
			leftMainPanel2.setBorder(new TitledBorder(GreenLabGUI
					.getString("Allometry_Petiole")));
			leftMainPanel3.setBorder(new TitledBorder(GreenLabGUI
					.getString("Allometry_Blade")));
			leftPanel.setBorder(new TitledBorder(GreenLabGUI
					.getString("Allometry_Shape_Coefficient")));
			rightPanel.setBorder(new TitledBorder(GreenLabGUI
					.getString("Allometry_Density")));
			Box paMainPanel = Box.createHorizontalBox();
			paMainPanel.add(leftPanel);
			paMainPanel.add(rightPanel);
			pa[i].add(paMainPanel, BorderLayout.WEST);
			jtp.add(pa[i], GreenLabGUI.getString("PA") + (i + 1));
		}
		this.setLayout(new BorderLayout());
		this.add(jtp, BorderLayout.NORTH);
		addAction();
		updateValue();
	}

	public void updateValue() {
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			for (int j = 0; j < 10; j++) {
				allText[i][j].setValue(GreenLabPropertyFileReader.get()
						.getProperty(getGroupName(j), i + 1));
			}
		}
	}

	private void addAction() {
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			for (int j = 0; j < 10; j++) {
				final int fi = i, fj = j;
				allText[i][j].getDocument().addDocumentListener(
						new DocumentListener() {

							public void actionPerformed() {

								GreenLabPropertyFileReader.get().setProperty(
										getGroupName(fj),
										allText[fi][fj].getText(), fi + 1);
							}

							@Override
							public void changedUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void insertUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void removeUpdate(DocumentEvent e) {
								actionPerformed();
							}
						});
			}
		}
	}

	public void activeTab(int n) {
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			if (i <= n) {
				jtp.setEnabledAt(i, true);
			} else {
				jtp.setEnabledAt(i, false);
			}
		}
		// jtp.setSelectedIndex(n);
	}

	private String getGroupName(int i) {
		switch (i) {
		case 0:
			return "Allom_Internode(a_bI)";
		case 1:
			return "(a_aI)";
		case 2:
			return "Allom_Petiole(a_bP)";
		case 3:
			return "(a_aP)";
		case 4:
			return "Allom_Blade(a_bB)";
		case 5:
			return "(a_aB)";
		case 6:
			return "Allom_Thick(Th_B)";
		case 7:
			return "Female_Density(Den_Ff)";
		case 8:
			return "Male_Density(Den_Fm)";
		case 9:
			return "Internode_Dens(Den_I)";
		default:
			return null;
		}
	}
}
