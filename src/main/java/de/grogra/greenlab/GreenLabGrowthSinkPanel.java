/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.grogra.greenlab.conf.GreenLabAtributes;
import de.grogra.greenlab.conf.GreenLabPropertyFileReader;
import de.grogra.greenlab.ui.elements.GreenLabConstComboBox;
import de.grogra.greenlab.ui.elements.GreenLabJLabel;
import de.grogra.greenlab.ui.elements.GreenLabNumberField;

/**
 * The Sink Panel of Growth Panel
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabGrowthSinkPanel extends JPanel {

	private static final long serialVersionUID = 1199462343518132612L;
	private JTabbedPane jtp;
	private JPanel pa[];
	private JTextField paText1[][], paText2[][], groupText1[][],
			groupText2[][], groupText3[][], bText[];
	private GreenLabConstComboBox constCombo, layerDemandCombo, expanModeCombo,
			rootDemandCombo;

	GreenLabGrowthSinkPanel() {
		pa = new JPanel[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE];
		paText1 = new GreenLabNumberField[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][7];
		paText2 = new GreenLabNumberField[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][3];
		jtp = new JTabbedPane();
		String str[] = { GreenLabGUI.getString("sink_constant"),
				GreenLabGUI.getString("sink_variable"),
				GreenLabGUI.getString("from_file") };
		constCombo = new GreenLabConstComboBox("Flag_Time_expansion_version",
				str);
		String[] layerDemandStr = {
				GreenLabGUI.getString("Flag_TotalLayerDemand_Mode0"),
				GreenLabGUI.getString("Flag_TotalLayerDemand_Mode1"),
				GreenLabGUI.getString("Flag_TotalLayerDemand_Mode2") };
		layerDemandCombo = new GreenLabConstComboBox(
				"Flag_TotalLayerDemand_Mode", layerDemandStr);
		String[] expanModeStr = { GreenLabGUI.getString("Expansion_Mode1"),
				GreenLabGUI.getString("Expansion_Mode2"),
				GreenLabGUI.getString("Expansion_Mode3"),
				GreenLabGUI.getString("Expansion_Mode4") };
		expanModeCombo = new GreenLabConstComboBox("Expansion_Mode",
				expanModeStr, 1);
		String[] rootDemandStr = { GreenLabGUI.getString("Root_Demand_Mode1"),
				GreenLabGUI.getString("Root_Demand_Mode2") };
		rootDemandCombo = new GreenLabConstComboBox("Root_Demand_Mode",
				rootDemandStr, 1);
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			pa[i] = new JPanel(new BorderLayout());

			JPanel topPanel = new JPanel(new GridLayout(3, 8));
			for (int j = 0; j < 8; j++) {
				topPanel.add(new GreenLabJLabel(""));
			}
			topPanel.add(new GreenLabJLabel(("Sink_Strength")));
			for (int j = 0; j < 7; j++) {
				paText1[i][j] = new GreenLabNumberField("Sink_Strength");
				topPanel.add(paText1[i][j]);
			}
			for (int j = 0; j < 8; j++) {
				topPanel.add(new GreenLabJLabel(""));
			}

			JPanel paMainPanel = new JPanel(new GridLayout(3, 2));

			for (int j = 0; j < 3; j++) {
				if (j < 2)
					paText2[i][j] = new GreenLabNumberField(getGroup1Name(j)
							.replaceAll("[\\(\\)]", "_"));
				else
					paText2[i][j] = new GreenLabNumberField(getGroup1Name(j)
							.replaceAll("[\\(\\)]", "_"));
				paMainPanel.add(new GreenLabJLabel((getGroup1Name(j)
						.replaceAll("[\\(\\)]", "_"))));
				paMainPanel.add(paText2[i][j]);
			}
			paMainPanel.setBorder(new TitledBorder(GreenLabGUI
					.getString("Growth_Delay")));
			pa[i].add(topPanel, BorderLayout.NORTH);
			pa[i].add(paMainPanel, BorderLayout.WEST);
			jtp.add(pa[i], GreenLabGUI.getString("PA") + (i + 1));
		}

		JPanel picPanel = new JPanel(new GridLayout(1, 8));
		picPanel.add(new GreenLabJLabel(""));
		for (int j = 0; j < 7; j++) {
			picPanel.add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), ""
					+ getOrganType(j + 1).toLowerCase() + "_trans")));
		}

		JPanel mainPA = new JPanel(new GridLayout(14, 8));

		mainPA.add(new GreenLabJLabel(""));
		for (int j = 1; j < 8; j++) {
			mainPA.add(new GreenLabJLabel((getOrganType(j).replaceAll(
					"[\\(\\)]", "_")), SwingConstants.CENTER));
		}

		groupText1 = new GreenLabNumberField[4][7];
		groupText2 = new GreenLabNumberField[1][7];
		groupText3 = new GreenLabNumberField[4][6];
		mainPA.add(new GreenLabJLabel(("sink_constant"), SwingConstants.RIGHT));
		for (int j = 0; j < 7; j++) {
			mainPA.add(new GreenLabJLabel(""));
		}
		mainPA.add(new GreenLabJLabel(""));
		for (int j = 0; j < 7; j++) {
			groupText2[0][j] = new GreenLabNumberField("sink_constant", true);
			mainPA.add(groupText2[0][j]);
		}
		mainPA.add(new GreenLabJLabel(("sink_variable"), SwingConstants.RIGHT));
		for (int j = 0; j < 7; j++) {
			mainPA.add(new GreenLabJLabel(""));
		}

		for (int i = 0; i < 4; i++) {
			mainPA.add(new GreenLabJLabel((getVarName(i).replaceAll("[\\(\\)]",
					"_"))));
			for (int j = 0; j < 6; j++) {
				if (j == 5)
					mainPA.add(new GreenLabJLabel(""));
				groupText3[i][j] = new GreenLabNumberField("sink_variable",
						true);
				mainPA.add(groupText3[i][j]);
			}
		}
		mainPA.add(new GreenLabJLabel(("Beta_Law"), SwingConstants.RIGHT));
		for (int j = 0; j < 7; j++) {
			mainPA.add(new GreenLabJLabel(""));
		}
		for (int i = 0; i < 4; i++) {
			if (i == 2) {
				mainPA.add(new GreenLabJLabel(("Remobilization"),
						SwingConstants.RIGHT));
				for (int j = 0; j < 7; j++) {
					mainPA.add(new GreenLabJLabel(""));
				}
			}

			mainPA.add(new GreenLabJLabel((getGroup0Name(i).replaceAll(
					"[\\(\\)]", "_"))));
			for (int j = 0; j < 7; j++) {
				groupText1[i][j] = new GreenLabNumberField(getGroup0Name(i)
						.replaceAll("[\\(\\)]", "_"), true);
				mainPA.add(groupText1[i][j]);
			}
		}

		bText = new JTextField[8];
		for (int i = 0; i < 8; i++) {
			if (i == 0) {
			} else if (i < 3)
				bText[i] = new GreenLabNumberField(getFieldName(i).replaceAll(
						"[\\(\\)]", "_"), true);
			else if (i == 3)
				bText[i] = new GreenLabNumberField(getFieldName(i).replaceAll(
						"[\\(\\)]", "_"), true);
			else if (i == 4) {
			} else if (i == 5) {
			} else if (i == 6) {
				bText[i] = new GreenLabNumberField(getFieldName(i).replaceAll(
						"[\\(\\)]", "_"), true);
			} else
				bText[i] = new GreenLabNumberField(getFieldName(i).replaceAll(
						"[\\(\\)]", "_"), true);
		}

		JPanel paraPanelMain = new JPanel(new GridLayout(2, 2));
		JPanel paraPanelMainList[] = new JPanel[4];
		JPanel paraPanelMainTopList[] = new JPanel[4];
		for (int i = 0; i < 4; i++) {
			paraPanelMainTopList[i] = new JPanel(new GridLayout(2, 1));
			paraPanelMainList[i] = new JPanel(new GridLayout(4, 2));

			if (i == 0) {
				paraPanelMainTopList[i].add(new GreenLabJLabel(""));
				paraPanelMainTopList[i].setBorder(new TitledBorder(GreenLabGUI
						.getString("GrowthSinkTab1")));
			} else if (i == 1) {
				paraPanelMainTopList[i].add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(), "ring_trans")));
				paraPanelMainTopList[i].setBorder(new TitledBorder(GreenLabGUI
						.getString("Layer")));
			} else if (i == 2) {
				paraPanelMainTopList[i].add(new GreenLabJLabel(""));
				paraPanelMainTopList[i].setBorder(new TitledBorder(GreenLabGUI
						.getString("GrowthSinkTab2")));
			} else {
				paraPanelMainTopList[i].add(new GreenLabJLabel(GreenLabAtributes.getImageIcon(this.getClass(),"root_trans")));
				paraPanelMainTopList[i].setBorder(new TitledBorder(GreenLabGUI
						.getString("Root")));
			}
			paraPanelMainTopList[i].add(paraPanelMainList[i]);
			paraPanelMain.add(paraPanelMainTopList[i]);
		}
		int n = 0;
		paraPanelMainList[0].add(new GreenLabJLabel(
				("Flag_Time_expansion_version")));
		paraPanelMainList[0].add(constCombo);
		n = 4;
		paraPanelMainList[0].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainList[0].add(expanModeCombo);
		paraPanelMainList[0].add(new GreenLabJLabel(""));
		paraPanelMainList[0].add(new GreenLabJLabel(""));
		paraPanelMainList[0].add(new GreenLabJLabel(""));
		paraPanelMainList[0].add(new GreenLabJLabel(""));
		n = 0;
		paraPanelMainList[1].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainList[1].add(layerDemandCombo);
		n = 1;
		paraPanelMainList[1].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainList[1].add(bText[n]);
		n = 2;
		paraPanelMainList[1].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainList[1].add(bText[n]);
		n = 3;
		paraPanelMainList[1].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainList[1].add(bText[n]);
		n = 6;
		paraPanelMainList[2].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainList[2].add(bText[n]);
		n = 7;
		paraPanelMainList[2].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainList[2].add(bText[n]);

		paraPanelMainList[2].add(new GreenLabJLabel(""));
		paraPanelMainList[2].add(new GreenLabJLabel(""));
		paraPanelMainList[2].add(new GreenLabJLabel(""));
		paraPanelMainList[2].add(new GreenLabJLabel(""));
		n = 5;
		paraPanelMainList[3].add(new GreenLabJLabel((getFieldName(n)
				.replaceAll("[\\(\\)]", "_"))));
		paraPanelMainList[3].add(rootDemandCombo);
		paraPanelMainList[3].add(new GreenLabJLabel(""));
		paraPanelMainList[3].add(new GreenLabJLabel(""));
		paraPanelMainList[3].add(new GreenLabJLabel(""));
		paraPanelMainList[3].add(new GreenLabJLabel(""));
		paraPanelMainList[3].add(new GreenLabJLabel(""));
		paraPanelMainList[3].add(new GreenLabJLabel(""));

		Box box = Box.createVerticalBox();
		box.add(paraPanelMain);
		box.add(picPanel);
		box.add(mainPA);
		box.add(jtp);

		this.setLayout(new BorderLayout());
		this.add(box, BorderLayout.NORTH);

		constVisiable();
		addAction();
		updateValue();
	}

	public void activeTab(int n) {
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			if (i <= n) {
				jtp.setEnabledAt(i, true);
			} else {
				jtp.setEnabledAt(i, false);
			}
		}
		// jtp.setSelectedIndex(n);
	}

	private void constVisiable() {
		boolean v1 = constCombo.getPos() == 0;
		boolean v2 = constCombo.getPos() == 1;
		for (int j = 0; j < 7; j++) {
			groupText2[0][j].setEditable(v1);
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 6; j++) {
				groupText3[i][j].setEditable(v2);
			}
		}
	}

	public void updateValue() {
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			for (int j = 0; j < 7; j++) {
				paText1[i][j].setText(GreenLabPropertyFileReader.get()
						.getProperty(getGroup2Name(j), i + 1));
			}
			for (int j = 0; j < 3; j++) {
				paText2[i][j].setText(GreenLabPropertyFileReader.get()
						.getProperty(getGroup1Name(j), i + 1));
			}
		}
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 7; j++) {
				groupText1[i][j].setText(GreenLabPropertyFileReader.get()
						.getProperty(getGroup0Name(i), j + 1));
			}
		}
		for (int j = 0; j < 7; j++) {
			groupText2[0][j].setText(GreenLabPropertyFileReader.get()
					.getProperty("Time_Expansion(t_exp)", j + 1));
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 6; j++) {
				groupText3[i][j]
						.setText(GreenLabPropertyFileReader.get()
								.getProperty(
										(j == 5 ? "Root" : getOrganType(j + 1))
												+ "__6", i + 1));
			}
		}
		for (int i = 0; i < 8; i++) {
			if (i != 0 && i != 4 && i != 5)
				bText[i].setText(GreenLabPropertyFileReader.get().getProperty(
						getFieldName(i)));
		}
		constCombo.setText(Integer.parseInt(GreenLabPropertyFileReader.get()
				.getProperty("Flag_Time_expansion_version")));
		layerDemandCombo.setText(Integer.parseInt(GreenLabPropertyFileReader
				.get().getProperty("Flag_TotalLayerDemand_Mode")));
		expanModeCombo.setText(Integer.parseInt(GreenLabPropertyFileReader
				.get().getProperty("Expansion_Mode")));
		rootDemandCombo.setText(Integer.parseInt(GreenLabPropertyFileReader
				.get().getProperty("Root_Demand_Mode")));
	}

	private void addAction() {
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			for (int j = 0; j < 7; j++) {
				final int fi = i, fj = j;
				paText1[i][j].getDocument().addDocumentListener(
						new DocumentListener() {

							public void actionPerformed() {

								GreenLabPropertyFileReader.get().setProperty(
										getGroup2Name(fj),
										paText1[fi][fj].getText(), fi + 1);
							}

							@Override
							public void changedUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void insertUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void removeUpdate(DocumentEvent e) {
								actionPerformed();
							}

						});
			}
			for (int j = 0; j < 3; j++) {
				final int fi = i, fj = j;
				paText2[i][j].getDocument().addDocumentListener(
						new DocumentListener() {

							public void actionPerformed() {
								GreenLabPropertyFileReader.get().setProperty(
										getGroup1Name(fj),
										paText2[fi][fj].getText(), fi + 1);
							}

							@Override
							public void changedUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void insertUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void removeUpdate(DocumentEvent e) {
								actionPerformed();
							}

						});
			}
		}
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 7; j++) {
				final int fi = i, fj = j;
				groupText1[i][j].getDocument().addDocumentListener(
						new DocumentListener() {

							public void actionPerformed() {
								GreenLabPropertyFileReader.get().setProperty(
										getGroup0Name(fi),
										groupText1[fi][fj].getText(), fj + 1);
							}

							@Override
							public void changedUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void insertUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void removeUpdate(DocumentEvent e) {
								actionPerformed();
							}
						});
			}
		}
		for (int j = 0; j < 7; j++) {
			final int fj = j;
			groupText2[0][j].getDocument().addDocumentListener(
					new DocumentListener() {

						public void actionPerformed() {

							GreenLabPropertyFileReader.get().setProperty(
									"Time_Expansion(t_exp)",
									groupText2[0][fj].getText(), fj + 1);
						}

						@Override
						public void changedUpdate(DocumentEvent e) {
							actionPerformed();
						}

						@Override
						public void insertUpdate(DocumentEvent e) {
							actionPerformed();
						}

						@Override
						public void removeUpdate(DocumentEvent e) {
							actionPerformed();
						}

					});
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 6; j++) {
				final int fi = i, fj = j;
				groupText3[i][j].getDocument().addDocumentListener(
						new DocumentListener() {

							public void actionPerformed() {

								GreenLabPropertyFileReader.get().setProperty(
										fj == 5 ? "Root" : getOrganType(fj + 1)
												+ "__6",
										groupText3[fi][fj].getText(), fi + 1);
							}

							@Override
							public void changedUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void insertUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void removeUpdate(DocumentEvent e) {
								actionPerformed();
							}

						});
			}
		}
		for (int i = 0; i < 8; i++) {
			if (i != 0 && i != 4 && i != 5) {
				final int fi = i;
				bText[i].getDocument().addDocumentListener(
						new DocumentListener() {

							public void actionPerformed() {
								GreenLabPropertyFileReader.get().setProperty(
										getFieldName(fi), bText[fi].getText());
							}

							@Override
							public void changedUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void insertUpdate(DocumentEvent e) {
								actionPerformed();
							}

							@Override
							public void removeUpdate(DocumentEvent e) {
								actionPerformed();
							}
						});
			}
		}
		constCombo.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				constVisiable();
				GreenLabPropertyFileReader.get().setProperty(
						"Flag_Time_expansion_version", constCombo.getText());
			}
		});

		layerDemandCombo.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				GreenLabPropertyFileReader.get().setProperty(
						"Flag_TotalLayerDemand_Mode",
						layerDemandCombo.getText());
			}
		});
		expanModeCombo.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				GreenLabPropertyFileReader.get().setProperty("Expansion_Mode",
						(expanModeCombo.getPos()) + "");
			}
		});
		rootDemandCombo.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				GreenLabPropertyFileReader.get().setProperty(
						"Root_Demand_Mode", (rootDemandCombo.getPos()) + "");
			}
		});

	}

	private String getVarName(int i) {
		switch (i) {
		case 0:
			return "GC";
		case 1:
			return "min";
		case 2:
			return "GC";
		case 3:
			return "max";
		default:
			return null;
		}
	}

	private String getOrganType(int i) {
		switch (i) {
		case 0:
			return "";
		case 1:
			return "Blade";
		case 2:
			return "Petiole";
		case 3:
			return "Internode";
		case 4:
			return "Femalefruit";
		case 5:
			return "Malefruit";
		case 6:
			return "Layer";
		case 7:
			return "Root";
		default:
			return null;
		}
	}

	private String getFieldName(int i) {
		switch (i) {
		case 0:
			return "Flag_TotalLayerDemand_Mode";
		case 1:
			return "Global_Sink_Layer(Slay)";
		case 2:
			return "Global_Sink_Coefficient(Slope)";
		case 3:
			return "Layer_repartition_Coefficient(Lamda)";
		case 4:
			return "Expansion_Mode";
		case 5:
			return "Root_Demand_Mode";
		case 6:
			return "ShortInter_Number(N_SI)";
		case 7:
			return "ShortInter_RelaSink(kp_SI)";
		default:
			return null;
		}
	}

	private String getGroup0Name(int i) {
		switch (i) {
		case 0:
			return "Para_a(Bt_a)";
		case 1:
			return "Para_b(Bt_b)";
		case 2:
			return "Portion_Remobilization(P_Remob)";
		case 3:
			return "Speed_Remobilization(V_Remob)";
		default:
			return null;
		}
	}

	private String getGroup1Name(int i) {
		switch (i) {
		case 0:
			return "Number_Primordia(N_PM)";
		case 1:
			return "Sink_Primordia(S_PM)";
		case 2:
			return "Delay_Branch(Cp_B)";
		default:
			return null;
		}
	}

	private String getGroup2Name(int i) {
		switch (i) {
		case 0:
			return "Sink_Blade(S_B)";
		case 1:
			return "Sink_Petiole(S_P)";
		case 2:
			return "Sink_Internode(S_I)";
		case 3:
			return "Sink_Female(S_Ff)";
		case 4:
			return "Sink_Male(S_Fm)";
		case 5:
			return "Sink_Layer(S_L)";
		case 6:
			return "Sink_Root(S_R)";
		default:
			return null;
		}
	}

}
