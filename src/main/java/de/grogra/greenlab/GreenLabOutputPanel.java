/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import de.grogra.greenlab.conf.GreenLabAtributes;
import de.grogra.greenlab.conf.GreenLabPropertyFileReader;
import de.grogra.greenlab.ui.elements.GreenLabBooleanCheckBox;
import de.grogra.greenlab.ui.elements.GreenLabJLabel;

/**
 * The Output Panel
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabOutputPanel extends JPanel {

	private static final long serialVersionUID = -2073432007086110708L;

	private GreenLabBooleanCheckBox dataText[][] = new GreenLabBooleanCheckBox[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][GreenLabAtributes.OUTPUT_DATA_NUM];
	private GreenLabBooleanCheckBox dataAll[] = new GreenLabBooleanCheckBox[GreenLabAtributes.OUTPUT_DATA_NUM];
	private GreenLabBooleanCheckBox chartsText[][] = new GreenLabBooleanCheckBox[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][GreenLabAtributes.OUTPUT_CHART_NUM];
	private GreenLabBooleanCheckBox chartsAll[] = new GreenLabBooleanCheckBox[GreenLabAtributes.OUTPUT_CHART_NUM];
	private GreenLabBooleanCheckBox threedText[][] = new GreenLabBooleanCheckBox[GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE][GreenLabAtributes.OUTPUT_THREE_D_NUM];
	private GreenLabBooleanCheckBox threedAll[] = new GreenLabBooleanCheckBox[GreenLabAtributes.OUTPUT_THREE_D_NUM];

	GreenLabOutputPanel() {

		JPanel topPanelRight = new JPanel(new GridLayout(1,
				GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE + 2));
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			topPanelRight.add(new GreenLabJLabel(("PA") + (i + 1)));
		}
		topPanelRight.add(new GreenLabJLabel(""));
		topPanelRight.add(new GreenLabJLabel(("ALLPA")));

		JPanel dataPanel = new JPanel(new BorderLayout());
		dataPanel.setBorder(new TitledBorder(GreenLabGUI.getString(dataName(0)
				.replaceAll("[\\(\\)]", "_"))));
		JPanel dataPanelLeft = new JPanel(new GridLayout(
				GreenLabAtributes.OUTPUT_DATA_NUM + 1, 1));
		JPanel dataPanelRight = new JPanel(new GridLayout(
				GreenLabAtributes.OUTPUT_DATA_NUM + 1, 1));
		dataPanelLeft.add(new GreenLabJLabel(""));
		dataPanelRight.add(topPanelRight);
		for (int i = 0; i < GreenLabAtributes.OUTPUT_DATA_NUM; i++) {
			JPanel listPanel = new JPanel(new GridLayout(1,
					GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE + 2));
			if (GreenLabAtributes.OUTPUT_DATA_LIST_NUM[i] == 1) {
				for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE
						- GreenLabAtributes.OUTPUT_DATA_LIST_NUM[i] + 1; j++) {
					listPanel.add(new GreenLabJLabel(""));
				}
				JPanel tmp = new JPanel(new BorderLayout());
				tmp.add(new JSeparator(SwingConstants.VERTICAL),
						BorderLayout.CENTER);
				listPanel.add(tmp);
			}
			for (int j = 0; j < GreenLabAtributes.OUTPUT_DATA_LIST_NUM[i]; j++) {
				dataText[j][i] = new GreenLabBooleanCheckBox(dataName(i + 1)
						.replaceAll("[\\(\\)]", "_"));
				final int ii = i;
				final int jj = j;
				final GreenLabBooleanCheckBox tt = dataText[j][i];
				dataText[j][i].addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						GreenLabPropertyFileReader.get().setProperty(
								dataName(ii + 1), tt.getTextValue(), jj + 1);
					}
				});
				listPanel.add(dataText[j][i]);
			}
			if (GreenLabAtributes.OUTPUT_DATA_LIST_NUM[i] == GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE) {
				JPanel tmp = new JPanel(new BorderLayout());
				tmp.add(new JSeparator(SwingConstants.VERTICAL),
						BorderLayout.CENTER);
				listPanel.add(tmp);
				dataAll[i] = new GreenLabBooleanCheckBox(dataName(i + 1)
						.replaceAll("[\\(\\)]", "_"));
				listPanel.add(dataAll[i]);
				final int ii = i;
				dataAll[i].addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; j++) {
							if (dataText[j][ii].isEnabled()) {
								dataText[j][ii].setSelected(dataAll[ii]
										.isSelected());
								GreenLabPropertyFileReader.get().setProperty(
										dataName(ii + 1),
										dataText[j][ii].getTextValue(), j + 1);
							}
						}
					}
				});
			}
			dataPanelLeft.add(new GreenLabJLabel((dataName(i + 1).replaceAll(
					"[\\(\\)]", "_"))));
			dataPanelRight.add(listPanel);
			dataPanel.add(dataPanelLeft, BorderLayout.WEST);
			dataPanel.add(dataPanelRight, BorderLayout.EAST);
		}

		topPanelRight = new JPanel(new GridLayout(1,
				GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE + 2));
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			topPanelRight.add(new GreenLabJLabel(("PA") + (i + 1)));
		}
		topPanelRight.add(new GreenLabJLabel(""));
		topPanelRight.add(new GreenLabJLabel(("ALLPA")));

		JPanel chartsPanel = new JPanel(new BorderLayout());
		chartsPanel.setBorder(new TitledBorder(GreenLabGUI
				.getString(chartsName(0).replaceAll("[\\(\\)]", "_"))));
		JPanel chartsPanelLeft = new JPanel(new GridLayout(
				GreenLabAtributes.OUTPUT_CHART_NUM + 1, 1));
		JPanel chartsPanelRight = new JPanel(new GridLayout(
				GreenLabAtributes.OUTPUT_CHART_NUM + 1, 1));
		chartsPanelLeft.add(new GreenLabJLabel(""));
		chartsPanelRight.add(topPanelRight);
		for (int i = 0; i < GreenLabAtributes.OUTPUT_CHART_NUM; i++) {
			JPanel listPanel = new JPanel(new GridLayout(1,
					GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE + 2));
			if (GreenLabAtributes.OUTPUT_CHART_LIST_NUM[i] == 1) {
				for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE
						- GreenLabAtributes.OUTPUT_CHART_LIST_NUM[i] + 1; j++) {
					listPanel.add(new GreenLabJLabel(""));
				}
				JPanel tmp = new JPanel(new BorderLayout());
				tmp.add(new JSeparator(SwingConstants.VERTICAL),
						BorderLayout.CENTER);
				listPanel.add(tmp);
			}
			for (int j = 0; j < GreenLabAtributes.OUTPUT_CHART_LIST_NUM[i]; j++) {
				chartsText[j][i] = new GreenLabBooleanCheckBox(
						chartsName(i + 1).replaceAll("[\\(\\)]", "_"));
				final int ii = i;
				final int jj = j;
				final GreenLabBooleanCheckBox tt = chartsText[j][i];
				chartsText[j][i].addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						GreenLabPropertyFileReader.get().setProperty(
								chartsName(ii + 1), tt.getTextValue(), jj + 1);
					}
				});
				listPanel.add(chartsText[j][i]);
			}
			if (GreenLabAtributes.OUTPUT_CHART_LIST_NUM[i] == GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE) {
				JPanel tmp = new JPanel(new BorderLayout());
				tmp.add(new JSeparator(SwingConstants.VERTICAL),
						BorderLayout.CENTER);
				listPanel.add(tmp);
				chartsAll[i] = new GreenLabBooleanCheckBox(chartsName(i + 1)
						.replaceAll("[\\(\\)]", "_"));
				listPanel.add(chartsAll[i]);
				final int ii = i;
				chartsAll[i].addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; j++) {
							if (chartsText[j][ii].isEnabled()) {
								chartsText[j][ii].setSelected(chartsAll[ii]
										.isSelected());
								GreenLabPropertyFileReader.get()
										.setProperty(
												chartsName(ii + 1),
												chartsText[j][ii]
														.getTextValue(), j + 1);
							}
						}
					}
				});
			}
			chartsPanelLeft.add(new GreenLabJLabel((chartsName(i + 1)
					.replaceAll("[\\(\\)]", "_"))));
			chartsPanelRight.add(listPanel);
			chartsPanel.add(chartsPanelLeft, BorderLayout.WEST);
			chartsPanel.add(chartsPanelRight, BorderLayout.EAST);
		}

		topPanelRight = new JPanel(new GridLayout(1,
				GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE + 2));
		for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
			topPanelRight.add(new GreenLabJLabel(("PA") + (i + 1)));
		}
		topPanelRight.add(new GreenLabJLabel(""));
		topPanelRight.add(new GreenLabJLabel(("ALLPA")));

		JPanel threedPanel = new JPanel(new BorderLayout());
		threedPanel.setBorder(new TitledBorder(GreenLabGUI
				.getString(threedName(0).replaceAll("[\\(\\)]", "_"))));
		JPanel threedPanelLeft = new JPanel(new GridLayout(
				GreenLabAtributes.OUTPUT_THREE_D_NUM + 1, 1));
		JPanel threedPanelRight = new JPanel(new GridLayout(
				GreenLabAtributes.OUTPUT_THREE_D_NUM + 1, 1));
		threedPanelLeft.add(new GreenLabJLabel(""));
		threedPanelRight.add(topPanelRight);
		for (int i = 0; i < GreenLabAtributes.OUTPUT_THREE_D_NUM; i++) {
			JPanel listPanel = new JPanel(new GridLayout(1,
					GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE + 2));
			if (GreenLabAtributes.OUTPUT_THREE_D_LIST_NUM[i] == 1) {
				for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE
						- GreenLabAtributes.OUTPUT_THREE_D_LIST_NUM[i] + 1; j++) {
					listPanel.add(new GreenLabJLabel(""));
				}
				JPanel tmp = new JPanel(new BorderLayout());
				tmp.add(new JSeparator(SwingConstants.VERTICAL),
						BorderLayout.CENTER);
				listPanel.add(tmp);
			}
			for (int j = 0; j < GreenLabAtributes.OUTPUT_THREE_D_LIST_NUM[i]; j++) {
				threedText[j][i] = new GreenLabBooleanCheckBox(
						threedName(i + 1).replaceAll("[\\(\\)]", "_"));
				final int ii = i;
				final int jj = j;
				final GreenLabBooleanCheckBox tt = threedText[j][i];
				threedText[j][i].addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						GreenLabPropertyFileReader.get().setProperty(
								threedName(ii + 1), tt.getTextValue(), jj + 1);
					}
				});
				listPanel.add(threedText[j][i]);
			}
			if (GreenLabAtributes.OUTPUT_THREE_D_LIST_NUM[i] == GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE) {
				JPanel tmp = new JPanel(new BorderLayout());
				tmp.add(new JSeparator(SwingConstants.VERTICAL),
						BorderLayout.CENTER);
				listPanel.add(tmp);
				threedAll[i] = new GreenLabBooleanCheckBox(threedName(i + 1)
						.replaceAll("[\\(\\)]", "_"));
				listPanel.add(threedAll[i]);
				final int ii = i;
				threedAll[i].addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; j++) {
							if (threedText[j][ii].isEnabled()) {
								threedText[j][ii].setSelected(threedAll[ii]
										.isSelected());
								GreenLabPropertyFileReader.get()
										.setProperty(
												threedName(ii + 1),
												threedText[j][ii]
														.getTextValue(), j + 1);
							}
						}
					}
				});
			}
			threedPanelLeft.add(new GreenLabJLabel((threedName(i + 1)
					.replaceAll("[\\(\\)]", "_"))));
			threedPanelRight.add(listPanel);
			threedPanel.add(threedPanelLeft, BorderLayout.WEST);
			threedPanel.add(threedPanelRight, BorderLayout.EAST);
		}
		Box box = Box.createVerticalBox();
		box.add(dataPanel);
		box.add(chartsPanel);
		box.add(threedPanel);
		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(box, BorderLayout.NORTH);
		this.setLayout(new BorderLayout());
		this.add(mainPanel, BorderLayout.WEST);
		updateValue();
	}

	public void updateValue() {
		for (int i = 0; i < GreenLabAtributes.OUTPUT_DATA_NUM; i++) {
			for (int j = 0; j < GreenLabAtributes.OUTPUT_DATA_LIST_NUM[i]; j++) {
				dataText[j][i].setTextValue(GreenLabPropertyFileReader.get()
						.getProperty(dataName(i + 1), j + 1));
			}
		}
		for (int i = 0; i < GreenLabAtributes.OUTPUT_CHART_NUM; i++) {
			for (int j = 0; j < GreenLabAtributes.OUTPUT_CHART_LIST_NUM[i]; j++) {
				chartsText[j][i].setTextValue(GreenLabPropertyFileReader.get()
						.getProperty(chartsName(i + 1), j + 1));
			}
		}
		for (int i = 0; i < GreenLabAtributes.OUTPUT_THREE_D_NUM; i++) {
			for (int j = 0; j < GreenLabAtributes.OUTPUT_THREE_D_LIST_NUM[i]; j++) {
				threedText[j][i].setTextValue(GreenLabPropertyFileReader.get()
						.getProperty(threedName(i + 1), j + 1));
			}
		}
	}

	private String dataName(int n) {
		String str;
		switch (n) {
		case 0:
			str = "Data";
			break;
		case 1:
			str = "Flag_topo_disp";
			break;
		case 2:
			str = "Flag_topo_disp_total";
			break;
		case 3:
			str = "Flag_topo_disp_living";
			break;
		case 4:
			str = "Flag_topo_disp_living_aboveGU";
			break;
		case 5:
			str = "Flag_demand_biomass_layer";
			break;
		case 6:
			str = "Flag_demand_biomass_plant";
			break;
		case 7:
			str = "Flag_biomass_repartition";
			break;
		default:
			str = "";
			break;
		}
		return str;
	}

	private String chartsName(int n) {
		String str;
		switch (n) {
		case 0:
			str = "Charts";
			break;
		case 1:
			str = "Flag_biomass_fig";
			break;
		case 2:
			str = "Flag_biomassdemand_fig";
			break;
		case 3:
			str = "Flag_biomass_fig_accumulated";
			break;
		case 4:
			str = "Flag_disp_LAI";
			break;
		case 5:
			str = "Flag_organ_biomass_fig_phy";
			break;
		case 6:
			str = "Flag_organ_size_fig_phy";
			break;
		default:
			str = "";
			break;
		}
		return str;
	}

	private String threedName(int n) {
		String str;
		switch (n) {
		case 0:
			str = "_3D";
			break;
		case 1:
			str = "Flag_geometry";
			break;
		case 2:
			str = "Flag_geo_full";
			break;
		case 3:
			str = "Flag_geo_3D";
			break;
		case 4:
			str = "Flag_geo_leaf";
			break;
		case 5:
			str = "Flag_geo_fruit";
			break;
		case 6:
			str = "Flag_geo_skeleton";
			break;
		case 7:
			str = "Flag_geo_lig";
			break;
		case 8:
			str = "Flag_windows";
			break;
		default:
			str = "";
			break;
		}
		return str;
	}

	public void activeTab(int n) {
		for (int i = 0; i < GreenLabAtributes.OUTPUT_DATA_NUM; i++) {
			for (int j = 0; j < GreenLabAtributes.OUTPUT_DATA_LIST_NUM[i]; j++) {
				if (j <= n)
					dataText[j][i].setEnabled(true);
				else
					dataText[j][i].setEnabled(false);
			}
		}
		for (int i = 0; i < GreenLabAtributes.OUTPUT_CHART_NUM; i++) {
			for (int j = 0; j < GreenLabAtributes.OUTPUT_CHART_LIST_NUM[i]; j++) {
				if (j <= n)
					chartsText[j][i].setEnabled(true);
				else
					chartsText[j][i].setEnabled(false);
			}
		}
		for (int i = 0; i < GreenLabAtributes.OUTPUT_THREE_D_NUM; i++) {
			for (int j = 0; j < GreenLabAtributes.OUTPUT_THREE_D_LIST_NUM[i]; j++) {
				if (j <= n)
					threedText[j][i].setEnabled(true);
				else
					threedText[j][i].setEnabled(false);
			}
		}
	}
}
