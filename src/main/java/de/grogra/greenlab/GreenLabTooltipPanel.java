/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import de.grogra.greenlab.ui.elements.GreenLabJLabel;

/**
 * The Tooltip Panel shows additional information for a selected parameter.
 * Additional informations are e.g.: - description - parameter type (string,
 * int, real, boolean) - ranges (e.g.: [0, 1]) - measuring units (e.g.: m, g,
 * mol, ..)
 * 
 * @author mh, Cong Ding
 * 
 */
public class GreenLabTooltipPanel extends JPanel {

	private static final long serialVersionUID = -207343200110708L;

	private JTextArea descriptionArea = new JTextArea();
	private JTextArea typeArea = new JTextArea();
	private JTextArea rangeArea = new JTextArea();
	private JTextArea unitArea = new JTextArea();

	private final static Font KEY_WORT_FONT = new Font("LucidaSans",
			Font.PLAIN, 12);

	private final static Color BACKGROUND = UIManager
			.getColor("Button.background");

	GreenLabTooltipPanel() {
		// make panel for the additional informations
		JPanel t1 = new JPanel(new GridLayout(3, 1));
		GreenLabJLabel l1 = new GreenLabJLabel(("tooltip.type.Name"));
		l1.setFont(KEY_WORT_FONT);
		t1.add(l1);
		GreenLabJLabel l2 = new GreenLabJLabel(("tooltip.range.Name"));
		l2.setFont(KEY_WORT_FONT);
		t1.add(l2);
		GreenLabJLabel l3 = new GreenLabJLabel(("tooltip.unit.Name"));
		l3.setFont(KEY_WORT_FONT);
		t1.add(l3);

		JPanel t2 = new JPanel(new GridLayout(3, 1));
		t2.add(new GreenLabJLabel(" : "));
		t2.add(new GreenLabJLabel(" : "));
		t2.add(new GreenLabJLabel(" : "));

		typeArea.setEditable(false);
		rangeArea.setEditable(false);
		unitArea.setEditable(false);
		typeArea.setBackground(BACKGROUND);
		rangeArea.setBackground(BACKGROUND);
		unitArea.setBackground(BACKGROUND);

		JPanel t3 = new JPanel(new GridLayout(3, 1));
		typeArea.setColumns(25);
		rangeArea.setColumns(25);
		unitArea.setColumns(25);
		t3.add(typeArea);
		t3.add(rangeArea);
		t3.add(unitArea);

		JPanel t4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		t4.add(t1);
		t4.add(t2);
		t4.add(t3);

		JPanel t5 = new JPanel(new BorderLayout());
		t5.add(new JSeparator(SwingConstants.VERTICAL), BorderLayout.WEST);
		t5.add(t4, BorderLayout.CENTER);

		descriptionArea.setEditable(false);
		descriptionArea.setBackground(BACKGROUND);

		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(descriptionArea, BorderLayout.CENTER);
		mainPanel.add(t5, BorderLayout.EAST);
		mainPanel.setBorder(new TitledBorder(" "
				+ GreenLabGUI.getString("tooltip.Name")));

		setLayout(new BorderLayout());
		add(mainPanel, BorderLayout.CENTER);
	}

	public void updateValue(String keyString) {
		String str = keyString.replaceAll("\\s", "")
				.replaceAll("[\\(\\)]", "_");
		if (!GreenLabGUI.getString(str + ".description").equals(
				str + ".description")) {
			descriptionArea
					.setText(GreenLabGUI.getString(str + ".description"));
			typeArea.setText(GreenLabGUI.getString(GreenLabGUI.getString(str
					+ ".type")
					+ ".type.Name"));

			if (!GreenLabGUI.getString(str + ".range.min").equals(
					str + ".range.min")) {
				String min = GreenLabGUI.getString(str + ".range.min");
				String max = GreenLabGUI.getString(str + ".range.max");
				if (min.equals("MIN")) {
					min = "-\u221e";
				}
				if (max.equals("MAX")) {
					max = "+\u221e";
				}
				rangeArea.setText("[" + min + ", " + max + "]");
			} else {
				rangeArea.setText("");
			}
			unitArea.setText(GreenLabGUI.getString(str + ".unit"));
		}
	}
}
