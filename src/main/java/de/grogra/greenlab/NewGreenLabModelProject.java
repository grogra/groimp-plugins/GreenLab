/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import de.grogra.pf.io.FileSource;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;

/**
 * Main class, called when an new GreenLab project is opened.
 * 
 */
public class NewGreenLabModelProject  {

	public static void open (Item item, Object info, Context ctx) {		
		String path = ctx.getClass ().getProtectionDomain ().getCodeSource ().getLocation ().getPath();
		// decode the url to an ascii string (this is only needed on windows systems)
		// e.g. replace %20 by an single blank 
		try {
			path = URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} 
		
		//if an installed version is running, remove the project-jar file
		path = path.replace ("Platform.jar", "");
		//and for the "running out of eclipse version" change the paths
		path = path.replace ("Platform", "GreenLab");
		if(path.contains("build")) path = path.replaceAll ("build", "src");
		FileSource fs = FileSource.createFileSource (path+"de/grogra/greenlab/GreenLabModel.gsz", new MimeType ("application/x-grogra-project+zip", null), ctx.getWorkbench (), null);
		ctx.getWorkbench ().open (fs, new StringMap ().putBoolean (Workbench.START_AS_DEMO, true));		
	}
	
}
