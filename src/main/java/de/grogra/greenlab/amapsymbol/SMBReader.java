/* Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab.amapsymbol;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

/**
 * Class providing functionality to load SMB geometry data
 * 
 * The SMBObject describes a geometric object stored in an .smb file. The .smb
 * file format is used by the AMAPmod software.
 * 
 * In this implementation, the class {@link #PolygonMesh} is re-used to
 * represent an SMB object.
 * 
 * As .smb files use unsigned variable types not supported by Java, the
 * following Java variable types are used to represent the unsigned variable
 * types.
 * 
 * Java 'long' to represent C++ 'unsigned int32' Java 'int' to represent C++
 * 'unsigned int16' Java 'float' to represent C++ 'float'
 * 
 * Due to the use of unsigned variables, the SMB object can contain more
 * vertices than what the class {@link #PolygonMesh} can support. (Because
 * {@link #PolygonMesh} uses int indices for indexing the vertices.)
 * 
 * Considering that it is unlikely for the number of vertices to exceed the max
 * of Java's int, PolygonMesh is re-used. This also allows re-use of the mesh
 * calculation algorithms already existing in GroIMP.
 * 
 * @author yongzhi ong
 * 
 */
public class SMBReader {

	private PolygonMesh polygonMesh = null;

	private SMBByteReader byteReader;
	private int byteIndex;
	private byte fileContent[];

	private InputStream istream;
	private long fileLength;

	private class SMBTriFace {
		public int[] pointIndices;

		SMBTriFace(int firstIndex, int middleIndex, int lastIndex) {
			pointIndices = new int[3];
			pointIndices[0] = firstIndex;
			pointIndices[1] = middleIndex;
			pointIndices[2] = lastIndex;
		}
	}

	ArrayList<SMBTriFace> additionalFaces;

	public SMBReader(InputStream input, long length) {
		byteReader = new SMBByteReader();
		byteIndex = 0;

		istream = input;
		fileLength = length;

		additionalFaces = new ArrayList<SMBTriFace>();

		try {
			loadPolygonMesh();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public SMBReader(InputStream input) {
		byteReader = new SMBByteReader();
		byteIndex = 0;

		fileLength = 0;
		int x;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			while ((x = input.read()) > -1) {
				fileLength++;
				baos.write(x);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		istream = new ByteArrayInputStream(baos.toByteArray());
		additionalFaces = new ArrayList<SMBTriFace>();

		try {
			loadPolygonMesh();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Uses SMBByteReader to read 4 bytes unsigned int from byte array.
	 * 
	 * @return long representing 4 byte unsigned int
	 */
	private long readUnsignedInt32() {
		long unsignedInt = 0;
		unsignedInt = byteReader.readUnsignedInt32(fileContent, byteIndex);
		byteIndex += 4;

		return unsignedInt;
	}

	/**
	 * Uses SMBByteReader to read 2 bytes unsigned int from byte array.
	 * 
	 * @return int representing 2 byte unsigned int
	 */
	private int readUnsignedInt16() {
		int unsignedInt = 0;
		unsignedInt = byteReader.readUnsignedInt16(fileContent, byteIndex);
		byteIndex += 2;

		return unsignedInt;
	}

	/**
	 * Uses SMBByteReader to read 4 bytes float from byte array.
	 * 
	 * @return float representing 4 byte float
	 */
	private float readFloat32() {
		float f;
		f = byteReader.readFloat32(fileContent, byteIndex);
		byteIndex += 4;

		return f;
	}

	/**
	 * Loads SMB data from file into PolygonMesh member variable.
	 * 
	 * @throws IOException
	 */
	private void loadPolygonMesh() throws IOException {
		FloatList vertexData;
		IntList indexData;
		float[] normalData;
		float[] textureData;

		try {
			// byte array for containing binary contents
			fileContent = new byte[(int) fileLength];

			// read binary data from file into memory array
			istream.read(fileContent);

			// read number of faces and vertices/points in SMB file
			long _facesCount = readUnsignedInt32();
			long _pointsCount = readUnsignedInt32();

			vertexData = new FloatList((int) (_pointsCount) * 3);
			indexData = new IntList((int) (_facesCount) * 3 /** 2*/); //* 2 if drawing both triangle order
			normalData = new float[(int) _pointsCount * 3];
			textureData = new float[(int) _pointsCount * 2];

			// FOR DEBUG
			// System.out.println("facesCount:" + _facesCount + "\n");
			// System.out.println("pointsCount:" + _pointsCount + "\n");
			// END DEBUG

			// read faces information
			for (long _i = 0; _i < _facesCount; _i++) {
				long _indicesCount = readUnsignedInt32();

				/* int _faceInfo1 = */readUnsignedInt16(); // unused value,
															// therefore not
															// assigned to
															// variable. To
															// assign if
															// required.
				/* int _faceInfo2 = */readUnsignedInt16(); // unused value,
															// therefore not
															// assigned to
															// variable. To
															// assign if
															// required.

				// FOR DEBUG
				// System.out.println("indicesCount:" + _indicesCount + "\n");
				// END DEBUG
				long[] pointIndices = new long[(int) _indicesCount];
				long _pointIndex = 0;
				for (long _j = 0; _j < _indicesCount; _j++) {
					/* long _pointColor = */readUnsignedInt32(); // unused
																	// value,therefore
																	// not
																	// assigned
																	// to
																	// variable.To
																	// assign if
																	// required.
					_pointIndex = readUnsignedInt32();
					/* int _pointAttribute = */readUnsignedInt16(); // unused
																	// value,
																	// therefore
																	// not
																	// assigned
																	// to
																	// variable.
																	// To assign
																	// if
																	// required.

					// FOR DEBUG
					// System.out.println("pointIndex:" + _pointIndex + "\n");
					// END DEBUG
					
					//clockwise  indices inserted
//					if (_j < 3)
//						indexData.push((int) _pointIndex);
					pointIndices[(int) _j] = _pointIndex;
				}
				
				//counter-clockwise indices inserted
				indexData.push((int) pointIndices[0]);
				indexData.push((int) pointIndices[2]);
				indexData.push((int) pointIndices[1]);
				
				// if more than 3 indices in face, perform simple triangulation
				// of the rest of the indices
				if (_indicesCount >= 4) {
					int currentIndex = 2;
					int first = indexData.get(indexData.size() - 3);
					long middle = 0;
					long last = 0;

					while ((currentIndex + 1) <= (_indicesCount - 1)) {
						
						//clockwise indices inserted
//						middle = pointIndices[currentIndex + 1];
//						last = pointIndices[currentIndex];
//
//						additionalFaces.add(new SMBTriFace(first, (int) middle,
//								(int) last));

						//counter-clockwise indices inserted
						middle = pointIndices[currentIndex];
						last = pointIndices[currentIndex + 1];

						additionalFaces.add(new SMBTriFace(first, (int) middle,
								(int) last));

						currentIndex++;
					}
				}
			}

			// read vertex/point normal,position and texture coordinate values
			float _x = 0, _y = 0, _z = 0;
			float _nx = 0, _ny = 0, _nz = 0;
			@SuppressWarnings("unused")
			float _u = 0, _v = 0, _d = 0;

			for (long _j = 0; _j < _pointsCount; _j++) {
				_nx = readFloat32();
				_ny = readFloat32();
				_nz = readFloat32();

				_x = readFloat32();
				_y = readFloat32();
				_z = readFloat32();

				_u = readFloat32();
				_v = readFloat32();
				_d = readFloat32();

				vertexData.push(_x, _y, _z);

				normalData[(int) _j * 3] = _nx;
				normalData[(int) _j * 3 + 1] = _ny;
				normalData[(int) _j * 3 + 2] = _nz;

				textureData[(int) _j * 2] = _u;
				textureData[(int) _j * 2 + 1] = _v;

				// FOR DEBUG
				// System.out.println("nx:" + _nx + "\n");
				// System.out.println("ny:" + _ny + "\n");
				// System.out.println("nz:" + _nz + "\n");
				//
				// System.out.println("x:" + _x + "\n");
				// System.out.println("y:" + _y + "\n");
				// System.out.println("z:" + _z + "\n");
				//
				// System.out.println("u:" + _u + "\n");
				// System.out.println("v:" + _v + "\n");
				// System.out.println("d:" + _d + "\n");
				// END DEBUG
			}

			if (additionalFaces.size() > 0) {
				// copy index data to another temp list
				IntList temp = new IntList((int) (_facesCount) * 3 * 2);
				for (int i = 0; i < indexData.size(); ++i)
					temp.push(indexData.get(i));

				// reset size of indexData with additional faces
				indexData.clear();
				indexData = new IntList(
						(int) ((_facesCount * 3 * 2) + additionalFaces.size()));

				// copy back original faces
				for (int i = 0; i < temp.size(); ++i)
					indexData.push(temp.get(i));

				// push in additional faces
				for (int i = 0; i < additionalFaces.size(); ++i) {
					SMBTriFace extraFace = additionalFaces.get(i);
					indexData.push(extraFace.pointIndices[0]);
					indexData.push(extraFace.pointIndices[1]);
					indexData.push(extraFace.pointIndices[2]);
				}
			}

			polygonMesh = new PolygonMesh();
			polygonMesh.setIndexData(indexData);
			polygonMesh.setNormalData(normalData);
			polygonMesh.setTextureData(textureData);
			polygonMesh.setVertexData(vertexData);
		} catch (FileNotFoundException e) {
			System.out.println("File not found" + e);
		} catch (IOException ioe) {
			System.out.println("Exception while reading the file " + ioe);
		} finally {
			if (istream != null)
				istream.close();
			System.gc();
		}
	}

	public Node getObeject() {
		MeshNode mnode = new MeshNode(getPolygonMesh());
		//mnode.setVisibleSides(Attributes.VISIBLE_SIDES_FRONT);
		mnode.setVisibleSides(Attributes.VISIBLE_SIDES_BOTH);
		return mnode;
	}

	public PolygonMesh getPolygonMesh() {
		return polygonMesh;
	}
}
