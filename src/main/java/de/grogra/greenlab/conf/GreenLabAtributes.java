package de.grogra.greenlab.conf;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;

import javax.swing.ImageIcon;

public final class GreenLabAtributes {

	public static final int MAX_PHYSIOLOGICAL_AGE = 6;

	public static final int MAX_RGB_VALUE = 255;

	public final static int TOPO_META_NUM = 6;
	public final static int TOPO_PARA_NUM = 13;

	public final static int DOUBLE_DIGITS = 10;

	public static final int ENVIRONMENT_SOIL_NUM = 10,
			ENVIRONMENT_TEMP_NUM = 8, ENVIRONMENT_LIGHT_NUM = 6;

	private static final String IMAGE_PATH = "img/";

	public static final int GEOMETRY_PARAMETER_NUM = 27;

	public static final int OUTPUT_DATA_NUM = 7, OUTPUT_CHART_NUM = 6,
			OUTPUT_THREE_D_NUM = 8 - 2;
	public static final int[] OUTPUT_DATA_LIST_NUM = { 6, 1, 6, 6, 1, 1, 1, 6 },
			OUTPUT_CHART_LIST_NUM = { 1, 1, 1, 1, 6, 6 },
			OUTPUT_THREE_D_LIST_NUM = { 1, 1, 1, 1, 1, 6, 1, 1 };

	public static ImageIcon getImageIcon(@SuppressWarnings("rawtypes") Class myClass, String keyString) {
		// TODO: find a better way to check whether GroIMP is running out of eclipse or an installed version
		// and adapt the paths  
		
		String path = myClass.getProtectionDomain ().getCodeSource ().getLocation ().getPath();
		
		//if the path contains the string ".jar" an installed version is running
		// and so the path are different comparing to run it out of eclipse
		if(path.contains(".jar")) {
			// decode the url to an ascii string (this is only needed on windows systems)
			// e.g. replace %20 by an single blank 
			try {
				path = URLDecoder.decode(path, "UTF-8");
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}

			path = path.replace ("GreenLab.jar", IMAGE_PATH);
			File file = new File(path + keyString +".gif");
			URL url = null;
			try {
				url = file.toURI().toURL();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			if(url==null) return null;
			return new ImageIcon(url);	
		}
		return new ImageIcon(myClass.getClassLoader().getResource(IMAGE_PATH + keyString +".gif"));
	}
}
