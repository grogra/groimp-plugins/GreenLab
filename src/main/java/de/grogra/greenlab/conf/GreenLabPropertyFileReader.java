/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab.conf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import de.grogra.pf.io.FileSource;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Context;
import de.grogra.vfs.MemoryFileSystem;

/**
 * The Property File Reader this class reads the *.sci file and store as a
 * HashMap, and is updated on time.
 * 
 * this class outputs the parameters to *.rgg file
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabPropertyFileReader {

	private HashMap<String, String> backup = null;
	private HashMap<String, String> properties = null;

	private File inFile = null;
	private Context ctx = null;

	private static GreenLabPropertyFileReader greenLabPropertyFileReader;

	public static synchronized GreenLabPropertyFileReader get() {
		if (greenLabPropertyFileReader == null) {
			greenLabPropertyFileReader = new GreenLabPropertyFileReader();
		}
		return greenLabPropertyFileReader;
	}

	@SuppressWarnings("unchecked")
	public void reset() {
		if (backup != null) {
			properties.clear();
			properties = (HashMap<String, String>) backup.clone();
		}
	}
	
	public void setFile(final Context ctx) {
		init();
		this.ctx = ctx;
		inFile = null;
		load();
	}

	public void setFile(String fileName) {
		init();
		inFile = new File(fileName);
		ctx = null;
		load();
	}

	public void setFile(FileSource fs) {
		init();
		inFile = fs.getInputFile();
		ctx = null;
		load();
	}

	// initial
	private void init() {
		properties = new HashMap<String, String>();
	}

	@SuppressWarnings("unchecked")
	private boolean load(InputStream fis) {
		try {
			BufferedReader in;
			String buf;
			in = new BufferedReader(new InputStreamReader(fis));
			// store the property name of the former property
			String proName = null;
			// read one more line
			while ((buf = in.readLine()) != null) {
				// if this line starts with number, it is connected to the
				// former line
				if (buf.matches("^\\s*-?\\d.*$")) {
					// remove unused characters, such as comments
					String split[] = buf.split("[^ \ta-zA-Z0-9_()\\-]\\.");
					String str = split[0].replaceAll("^\\s+", "")
							.replaceAll("\\s*$", "").replaceAll("\\s+", " ");
					// check whether it is the first line
					if (proName != null) {
						String value = properties.get(proName);
						// if no value for the former property name
						if (value == null) {
							value = str;
							// if the value for the former property name is not
							// null, we connect the value and the new line
						} else {
							value = value + " " + str;
						}
						properties.put(proName, value);
					}
					// if this line starts with [a-zA-Z], it is a new properity
				} else if (buf.matches("^\\s*[a-zA-Z(].*$")) {
					// remove unused characters, such as comments
					String split[] = buf.split("[^ \ta-zA-Z0-9_()\\.\\-]");
					String str = split[0].replaceAll("^\\s+", "")
							.replaceAll("\\s*$", "").replaceAll("\\s+", " ");
					// split the properity name and value
					int spPos = str.indexOf(" ");
					String value;
					// if this line contains value
					if (spPos > 0) {
						proName = str.substring(0, spPos);
						value = str.substring(spPos + 1);
						// check whether this key is used before, e.g., same key
						// name in different age
						// if the key exists before, change all the names to be
						// ***__k form
						if (properties.containsKey(proName)) {
							if (properties.containsKey(proName + "__1")) {
								int pos = 0;
								for (int i = 2; i <= 6; i++) {
									if (!properties.containsKey(proName + "__"
											+ i)) {
										pos = i;
										break;
									}
								}
								if (pos != 0) {
									properties.put(proName + "__" + pos, value);
									proName = proName + "__" + pos;
								}
							} else {
								properties.put(proName + "__1",
										properties.get(proName));
								properties.put(proName, "");
								properties.put(proName + "__2", value);
								proName = proName + "__2";
							}
						} else {
							properties.put(proName, value);
						}
						// if this line does not contain value, just set proName
					} else {
						proName = str;
					}
				} else {
				}
			}
			in.close();
		} catch (IOException exc) {
			System.out.println("Error during reading a property file: " + exc);
			return false;
		}
		if (backup == null) {
			backup = (HashMap<String, String>) properties.clone();
		}
		return true;
	}

	// load properties file
	private boolean load() {
		if (inFile == null) {
			Registry r = ctx.getWorkbench().getRegistry();
			MemoryFileSystem fs = (MemoryFileSystem) r.getFileSystem();
			InputStream ins = fs.getInputStream(fs.getFile("maize.sci"));
			return load(ins);
		} else {
			try {
				return load(new FileInputStream(inFile));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			return false;
		}
	}

	public String getProperty(String key, int age, int pos) {
		if (pos > 7 - age || pos < 1 || age < 1 || age > 6) {
			return null;
		} else {
			// exchange age and pos to position
			return getProperty(key, age * (15 - age) / 2 - 7 + pos);
		}
	}

	// get properties in age
	public String getProperty(String key, int pos) {
		String sp[] = properties.get(key).split("\\s+");
		if (pos <= sp.length && pos >= 1) {
			return sp[pos - 1];
		} else {
			return null;
		}
	}

	// get properties in age 0
	public String getProperty(String key) {
		return properties.get(key);
	}

	// get properties variable
	public HashMap<String, String> getProperties() {
		return properties;
	}

	// change a value in an age
	public boolean setProperty(String key, String value, int age, int pos) {
		if (pos > 7 - age || pos < 1 || age < 1 || age > 6) {
			return false;
		}
		// exchange age and pos to position
		return setProperty(key, value, age * (15 - age) / 2 - 7 + pos);
	}

	// change a value in an array
	public boolean setProperty(String key, String value, int pos) {
		value = value.replaceAll(",", ".");
		if (key.equals("") || value.equals(""))
			return false;
		if (pos == 0) {
			properties.put(key, value);
			return true;
		} else if (pos > 0) {
			String oValue = properties.get(key);
			String[] oValueSplit = oValue.split("\\s+");
			if (pos > oValueSplit.length) {
				return false;
			}
			oValueSplit[pos - 1] = value;
			String iValue = oValueSplit[0];
			for (int i = 1; i < oValueSplit.length; i++) {
				iValue += " " + oValueSplit[i];
			}
			properties.put(key, iValue);
			return true;
		} else {
			return false;
		}
	}

	// change a value
	public boolean setProperty(String key, String value) {
		value = value.replaceAll(",", ".");
		if (properties.containsKey(key)
				&& properties.get(key).split("\\s+").length == value
						.split("\\s+").length) {
			return setProperty(key, value, 0);
		} else if (!properties.containsKey(key)) {
			return setProperty(key, value, 0);
		} else {
			System.err.println(properties.get(key) + "\t" + key + "\t" + value);
			return false;
		}
	}

	private String printList(String str) {
		String[] sp = str.split("\\s+");
		String ret = "{";
		for (int i = 0; i < sp.length; i++) {
			ret += sp[i];
			if (i != sp.length - 1) {
				ret += ", ";
			}
		}
		ret += "}";
		return ret;
	}

	private String printBool(String str) {
		return str.equals("1") ? "true" : "false";
	}

	private String printBoolList(String str) {
		String[] sp = str.split("\\s+");
		String ret = "{";
		for (int i = 0; i < sp.length; i++) {
			ret += sp[i].equals("1") ? "true" : "false";
			if (i != sp.length - 1) {
				ret += ", ";
			}
		}
		ret += "}";
		return ret;
	}

	private String printAges(String str) {
		String[] sp = str.split("\\s+");
		String ret = "{";
		int k = 0;
		for (int i = 0; i < 6; i++) {
			ret += "{";
			for (int j = 0; j < 6; j++) {
				if (j < i) {
					ret += "0";
				} else {
					ret += sp[k];
					k++;
				}
				if (j != 5)
					ret += ", ";

			}
			if (i == 5)
				ret += "}";
			else
				ret += "}, ";
		}
		ret += "}";
		return ret;
	}

	private String printSp(String str, int it, int jt) {
		String[] sp = str.split("\\s+");
		String ret = "{";
		int k = 0;
		for (int i = 0; i < it; i++) {
			ret += "{";
			for (int j = 0; j < jt; j++) {
				ret += sp[k];
				k++;
				if (j != jt - 1)
					ret += ", ";

			}
			if (i == it - 1)
				ret += "}";
			else
				ret += "}, ";
		}
		ret += "}";
		return ret;
	}

	private String printPhyAge(int age) {
		String ret = "{";
		String[] v;
		v = new String[5];
		v[0] = printList(getProperty("Time_Function(Tu_O(1))__" + age));
		v[1] = printList(getProperty("Time_appearance(2)__" + age));
		v[2] = printList(getProperty("Time_disappearance(3)__" + age));
		v[3] = printList(getProperty("Time_busy(4)__" + age));
		v[4] = printList(getProperty("Time_idle(5)__" + age));
		for (int j = 0; j < v.length; j++) {
			ret += v[j];
			if (j != v.length - 1)
				ret += ", ";
		}
		ret += "}";
		return ret;
	}

	private String printTimeFunc(int age) {
		String ret = "{";
		String[] v;
		v = new String[6];
		v[0] = printList(getProperty("Blade__" + age));
		v[1] = printList(getProperty("Petiole__" + age));
		v[2] = printList(getProperty("Internode__" + age));
		v[3] = printList(getProperty("Femalefruit__" + age));
		v[4] = printList(getProperty("Malefruit__" + age));
		v[5] = printList(getProperty("Root__" + age));
		for (int j = 0; j < v.length; j++) {
			ret += v[j];
			if (j != v.length - 1)
				ret += ", ";
		}
		ret += "}";
		return ret;
	}

	private String printSink() {
		String ret = "{";
		String[] v;
		v = new String[7];
		v[0] = printList(getProperty("Sink_Blade(S_B)"));
		v[1] = printList(getProperty("Sink_Petiole(S_P)"));
		v[2] = printList(getProperty("Sink_Internode(S_I)"));
		v[3] = printList(getProperty("Sink_Female(S_Ff)"));
		v[4] = printList(getProperty("Sink_Male(S_Fm)"));
		v[5] = printList(getProperty("Sink_Layer(S_L)"));
		v[6] = printList(getProperty("Sink_Root(S_R)"));
		for (int j = 0; j < v.length; j++) {
			ret += v[j];
			if (j != v.length - 1)
				ret += ", ";
		}
		ret += "}";
		return ret;
	}

	private String printSci(String str) {
		String[] sp = str.split(" ");
		String ret = "";
		if (sp.length == 21) {
			int cnt = 0;
			for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
				if (i != 0)
					for (int j = 0; j < i + 8; j++) {
						ret += "  ";
					}
				for (int j = 0; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE - i; j++) {
					ret += sp[cnt] + " ";
					cnt++;
				}
				ret += "\n";
			}
		} else {
			ret = str;
		}
		return ret;
	}

	private String printSci(String str, int pos) {
		String[] sp = str.split(" ");
		String ret = "\n";
		int cnt = 0;
		for (int i = 0; i < sp.length / pos; i++) {
			for (int j = 0; j < pos; j++) {
				ret += sp[cnt] + " ";
				cnt++;
			}
			ret += "\n";
		}
		return ret;
	}

	public String getSci() {
		String ret = "";
		ret += " \n";
		ret += "Chr_Age(N) " + printSci(getProperty("Chr_Age(N)")) + "\n";
		ret += "Phy_Age(Maxp) " + printSci(getProperty("Phy_Age(Maxp)")) + "\n";
		ret += "Sample_Size(Tr) " + printSci(getProperty("Sample_Size(Tr)"))
				+ "\n";
		ret += " \n";
		ret += "Axillary_Num(Nu_A) "
				+ printSci(getProperty("Axillary_Num(Nu_A)")) + "\n";
		ret += "Leaf_Num(Nu_B) " + printSci(getProperty("Leaf_Num(Nu_B)"))
				+ "\n";
		ret += "Female_Num(Nu_Ff) "
				+ printSci(getProperty("Female_Num(Nu_Ff)")) + "\n";
		ret += "Male_Num(Nu_Fm) " + printSci(getProperty("Male_Num(Nu_Fm)"))
				+ "\n";
		ret += "Micro_Num(Nu_I) " + printSci(getProperty("Micro_Num(Nu_I)"))
				+ "\n";
		ret += " \n";
		ret += "Flag_Nu_Ma_version "
				+ printSci(getProperty("Flag_Nu_Ma_version")) + "\n";
		ret += "Macro_Num(Nu_Ma) " + printSci(getProperty("Macro_Num(Nu_Ma)"))
				+ "\n";
		ret += " \n";
		ret += "Struc_Jump(st_j) " + printSci(getProperty("Struc_Jump(st_j)"))
				+ "\n";
		ret += "Reiter_Order(b_o) "
				+ printSci(getProperty("Reiter_Order(b_o)")) + "\n";
		ret += "Bran_Control(br_a) "
				+ printSci(getProperty("Bran_Control(br_a)")) + "\n";
		ret += "Reiter_Control(re_a) "
				+ printSci(getProperty("Reiter_Control(re_a)")) + "\n";
		ret += "Rest_Func(rs_A) " + printSci(getProperty("Rest_Func(rs_A)"))
				+ "\n";
		ret += "Rest_Func(rs_B) " + printSci(getProperty("Rest_Func(rs_B)"))
				+ "\n";
		ret += "rythm_Ratio(rt_a) "
				+ printSci(getProperty("rythm_Ratio(rt_a)")) + "\n";
		ret += " \n";
		ret += "branching_proba(a) "
				+ printSci(getProperty("branching_proba(a)")) + "\n";
		ret += "Growth_macro(b) " + printSci(getProperty("Growth_macro(b)"))
				+ "\n";
		ret += "Growth_micro(bu) " + printSci(getProperty("Growth_micro(bu)"))
				+ "\n";
		ret += "Survive(c) " + printSci(getProperty("Survive(c)")) + "\n";
		ret += " \n";
		ret += "%***********************Hydro*********************\n";
		ret += "%OrganType(id)--------Blade-Petiel-Internode-FemaleFlower-MaleFlower-Ring-Root\n";
		ret += "%phy_age=1\n";
		ret += "Time_Function(Tu_O(1)) "
				+ printSci(getProperty("Time_Function(Tu_O(1))__1")) + "\n";
		ret += "Time_appearance(2) "
				+ printSci(getProperty("Time_appearance(2)__1")) + "\n";
		ret += "Time_disappearance(3) "
				+ printSci(getProperty("Time_disappearance(3)__1")) + "\n";
		ret += "Time_busy(4) " + printSci(getProperty("Time_busy(4)__1"))
				+ "\n";
		ret += "Time_idle(5) " + printSci(getProperty("Time_idle(5)__1"))
				+ "\n";
		ret += "%phy_age=2\n";
		ret += "Time_Function(Tu_O(1)) "
				+ printSci(getProperty("Time_Function(Tu_O(1))__2")) + "\n";
		ret += "Time_appearance(2) "
				+ printSci(getProperty("Time_appearance(2)__2")) + "\n";
		ret += "Time_disappearance(3) "
				+ printSci(getProperty("Time_disappearance(3)__2")) + "\n";
		ret += "Time_busy(4) " + printSci(getProperty("Time_busy(4)__2"))
				+ "\n";
		ret += "Time_idle(5) " + printSci(getProperty("Time_idle(5)__2"))
				+ "\n";
		ret += "%phy_age=3\n";
		ret += "Time_Function(Tu_O(1)) "
				+ printSci(getProperty("Time_Function(Tu_O(1))__3")) + "\n";
		ret += "Time_appearance(2) "
				+ printSci(getProperty("Time_appearance(2)__3")) + "\n";
		ret += "Time_disappearance(3) "
				+ printSci(getProperty("Time_disappearance(3)__3")) + "\n";
		ret += "Time_busy(4) " + printSci(getProperty("Time_busy(4)__3"))
				+ "\n";
		ret += "Time_idle(5) " + printSci(getProperty("Time_idle(5)__3"))
				+ "\n";
		ret += "%phy_age=4\n";
		ret += "Time_Function(Tu_O(1)) "
				+ printSci(getProperty("Time_Function(Tu_O(1))__4")) + "\n";
		ret += "Time_appearance(2) "
				+ printSci(getProperty("Time_appearance(2)__4")) + "\n";
		ret += "Time_disappearance(3) "
				+ printSci(getProperty("Time_disappearance(3)__4")) + "\n";
		ret += "Time_busy(4) " + printSci(getProperty("Time_busy(4)__4"))
				+ "\n";
		ret += "Time_idle(5) " + printSci(getProperty("Time_idle(5)__4"))
				+ "\n";
		ret += "%phy_age=5\n";
		ret += "Time_Function(Tu_O(1)) "
				+ printSci(getProperty("Time_Function(Tu_O(1))__5")) + "\n";
		ret += "Time_appearance(2) "
				+ printSci(getProperty("Time_appearance(2)__5")) + "\n";
		ret += "Time_disappearance(3) "
				+ printSci(getProperty("Time_disappearance(3)__5")) + "\n";
		ret += "Time_busy(4) " + printSci(getProperty("Time_busy(4)__5"))
				+ "\n";
		ret += "Time_idle(5) " + printSci(getProperty("Time_idle(5)__5"))
				+ "\n";
		ret += "%phy_age=6\n";
		ret += "Time_Function(Tu_O(1)) "
				+ printSci(getProperty("Time_Function(Tu_O(1))__6")) + "\n";
		ret += "Time_appearance(2) "
				+ printSci(getProperty("Time_appearance(2)__6")) + "\n";
		ret += "Time_disappearance(3) "
				+ printSci(getProperty("Time_disappearance(3)__6")) + "\n";
		ret += "Time_busy(4) " + printSci(getProperty("Time_busy(4)__6"))
				+ "\n";
		ret += "Time_idle(5) " + printSci(getProperty("Time_idle(5)__6"))
				+ "\n";
		ret += " \n";
		ret += "Time_Expansion(t_exp) "
				+ printSci(getProperty("Time_Expansion(t_exp)")) + "\n";
		ret += " \n";
		ret += "//*****************function_time_variable*****************\n";
		ret += "//******************rank*min*rank*max*********************\n";
		ret += "Flag_Time_function_version "
				+ printSci(getProperty("Flag_Time_function_version")) + "\n";
		ret += "Time_function(T_f(1))\n";
		ret += "Blade " + printSci(getProperty("Blade__1")) + "\n";
		ret += "Petiole " + printSci(getProperty("Petiole__1")) + "\n";
		ret += "Internode " + printSci(getProperty("Internode__1")) + "\n";
		ret += "Femalefruit " + printSci(getProperty("Femalefruit__1")) + "\n";
		ret += "Malefruit " + printSci(getProperty("Malefruit__1")) + "\n";
		ret += "Root " + printSci(getProperty("Root__1")) + "\n";
		ret += "Time_function(T_f(2))\n";
		ret += "Blade " + printSci(getProperty("Blade__2")) + "\n";
		ret += "Petiole " + printSci(getProperty("Petiole__2")) + "\n";
		ret += "Internode " + printSci(getProperty("Internode__2")) + "\n";
		ret += "Femalefruit " + printSci(getProperty("Femalefruit__2")) + "\n";
		ret += "Malefruit " + printSci(getProperty("Malefruit__2")) + "\n";
		ret += "Root " + printSci(getProperty("Root__2")) + "\n";
		ret += "Time_function(T_f(3))\n";
		ret += "Blade " + printSci(getProperty("Blade__3")) + "\n";
		ret += "Petiole " + printSci(getProperty("Petiole__3")) + "\n";
		ret += "Internode " + printSci(getProperty("Internode__3")) + "\n";
		ret += "Femalefruit " + printSci(getProperty("Femalefruit__3")) + "\n";
		ret += "Malefruit " + printSci(getProperty("Malefruit__3")) + "\n";
		ret += "Root " + printSci(getProperty("Root__3")) + "\n";
		ret += "Time_function(T_f(4))\n";
		ret += "Blade " + printSci(getProperty("Blade__4")) + "\n";
		ret += "Petiole " + printSci(getProperty("Petiole__4")) + "\n";
		ret += "Internode " + printSci(getProperty("Internode__4")) + "\n";
		ret += "Femalefruit " + printSci(getProperty("Femalefruit__4")) + "\n";
		ret += "Malefruit " + printSci(getProperty("Malefruit__4")) + "\n";
		ret += "Root " + printSci(getProperty("Root__4")) + "\n";
		ret += "Time_function(T_f(5))\n";
		ret += "Blade " + printSci(getProperty("Blade__5")) + "\n";
		ret += "Petiole " + printSci(getProperty("Petiole__5")) + "\n";
		ret += "Internode " + printSci(getProperty("Internode__5")) + "\n";
		ret += "Femalefruit " + printSci(getProperty("Femalefruit__5")) + "\n";
		ret += "Malefruit " + printSci(getProperty("Malefruit__5")) + "\n";
		ret += "Root " + printSci(getProperty("Root__5")) + "\n";
		ret += " \n";
		ret += "//*********************expansion_time_variable************************\n";
		ret += "//******************rank*min*rank*max*********************\n";
		ret += "Flag_Time_expansion_version "
				+ printSci(getProperty("Flag_Time_expansion_version")) + "\n";
		ret += "Time_expansion(T_exp)\n";
		ret += "Blade " + printSci(getProperty("Blade__6")) + "\n";
		ret += "Petiole " + printSci(getProperty("Petiole__6")) + "\n";
		ret += "Internode " + printSci(getProperty("Internode__6")) + "\n";
		ret += "Femalefruit " + printSci(getProperty("Femalefruit__6")) + "\n";
		ret += "Malefruit " + printSci(getProperty("Malefruit__6")) + "\n";
		ret += "Root " + printSci(getProperty("Root__6")) + "\n";
		ret += " \n";
		ret += "%OrganType(id)--------Blade-Petiel-Internode-FemaleFlower-MaleFlower-Ring-Root\n";
		ret += "Para_a(Bt_a) " + printSci(getProperty("Para_a(Bt_a)")) + "\n";
		ret += "Para_b(Bt_b) " + printSci(getProperty("Para_b(Bt_b)")) + "\n";
		ret += " \n";
		ret += "Portion_Remobilization(P_Remob) "
				+ printSci(getProperty("Portion_Remobilization(P_Remob)"))
				+ "\n";
		ret += "Speed_Remobilization(V_Remob) "
				+ printSci(getProperty("Speed_Remobilization(V_Remob)")) + "\n";
		ret += " \n";
		ret += "Sink_Blade(S_B) " + printSci(getProperty("Sink_Blade(S_B)"))
				+ "\n";
		ret += "Sink_Petiole(S_P) "
				+ printSci(getProperty("Sink_Petiole(S_P)")) + "\n";
		ret += "Sink_Internode(S_I) "
				+ printSci(getProperty("Sink_Internode(S_I)")) + "\n";
		ret += "Sink_Female(S_Ff) "
				+ printSci(getProperty("Sink_Female(S_Ff)")) + "\n";
		ret += "Sink_Male(S_Fm) " + printSci(getProperty("Sink_Male(S_Fm)"))
				+ "\n";
		ret += "Sink_Layer(S_L) " + printSci(getProperty("Sink_Layer(S_L)"))
				+ "\n";
		ret += "Sink_Root(S_R) " + printSci(getProperty("Sink_Root(S_R)"))
				+ "\n";
		ret += " \n";
		ret += "Allom_Internode(a_bI) "
				+ printSci(getProperty("Allom_Internode(a_bI)")) + "\n";
		ret += "(a_aI) " + printSci(getProperty("(a_aI)")) + "\n";
		ret += "Allom_Petiole(a_bP) "
				+ printSci(getProperty("Allom_Petiole(a_bP)")) + "\n";
		ret += "(a_aP) " + printSci(getProperty("(a_aP)")) + "\n";
		ret += "Allom_Blade(a_bB) "
				+ printSci(getProperty("Allom_Blade(a_bB)")) + "\n";
		ret += "(a_aB) " + printSci(getProperty("(a_aB)")) + "\n";
		ret += "Allom_Thick(Th_B) "
				+ printSci(getProperty("Allom_Thick(Th_B)")) + "\n";
		ret += "Female_Density(Den_Ff) "
				+ printSci(getProperty("Female_Density(Den_Ff)")) + "\n";
		ret += "Male_Density(Den_Fm) "
				+ printSci(getProperty("Male_Density(Den_Fm)")) + "\n";
		ret += "Internode_Dens(Den_I) "
				+ printSci(getProperty("Internode_Dens(Den_I)")) + "\n";
		ret += " \n";
		ret += "Number_Primordia(N_PM) "
				+ printSci(getProperty("Number_Primordia(N_PM)")) + "\n";
		ret += "Sink_Primordia(S_PM) "
				+ printSci(getProperty("Sink_Primordia(S_PM)")) + "\n";
		ret += " \n";
		ret += "%Top_Down_Delay_Parameter:ND(p)=Np(p)+Cp(p)*(Nu_Ma(1)-Pos),gl_demand\n";
		ret += "Delay_Branch(Cp_B) "
				+ printSci(getProperty("Delay_Branch(Cp_B)")) + "\n";
		ret += " \n";
		ret += "Flag_field " + printSci(getProperty("Flag_field")) + "\n";
		ret += "Resist_Blade(r_B) "
				+ printSci(getProperty("Resist_Blade(r_B)")) + "\n";
		ret += "Resist_Petiole(r_P) "
				+ printSci(getProperty("Resist_Petiole(r_P)")) + "\n";
		ret += "Resist_Root(r_R) " + printSci(getProperty("Resist_Root(r_R)"))
				+ "\n";
		ret += "Projected_area(Sp) "
				+ printSci(getProperty("Projected_area(Sp)")) + "\n";
		ret += "Beer_law(rp) " + printSci(getProperty("Beer_law(rp)")) + "\n";
		ret += "Beer_law(kp) " + printSci(getProperty("Beer_law(kp)")) + "\n";
		ret += " \n";
		ret += "Seed_Biomass(Q0) " + printSci(getProperty("Seed_Biomass(Q0)"))
				+ "\n";
		ret += "Flow_Biomass(dQ0) "
				+ printSci(getProperty("Flow_Biomass(dQ0)")) + "\n";
		ret += " \n";
		ret += "Flag_TotalLayerDemand_Mode "
				+ printSci(getProperty("Flag_TotalLayerDemand_Mode")) + "\n";
		ret += "Global_Sink_Layer(Slay) "
				+ printSci(getProperty("Global_Sink_Layer(Slay)")) + "\n";
		ret += "Global_Sink_Coefficient(Slope) "
				+ printSci(getProperty("Global_Sink_Coefficient(Slope)"))
				+ "\n";
		ret += "Layer_repartition_Coefficient(Lamda) "
				+ printSci(getProperty("Layer_repartition_Coefficient(Lamda)"))
				+ "\n";
		ret += " \n";
		ret += "Expansion_Mode " + printSci(getProperty("Expansion_Mode"))
				+ "\n";
		ret += "Root_Demand_Mode " + printSci(getProperty("Root_Demand_Mode"))
				+ "\n";
		ret += " \n";
		ret += "ShortInter_Number(N_SI) "
				+ printSci(getProperty("ShortInter_Number(N_SI)")) + "\n";
		ret += "ShortInter_RelaSink(kp_SI) "
				+ printSci(getProperty("ShortInter_RelaSink(kp_SI)")) + "\n";
		ret += " \n";
		ret += "%***********************Geometry*********************\n";
		ret += "Leaf_shape(smb_L) "
				+ printSci(getProperty("Leaf_shape(smb_L)")) + "\n";
		ret += "Female_shape(smb_Ff) "
				+ printSci(getProperty("Female_shape(smb_Ff)")) + "\n";
		ret += "Male_shape(smb_Fm) "
				+ printSci(getProperty("Male_shape(smb_Fm)")) + "\n";
		ret += " \n";
		ret += "Leaf_color(Color_B) "
				+ printSci(getProperty("Leaf_color(Color_B)"), 3) + "\n";
		ret += "Internode_color(Color_I) "
				+ printSci(getProperty("Internode_color(Color_I)"), 3) + "\n";
		ret += "Female_color(Color_Ff) "
				+ printSci(getProperty("Female_color(Color_Ff)"), 3) + "\n";
		ret += "Male_color(Color_Fm) "
				+ printSci(getProperty("Male_color(Color_Fm)"), 3) + "\n";
		ret += " \n";
		ret += "Phy_Color(Color_P) "
				+ printSci(getProperty("Phy_Color(Color_P)"), 3) + "\n";
		ret += " \n";
		ret += "Angle_Male(Ang_Fm) "
				+ printSci(getProperty("Angle_Male(Ang_Fm)")) + "\n";
		ret += "Angle_Female(Ang_Ff) "
				+ printSci(getProperty("Angle_Female(Ang_Ff)")) + "\n";
		ret += "Angle_Blade(Ang_B) "
				+ printSci(getProperty("Angle_Blade(Ang_B)")) + "\n";
		ret += "Angle_Axillary(Ang_A) "
				+ printSci(getProperty("Angle_Axillary(Ang_A)")) + "\n";
		ret += "Angle_Phyllotaxy(Ang_Ph) "
				+ printSci(getProperty("Angle_Phyllotaxy(Ang_Ph)")) + "\n";
		ret += " \n";
		ret += "Young_Modulus(Ey) "
				+ printSci(getProperty("Young_Modulus(Ey)")) + "\n";
		ret += "Force_point(fp) " + printSci(getProperty("Force_point(fp)"))
				+ "\n";
		ret += "Terminal_Angle(Eng_Ang) "
				+ printSci(getProperty("Terminal_Angle(Eng_Ang)")) + "\n";
		ret += "Righting_point(Eng_N) "
				+ printSci(getProperty("Righting_point(Eng_N)")) + "\n";
		ret += "InitialMin_angle(InitMin) "
				+ printSci(getProperty("InitialMin_angle(InitMin)")) + "\n";
		ret += "InitialMax_angle(InitMax) "
				+ printSci(getProperty("InitialMax_angle(InitMax)")) + "\n";
		ret += "InitialNum_angle(InitNum) "
				+ printSci(getProperty("InitialNum_angle(InitNum)")) + "\n";
		ret += "Flag_Bending_by_node "
				+ printSci(getProperty("Flag_Bending_by_node")) + "\n";
		ret += " \n";
		ret += "Max_Angle(wb) " + printSci(getProperty("Max_Angle(wb)")) + "\n";
		ret += "Openning_cycle(wbn) "
				+ printSci(getProperty("Openning_cycle(wbn)")) + "\n";
		ret += " \n";
		ret += "Organ(Flag_organ_bending) "
				+ printSci(getProperty("Organ(Flag_organ_bending)")) + "\n";
		ret += "Bending_branch(Theta_B) "
				+ printSci(getProperty("Bending_branch(Theta_B)")) + "\n";
		ret += "Bending_leaf(Theta_L) "
				+ printSci(getProperty("Bending_leaf(Theta_L)")) + "\n";
		ret += "Bending_fruit(Theta_F) "
				+ printSci(getProperty("Bending_fruit(Theta_F)")) + "\n";
		ret += " \n";
		ret += "Flag_plagiotropic "
				+ printSci(getProperty("Flag_plagiotropic")) + "\n";
		ret += "Leaf_direction(Flag) "
				+ printSci(getProperty("Leaf_direction(Flag)")) + "\n";
		ret += " \n";
		ret += "Pruning(Flag_pruning) "
				+ printSci(getProperty("Pruning(Flag_pruning)")) + "\n";
		ret += "Delay(Pruning_delay) "
				+ printSci(getProperty("Delay(Pruning_delay)")) + "\n";
		ret += "Leaf_display(Flag_leaf) "
				+ printSci(getProperty("Leaf_display(Flag_leaf)")) + "\n";
		ret += "Fruit_display(Flag_fruit) "
				+ printSci(getProperty("Fruit_display(Flag_fruit)")) + "\n";
		ret += " \n";
		ret += "Internode_Volume(VI) "
				+ printSci(getProperty("Internode_Volume(VI)")) + "\n";
		ret += "Blade_Volume(VB) " + printSci(getProperty("Blade_Volume(VB)"))
				+ "\n";
		ret += "Fruit_Volume(VF) " + printSci(getProperty("Fruit_Volume(VF)"))
				+ "\n";
		ret += "Internode_C(CI) " + printSci(getProperty("Internode_C(CI)"))
				+ "\n";
		ret += "Blade_C(CB) " + printSci(getProperty("Blade_C(CB)")) + "\n";
		ret += "Fruit_C(CF) " + printSci(getProperty("Fruit_C(CF)")) + "\n";
		ret += " \n";
		ret += "%***********************Environment*********************\n";
		ret += "%soil_water_budget\n";
		ret += "Flag_climate " + printSci(getProperty("Flag_climate")) + "\n";
		ret += "SoilWaterEffect(K1) "
				+ printSci(getProperty("SoilWaterEffect(K1)")) + "\n";
		ret += "SoilWaterEffect(K2) "
				+ printSci(getProperty("SoilWaterEffect(K2)")) + "\n";
		ret += "Soil_budget_para(C1) "
				+ printSci(getProperty("Soil_budget_para(C1)")) + "\n";
		ret += "Soil_budget_para(C2) "
				+ printSci(getProperty("Soil_budget_para(C2)")) + "\n";
		ret += "InitialSoilWater(QSW0) "
				+ printSci(getProperty("InitialSoilWater(QSW0)")) + "\n";
		ret += "MaxPlantSoilWater(QSWMAX) "
				+ printSci(getProperty("MaxPlantSoilWater(QSWMAX)")) + "\n";
		ret += "MinPlantSoilWater(QSWMIN) "
				+ printSci(getProperty("MinPlantSoilWater(QSWMIN)")) + "\n";
		ret += "MaxEnvFSoilWater(QSW_MAX) "
				+ printSci(getProperty("MaxEnvFSoilWater(QSW_MAX)")) + "\n";
		ret += "MinEnvFSoilWater(QSW_MIN) "
				+ printSci(getProperty("MinEnvFSoilWater(QSW_MIN)")) + "\n";
		ret += " \n";
		ret += "%temperature_effect\n";
		ret += "Flag_temperature " + printSci(getProperty("Flag_temperature"))
				+ "\n";
		ret += "SumTEffect(Flag_sumt) "
				+ printSci(getProperty("SumTEffect(Flag_sumt)")) + "\n";
		ret += "Temperature(E_ALPHA) "
				+ printSci(getProperty("Temperature(E_ALPHA)")) + "\n";
		ret += "Temperature(E_BETA) "
				+ printSci(getProperty("Temperature(E_BETA)")) + "\n";
		ret += "SumTEffect(KSUMT) "
				+ printSci(getProperty("SumTEffect(KSUMT)")) + "\n";
		ret += "SumTEffect(THETA_BASE) "
				+ printSci(getProperty("SumTEffect(THETA_BASE)")) + "\n";
		ret += "MaxPlantTemp(THETAMAX) "
				+ printSci(getProperty("MaxPlantTemp(THETAMAX)")) + "\n";
		ret += "MinPlantTemp(THETAMIN) "
				+ printSci(getProperty("MinPlantTemp(THETAMIN)")) + "\n";
		ret += "MaxEnvFTemp(THETA_MAX) "
				+ printSci(getProperty("MaxEnvFTemp(THETA_MAX)")) + "\n";
		ret += "MinEnvFTemp(THETA_MIN) "
				+ printSci(getProperty("MinEnvFTemp(THETA_MIN)")) + "\n";
		ret += " \n";
		ret += "%light_effect\n";
		ret += "Flag_light " + printSci(getProperty("Flag_light")) + "\n";
		ret += "Light(E_A) " + printSci(getProperty("Light(E_A)")) + "\n";
		ret += "Light(E_B) " + printSci(getProperty("Light(E_B)")) + "\n";
		ret += "MaxEnvFLight(LIGHTMAX) "
				+ printSci(getProperty("MaxEnvFLight(LIGHTMAX)")) + "\n";
		ret += "MinEnvFLight(LIGHTMIN) "
				+ printSci(getProperty("MinEnvFLight(LIGHTMIN)")) + "\n";
		ret += "MaxEnvFLight(LIGHT_MAX) "
				+ printSci(getProperty("MaxEnvFLight(LIGHT_MAX)")) + "\n";
		ret += "MinEnvFLight(LIGHT_MIN) "
				+ printSci(getProperty("MinEnvFLight(LIGHT_MIN)")) + "\n";
		ret += "PotentialCondition(E0) "
				+ printSci(getProperty("PotentialCondition(E0)")) + "\n";
		ret += " \n";
		ret += "%***********************OutputOption*********************\n";
		ret += "Flag_topo_disp " + printSci(getProperty("Flag_topo_disp"))
				+ "\n";
		ret += "Flag_topo_disp_total "
				+ printSci(getProperty("Flag_topo_disp_total")) + "\n";
		ret += "Flag_topo_disp_living "
				+ printSci(getProperty("Flag_topo_disp_living")) + "\n";
		ret += "Flag_topo_disp_living_aboveGU "
				+ printSci(getProperty("Flag_topo_disp_living_aboveGU")) + "\n";
		ret += "Flag_hydro " + printSci(getProperty("Flag_hydro")) + "\n";
		ret += "Flag_demand_biomass_layer "
				+ printSci(getProperty("Flag_demand_biomass_layer")) + "\n";
		ret += "Flag_demand_biomass_plant "
				+ printSci(getProperty("Flag_demand_biomass_plant")) + "\n";
		ret += "Flag_biomass_repartition "
				+ printSci(getProperty("Flag_biomass_repartition")) + "\n";
		ret += "Flag_biomass_fig " + printSci(getProperty("Flag_biomass_fig"))
				+ "\n";
		ret += "Flag_biomassdemand_fig "
				+ printSci(getProperty("Flag_biomassdemand_fig")) + "\n";
		ret += "Flag_biomass_fig_accumulated "
				+ printSci(getProperty("Flag_biomass_fig_accumulated")) + "\n";
		ret += "Flag_disp_LAI " + printSci(getProperty("Flag_disp_LAI")) + "\n";
		ret += "Flag_organ_biomass_fig_phy "
				+ printSci(getProperty("Flag_organ_biomass_fig_phy")) + "\n";
		ret += "Flag_organ_size_fig_phy "
				+ printSci(getProperty("Flag_organ_size_fig_phy")) + "\n";
		ret += "Flag_geometry " + printSci(getProperty("Flag_geometry")) + "\n";
		ret += "Flag_geo_full " + printSci(getProperty("Flag_geo_full")) + "\n";
		ret += "Flag_geo_3D " + printSci(getProperty("Flag_geo_3D")) + "\n";
		ret += "Flag_geo_leaf " + printSci(getProperty("Flag_geo_leaf")) + "\n";
		ret += "Flag_geo_fruit " + printSci(getProperty("Flag_geo_fruit"))
				+ "\n";
		ret += "Flag_geo_skeleton "
				+ printSci(getProperty("Flag_geo_skeleton")) + "\n";
		ret += "Flag_geo_lig " + printSci(getProperty("Flag_geo_lig")) + "\n";
		ret += "Flag_windows " + printSci(getProperty("Flag_windows")) + "\n";
		ret += " \n";

		return ret;
	}

	public String getRgg() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String ret = "/**\n";
		ret += "* XL-Reimplementation of the GreenLab Model\n";
		ret += "* This parameter file is based on the one from the GreenScilabV1.0Demo software\n";
		ret += "* \n";
		ret += "* NOTE: DO NOT EDIT THIS FILE -\n";
		ret += "* IT WILL BE REPLACED AUTOMATICALLY WHEN YOU PUSH THE APPLY BUTTON!\n";
		ret += "*\n";
		ret += "* @date <" + format.format(date) + ">\n";
		ret += "* @author Groimp Generator\n";
		ret += "* @version \n";
		ret += "*/\n";
		ret += "///////////////////////////// GreenScilab parameters ///////////////////////////\n";
		ret += "\n";
		ret += "// Chr_Age(N)\n";
		ret += "public static final int  N = " + getProperty("Chr_Age(N)")
				+ ";\n";
		ret += "// Phy_Age(Maxp)\n";
		ret += "public static final int  MAXP = "
				+ getProperty("Phy_Age(Maxp)") + ";\n";
		ret += "// Sample_Size(Tr)\n";
		ret += "public static final int[]  TR = "
				+ printList(getProperty("Sample_Size(Tr)")) + ";\n";
		ret += "\n";
		ret += "// TOPOLOGY\n";
		ret += "\n";
		ret += "// number of organs (microstate)\n";
		ret += "// Axillary_Num(Nu_A)\n";
		ret += "public static final int[][]  NU_A = "
				+ printAges(getProperty("Axillary_Num(Nu_A)")) + ";\n";
		ret += "// Leaf_Num(Nu_B)\n";
		ret += "public static final int[][]  NU_B = "
				+ printAges(getProperty("Leaf_Num(Nu_B)")) + ";\n";
		ret += "// Female_Num(Nu_Ff)\n";
		ret += "public static final int[][]  NU_FF = "
				+ printAges(getProperty("Female_Num(Nu_Ff)")) + ";\n";
		ret += "// Male_Num(Nu_Fm)\n";
		ret += "public static final int[][]  NU_FM = "
				+ printAges(getProperty("Male_Num(Nu_Fm)")) + ";\n";
		ret += "// Micro_Num(Nu_I)\n";
		ret += "public static final int[][]  NU_I = "
				+ printAges(getProperty("Micro_Num(Nu_I)")) + ";\n";
		ret += "\n";
		ret += "// macrostate\n";
		ret += "// Flag_Nu_Ma_version\n";
		ret += "public static final int  FLAG_NU_MA = "
				+ getProperty("Flag_Nu_Ma_version") + ";\n";
		ret += "// Macro_Num(Nu_Ma)\n";
		ret += "public static final int[]  NU_MA = "
				+ printList(getProperty("Macro_Num(Nu_Ma)")) + ";\n";
		ret += "\n";
		ret += "// jump in PA for ending structures\n";
		ret += "// Struc_Jump(st_j)\n";
		ret += "public static final int[]  ST_J = "
				+ printList(getProperty("Struc_Jump(st_j)")) + ";\n";
		ret += "\n";
		ret += "// reiteration control\n";
		ret += "// Reiter_Order(b_o)\n";
		ret += "public static final int[]  B_O = "
				+ printList(getProperty("Reiter_Order(b_o)")) + ";\n";
		ret += "// Bran_Control(br_a)\n";
		ret += "public static final boolean[]  BR_A = "
				+ printBoolList(getProperty("Bran_Control(br_a)")) + ";\n";
		ret += "// Reiter_Control(re_a)\n";
		ret += "public static final boolean[]  RE_A = "
				+ printBoolList(getProperty("Reiter_Control(re_a)")) + ";\n";
		ret += "// rest before functioning\n";
		ret += "// Rest_Func(rs_A)\n";
		ret += "public static final int[]  RS_A = "
				+ printList(getProperty("Rest_Func(rs_A)")) + ";\n";
		ret += "// Rest_Func(rs_B)\n";
		ret += "public static final int[]  RS_B = "
				+ printList(getProperty("Rest_Func(rs_B)")) + ";\n";
		ret += "// rythm_Ratio(rt_a)\n";
		ret += "public static final double[]  RT_A = "
				+ printList(getProperty("rythm_Ratio(rt_a)")) + ";\n";
		ret += "\n";
		ret += "// stochastic control\n";
		ret += "// branching_proba(a)\n";
		ret += "public static final double[]  A = "
				+ printList(getProperty("branching_proba(a)")) + ";\n";
		ret += "// Growth_macro(b)\n";
		ret += "public static final double[]  B = "
				+ printList(getProperty("Growth_macro(b)")) + ";\n";
		ret += "// Growth_micro(bu)\n";
		ret += "public static final double[]  BU = "
				+ printList(getProperty("Growth_micro(bu)")) + ";\n";
		ret += "// Survive(c)\n";
		ret += "public static final double[]  C = "
				+ printList(getProperty("Survive(c)")) + ";\n";
		ret += "\n";
		ret += "// GROWTH\n";
		ret += "\n";
		ret += "// functioning duration, appearance, disappearance, busy, idle time, dependent on PA, constant\n";
		ret += "// %phy_age=1\n";
		ret += "public static final int[][]  TU_O_PA1 = " + printPhyAge(1)
				+ ";\n";
		ret += "// %phy_age=2\n";
		ret += "public static final int[][]  TU_O_PA2 = " + printPhyAge(2)
				+ ";\n";
		ret += "// %phy_age=3\n";
		ret += "public static final int[][]  TU_O_PA3 = " + printPhyAge(3)
				+ ";\n";
		ret += "// %phy_age=4\n";
		ret += "public static final int[][]  TU_O_PA4 = " + printPhyAge(4)
				+ ";\n";
		ret += "// %phy_age=5\n";
		ret += "public static final int[][]  TU_O_PA5 = " + printPhyAge(5)
				+ ";\n";
		ret += "// %phy_age=6\n";
		ret += "public static final int[][]  TU_O_PA6 = " + printPhyAge(6)
				+ ";\n";
		ret += "\n";
		ret += "// expansion duration, independent of PA, constant\n";
		ret += "// Time_Expansion(t_exp)\n";
		ret += "public static final int[]  T_EXP_CONST = "
				+ printList(getProperty("Time_Expansion(t_exp)")) + ";\n";
		ret += "\n";
		ret += "// Flag_Time_function_version, 0 - constant, 1 - variable, 2 - from file\n";
		ret += "public static final int  FLAG_TIME_FUNCTION = "
				+ getProperty("Flag_Time_function_version") + ";\n";
		ret += "\n";
		ret += "// functioning duration, dependent on PA, variable\n";
		ret += "// Time_function(T_f(1))\n";
		ret += "public static final int[][]  T_FUNC_VAR_PA1 = "
				+ printTimeFunc(1) + ";\n";
		ret += "// Time_function(T_f(2))\n";
		ret += "public static final int[][]  T_FUNC_VAR_PA2 = "
				+ printTimeFunc(2) + ";\n";
		ret += "// Time_function(T_f(3))\n";
		ret += "public static final int[][]  T_FUNC_VAR_PA3 = "
				+ printTimeFunc(3) + ";\n";
		ret += "// Time_function(T_f(4))\n";
		ret += "public static final int[][]  T_FUNC_VAR_PA4 = "
				+ printTimeFunc(4) + ";\n";
		ret += "// Time_function(T_f(5))\n";
		ret += "public static final int[][]  T_FUNC_VAR_PA5 = "
				+ printTimeFunc(5) + ";\n";
		ret += "\n";
		ret += "// Flag_Time_expansion_version, 0 - constant, 1 - variable, 2 - from file\n";
		ret += "public static final int  FLAG_TIME_EXPANSION = "
				+ getProperty("Flag_Time_expansion_version") + ";\n";
		ret += "// expansion duration, independent of PA, variable\n";
		ret += "// Time_expansion(T_exp)\n";
		ret += "public static final int[][]  T_EXP_VAR = " + printTimeFunc(6)
				+ ";\n";
		ret += "\n";
		ret += "// expansion law\n";
		ret += "// Para_a(Bt_a)\n";
		ret += "public static final double[]  BT_N = "
				+ printList(getProperty("Para_a(Bt_a)")) + ";\n";
		ret += "// Para_b(Bt_b)\n";
		ret += "public static final double[]  BT_P = "
				+ printList(getProperty("Para_b(Bt_b)")) + ";\n";
		ret += "\n";
		ret += "// remobilization\n";
		ret += "// Portion_Remobilization(P_Remob)\n";
		ret += "public static final double[]  P_REMOB = "
				+ printList(getProperty("Portion_Remobilization(P_Remob)"))
				+ ";\n";
		ret += "// Speed_Remobilization(V_Remob)\n";
		ret += "public static final double[]  V_REMOB = "
				+ printList(getProperty("Speed_Remobilization(V_Remob)"))
				+ ";\n";
		ret += "\n";
		ret += "// sink of organs\n";
		ret += "// Sink_Blade(S_B)\n";
		ret += "// Sink_Petiole(S_P)\n";
		ret += "// Sink_Internode(S_I)\n";
		ret += "// Sink_Female(S_Ff)\n";
		ret += "// Sink_Male(S_Fm)\n";
		ret += "// Sink_Layer(S_L)\n";
		ret += "// Sink_Root(S_R)\n";
		ret += "public static final double[][]  S_O = " + printSink() + ";\n";
		ret += "\n";
		ret += "// allometry\n";
		ret += "// Allom_Internode(a_bI)\n";
		ret += "public static final double[]  A_BI = "
				+ printList(getProperty("Allom_Internode(a_bI)")) + ";\n";
		ret += "// (a_aI)\n";
		ret += "public static final double[]  A_AI = "
				+ printList(getProperty("(a_aI)")) + ";\n";
		ret += "// Allom_Petiole(a_bP)\n";
		ret += "public static final double[]  A_BP = "
				+ printList(getProperty("Allom_Petiole(a_bP)")) + ";\n";
		ret += "// (a_aP)\n";
		ret += "public static final double[]  A_AP = "
				+ printList(getProperty("(a_aP)")) + ";\n";
		ret += "// Allom_Blade(a_bB)\n";
		ret += "public static final double[]  A_BB = "
				+ printList(getProperty("Allom_Blade(a_bB)")) + ";\n";
		ret += "// (a_aB)\n";
		ret += "public static final double[]  A_AB = "
				+ printList(getProperty("(a_aB)")) + ";\n";
		ret += "// Allom_Thick(Th_B)\n";
		ret += "public static final double[]  TH_B = "
				+ printList(getProperty("Allom_Thick(Th_B)")) + ";\n";
		ret += "// Female_Density(Den_Ff)\n";
		ret += "public static final double[]  DEN_FF = "
				+ printList(getProperty("Female_Density(Den_Ff)")) + ";\n";
		ret += "// Male_Density(Den_Fm)\n";
		ret += "public static final double[]  DEN_FM = "
				+ printList(getProperty("Male_Density(Den_Fm)")) + ";\n";
		ret += "// Internode_Dens(Den_I)\n";
		ret += "public static final double[]  DEN_I = "
				+ printList(getProperty("Internode_Dens(Den_I)")) + ";\n";
		ret += "\n";
		ret += "// Number_Primordia(N_PM) \n";
		ret += "public static final double[]  N_PM = "
				+ printList(getProperty("Number_Primordia(N_PM)")) + ";\n";
		ret += "// Sink_Primordia(S_PM)\n";
		ret += "public static final double[]  S_PM = "
				+ printList(getProperty("Sink_Primordia(S_PM)")) + ";\n";
		ret += "\n";
		ret += "// Delay_Branch(Cp_B)\n";
		ret += "public static final int[]  CP_B = "
				+ printList(getProperty("Delay_Branch(Cp_B)")) + ";\n";
		ret += "\n";
		ret += "// Flag_field\n";
		ret += "public static final int  FLAG_FIELD = "
				+ getProperty("Flag_field") + ";\n";
		ret += "// Resist_Blade(r_B)\n";
		ret += "public static final double  R_B = "
				+ getProperty("Resist_Blade(r_B)") + ";\n";
		ret += "// Resist_Petiole(r_P)\n";
		ret += "public static final double  R_P = "
				+ getProperty("Resist_Petiole(r_P)") + ";\n";
		ret += "// Resist_Root(r_R)\n";
		ret += "public static final double  R_R = "
				+ getProperty("Resist_Root(r_R)") + ";\n";
		ret += "// Projected_area(Sp)\n";
		ret += "public static final double  SP = "
				+ getProperty("Projected_area(Sp)") + ";\n";
		ret += "// Beer_law(rp)\n";
		ret += "public static final double  RP = "
				+ getProperty("Beer_law(rp)") + ";\n";
		ret += "// Beer_law(kp)\n";
		ret += "public static final double  KP = "
				+ getProperty("Beer_law(kp)") + ";\n";
		ret += "\n";
		ret += "// Seed_Biomass(Q0)\n";
		ret += "public static final double  Q0 = "
				+ getProperty("Seed_Biomass(Q0)") + ";\n";
		ret += "// Flow_Biomass(dQ0)\n";
		ret += "public static final double  DQ0 = "
				+ getProperty("Flow_Biomass(dQ0)") + ";\n";
		ret += "\n";
		ret += "// Flag_TotalLayerDemand_Mode\n";
		ret += "public static final int  FLAG_TOTAL_LAYER_DEMAND = "
				+ getProperty("Flag_TotalLayerDemand_Mode") + ";\n";
		ret += "// Global_Sink_Layer(Slay)\n";
		ret += "public static final double  SLAY = "
				+ getProperty("Global_Sink_Layer(Slay)") + ";\n";
		ret += "// Global_Sink_Coefficient(Slope)\n";
		ret += "public static final double  SLOPE = "
				+ getProperty("Global_Sink_Coefficient(Slope)") + ";\n";
		ret += "// Layer_repartition_Coefficient(Lamda)\n";
		ret += "public static final double  COFF_L = "
				+ getProperty("Layer_repartition_Coefficient(Lamda)") + ";\n";
		ret += "\n";
		ret += "// Expansion_Mode\n";
		ret += "public static final int  EXPANSION_MODE = "
				+ getProperty("Expansion_Mode") + ";\n";
		ret += "// Root_Demand_Mode\n";
		ret += "public static final int  ROOT_DEMAND_MODE = "
				+ getProperty("Root_Demand_Mode") + ";\n";
		ret += "\n";
		ret += "// ShortInter_Number(N_SI)\n";
		ret += "public static final int  N_SI = "
				+ getProperty("ShortInter_Number(N_SI)") + ";\n";
		ret += "// ShortInter_RelaSink(kp_SI)\n";
		ret += "public static final double  KP_SI = "
				+ getProperty("ShortInter_RelaSink(kp_SI)") + ";\n";
		ret += "\n";
		ret += "// GEOMETRY\n";
		ret += "\n";
		ret += "// shape\n";
		ret += "// Leaf_shape(smb_L)\n";
		ret += "public static final Reference LEAF_OBJECT = reference(\""
				+ getProperty("Leaf_shape(smb_L)") + "\");\n";
		ret += "// Female_shape(smb_Ff)\n";
		ret += "public static final Reference FEMALE_FRUIT_OBJECT = reference(\""
				+ getProperty("Female_shape(smb_Ff)") + "\");\n";
		ret += "// Male_shape(smb_Fm)\n";
		ret += "public static final Reference MALE_FRUIT_OBJECT = reference(\""
				+ getProperty("Male_shape(smb_Fm)") + "\");\n";
		ret += "\n";
		ret += "// colour\n";
		ret += "// Leaf_color(Color_B)\n";
		ret += "public static final double[][]  COLOR_B = "
				+ printSp(getProperty("Leaf_color(Color_B)"), 2, 3) + ";\n";
		ret += "// Internode_color(Color_I)\n";
		ret += "public static final double[][]  COLOR_I = "
				+ printSp(getProperty("Internode_color(Color_I)"), 2, 3)
				+ ";\n";
		ret += "// Female_color(Color_Ff)\n";
		ret += "public static final double[][]  COLOR_FF = "
				+ printSp(getProperty("Female_color(Color_Ff)"), 2, 3) + ";\n";
		ret += "// Male_color(Color_Fm)\n";
		ret += "public static final double[][]  COLOR_FM = "
				+ printSp(getProperty("Male_color(Color_Fm)"), 2, 3) + ";\n";
		ret += "// Phy_Color(Color_P)\n";
		ret += "public static final double[][]  COLOR_P = "
				+ printSp(getProperty("Phy_Color(Color_P)"), 6, 3) + ";\n";
		ret += "\n";
		ret += "// textures\n";
		ret += "// Leaf_texture_{alive, dead}\n";
		ret += "public static final String Leaf_texture_alive = \"" + getProperty("Leaf_texture_alive") + "\";\n";
		ret += "public static final String Leaf_texture_dead = \"" + getProperty("Leaf_texture_dead") + "\";\n";
		ret += "// Internode_texture_{alive, dead}\n";
		ret += "public static final String Internode_texture_alive = \"" + getProperty("Internode_texture_alive") + "\";\n";
		ret += "public static final String Internode_texture_dead = \"" + getProperty("Internode_texture_dead") + "\";\n";
		ret += "// Female_texture_{alive, dead}\n";
		ret += "public static final String Female_texture_alive = \""+ getProperty("Female_texture_alive") + "\";\n";
		ret += "public static final String Female_texture_dead = \""+ getProperty("Female_texture_dead") + "\";\n";
		ret += "// Male_texture_{alive, dead}\n";
		ret += "public static final String Male_texture_alive = \"" + getProperty("Male_texture_alive") + "\";\n";
		ret += "public static final String Male_texture_dead = \"" + getProperty("Male_texture_dead") + "\";\n";
		ret += "\n";
		ret += "// angles\n";
		ret += "// Angle_Male(Ang_Fm)\n";
		ret += "public static final double[][]  ANG_FM = "
				+ printAges(getProperty("Angle_Male(Ang_Fm)")) + ";\n";
		ret += "// Angle_Female(Ang_Ff)\n";
		ret += "public static final double[][]  ANG_FF = "
				+ printAges(getProperty("Angle_Female(Ang_Ff)")) + ";\n";
		ret += "// Angle_Blade(Ang_B)\n";
		ret += "public static final double[][]  ANG_B = "
				+ printAges(getProperty("Angle_Blade(Ang_B)")) + ";\n";
		ret += "// Angle_Axillary(Ang_A)\n";
		ret += "public static final double[][]  ANG_A = "
				+ printAges(getProperty("Angle_Axillary(Ang_A)")) + ";\n";
		ret += "// Angle_Phyllotaxy(Ang_Ph)\n";
		ret += "public static final double[]  ANG_PH = "
				+ printList(getProperty("Angle_Phyllotaxy(Ang_Ph)")) + ";\n";
		ret += "\n";
		ret += "// branch bending\n";
		ret += "// Young_Modulus(Ey)\n";
		ret += "public static final double[]  EY = "
				+ printList(getProperty("Young_Modulus(Ey)")) + ";\n";
		ret += "// Force_point(fp)\n";
		ret += "public static final double[]  FP = "
				+ printList(getProperty("Force_point(fp)")) + ";\n";
		ret += "// Terminal_Angle(Eng_Ang)\n";
		ret += "public static final double[]  ENG_ANG = "
				+ printList(getProperty("Terminal_Angle(Eng_Ang)")) + ";\n";
		ret += "// Righting_point(Eng_N)\n";
		ret += "public static final int[]  ENG_N = "
				+ printList(getProperty("Righting_point(Eng_N)")) + ";\n";
		ret += "// InitialMin_angle(InitMin)\n";
		ret += "public static final double[]  INITMIN = "
				+ printList(getProperty("InitialMin_angle(InitMin)")) + ";\n";
		ret += "// InitialMax_angle(InitMax)\n";
		ret += "public static final double[]  INITMAX = "
				+ printList(getProperty("InitialMax_angle(InitMax)")) + ";\n";
		ret += "// InitialNum_angle(InitNum)\n";
		ret += "public static final int[]  INITNUM = "
				+ printList(getProperty("InitialNum_angle(InitNum)")) + ";\n";
		ret += "//Flag_Bending_by_node\n";
		ret += "public static final boolean[]  FLAG_BENDING_BY_NODE = "
				+ printBoolList(getProperty("Flag_Bending_by_node")) + ";\n";
		ret += "\n";
		ret += "// Max_Angle(wb)\n";
		ret += "public static final double[]  WB = "
				+ printList(getProperty("Max_Angle(wb)")) + ";\n";
		ret += "// Openning_cycle(wbn)\n";
		ret += "public static final int[]  WBN = "
				+ printList(getProperty("Openning_cycle(wbn)")) + ";\n";
		ret += "\n";
		ret += "// Organ(Flag_organ_bending)\n";
		ret += "public static final boolean[]  FLAG_ORGAN_BENDING = "
				+ printBoolList(getProperty("Organ(Flag_organ_bending)"))
				+ ";\n";
		ret += "// Bending_branch(Theta_B)\n";
		ret += "public static final double[]  THETA_B = "
				+ printList(getProperty("Bending_branch(Theta_B)")) + ";\n";
		ret += "// Bending_leaf(Theta_L)\n";
		ret += "public static final double[]  THETA_L = "
				+ printList(getProperty("Bending_leaf(Theta_L)")) + ";\n";
		ret += "// Bending_fruit(Theta_F)\n";
		ret += "public static final double[]  THETA_F = "
				+ printList(getProperty("Bending_fruit(Theta_F)")) + ";\n";
		ret += "\n";
		ret += "// Flag_plagiotropic\n";
		ret += "public static final boolean[]  FLAG_PLAGIOTROPIC = "
				+ printBoolList(getProperty("Flag_plagiotropic")) + ";\n";
		ret += "// Leaf_direction(Flag)\n";
		ret += "public static final boolean[]  FLAG_LEAF_DIRECTION = "
				+ printBoolList(getProperty("Leaf_direction(Flag)")) + ";\n";
		ret += "\n";
		ret += "// Pruning(Flag_pruning)\n";
		ret += "public static final boolean[]  FLAG_PRUNING = "
				+ printBoolList(getProperty("Pruning(Flag_pruning)")) + ";\n";
		ret += "// Delay(Pruning_delay)\n";
		ret += "public static final int[]  PRUNING_DELAY = "
				+ printList(getProperty("Delay(Pruning_delay)")) + ";\n";
		ret += "// Leaf_display(Flag_leaf)\n";
		ret += "public static final boolean[]  FLAG_LEAF = "
				+ printBoolList(getProperty("Leaf_display(Flag_leaf)")) + ";\n";
		ret += "// Fruit_display(Flag_fruit)\n";
		ret += "public static final boolean[]  FLAG_FRUIT = "
				+ printBoolList(getProperty("Fruit_display(Flag_fruit)"))
				+ ";\n";
		ret += "\n";
		ret += "// Internode_Volume(VI)\n";
		ret += "public static final double[]  VI = "
				+ printList(getProperty("Internode_Volume(VI)")) + ";\n";
		ret += "// Blade_Volume(VB)\n";
		ret += "public static final double[]  VB = "
				+ printList(getProperty("Blade_Volume(VB)")) + ";\n";
		ret += "// Fruit_Volume(VF)\n";
		ret += "public static final double[]  VF = "
				+ printList(getProperty("Fruit_Volume(VF)")) + ";\n";
		ret += "// Internode_C(CI)\n";
		ret += "public static final double[]  CI = "
				+ printList(getProperty("Internode_C(CI)")) + ";\n";
		ret += "// Blade_C(CB)\n";
		ret += "public static final double[]  CB = "
				+ printList(getProperty("Blade_C(CB)")) + ";\n";
		ret += "// Fruit_C(CF)\n";
		ret += "public static final double[]  CF = "
				+ printList(getProperty("Fruit_C(CF)")) + ";\n";
		ret += "\n";
		ret += "// ENVIRONMENT\n";
		ret += "\n";
		ret += "// soil water\n";
		ret += "// Flag_climate\n";
		ret += "public static final boolean  FLAG_CLIMATE = "
				+ printBool(getProperty("Flag_climate")) + ";\n";
		ret += "// SoilWaterEffect(K1)\n";
		ret += "public static final double  E_K1 = "
				+ getProperty("SoilWaterEffect(K1)") + ";\n";
		ret += "// SoilWaterEffect(K2)\n";
		ret += "public static final double  E_K2 = "
				+ getProperty("SoilWaterEffect(K2)") + ";\n";
		ret += "// Soil_budget_para(C1)\n";
		ret += "public static final double  C1 = "
				+ getProperty("Soil_budget_para(C1)") + ";\n";
		ret += "// Soil_budget_para(C2)\n";
		ret += "public static final double  C2 = "
				+ getProperty("Soil_budget_para(C2)") + ";\n";
		ret += "// InitialSoilWater(QSW0)\n";
		ret += "public static final double  QSW0 = "
				+ getProperty("InitialSoilWater(QSW0)") + ";\n";
		ret += "// MaxPlantSoilWater(QSWMAX)\n";
		ret += "public static final double  QSWMAX = "
				+ getProperty("MaxPlantSoilWater(QSWMAX)") + ";\n";
		ret += "// MinPlantSoilWater(QSWMIN)\n";
		ret += "public static final double  QSWMIN = "
				+ getProperty("MinPlantSoilWater(QSWMIN)") + ";\n";
		ret += "// MaxEnvFSoilWater(QSW_MAX)\n";
		ret += "public static final double  QSW_MAX = "
				+ getProperty("MaxEnvFSoilWater(QSW_MAX)") + ";\n";
		ret += "// MinEnvFSoilWater(QSW_MIN)\n";
		ret += "public static final double  QSW_MIN = "
				+ getProperty("MinEnvFSoilWater(QSW_MIN)") + ";\n";
		ret += "\n";
		ret += "// temperature\n";
		ret += "// Flag_temperature\n";
		ret += "public static final boolean  FLAG_TEMPERATURE = "
				+ printBool(getProperty("Flag_temperature")) + ";\n";
		ret += "// SumTEffect(Flag_sumt)\n";
		ret += "public static final boolean  FLAG_SUMT = "
				+ printBool(getProperty("SumTEffect(Flag_sumt)")) + ";\n";
		ret += "// Temperature(E_ALPHA)\n";
		ret += "public static final double  E_ALPHA = "
				+ getProperty("Temperature(E_ALPHA)") + ";\n";
		ret += "// Temperature(E_BETA)\n";
		ret += "public static final double  E_BETA = "
				+ getProperty("Temperature(E_BETA)") + ";\n";
		ret += "// SumTEffect(KSUMT)\n";
		ret += "public static final double  KSUMT = "
				+ getProperty("SumTEffect(KSUMT)") + ";\n";
		ret += "// SumTEffect(THETA_BASE)\n";
		ret += "public static final double  THETA_BASE = "
				+ getProperty("SumTEffect(THETA_BASE)") + ";\n";
		ret += "// MaxPlantTemp(THETAMAX)\n";
		ret += "public static final double  THETAMAX = "
				+ getProperty("MaxPlantTemp(THETAMAX)") + ";\n";
		ret += "// MinPlantTemp(THETAMIN)\n";
		ret += "public static final double  THETAMIN = "
				+ getProperty("MinPlantTemp(THETAMIN)") + ";\n";
		ret += "// MaxEnvFTemp(THETA_MAX)\n";
		ret += "public static final double  THETA_MAX = "
				+ getProperty("MaxEnvFTemp(THETA_MAX)") + ";\n";
		ret += "// MinEnvFTemp(THETA_MIN)\n";
		ret += "public static final double  THETA_MIN = "
				+ getProperty("MinEnvFTemp(THETA_MIN)") + ";\n";
		ret += "\n";
		ret += "// light\n";
		ret += "// Flag_light\n";
		ret += "public static final boolean  FLAG_LIGHT = "
				+ printBool(getProperty("Flag_light")) + ";\n";
		ret += "// Light(E_A)\n";
		ret += "public static final double  E_A = " + getProperty("Light(E_A)")
				+ ";\n";
		ret += "// Light(E_B)\n";
		ret += "public static final double  E_B = " + getProperty("Light(E_B)")
				+ ";\n";
		ret += "// MaxEnvFLight(LIGHTMAX)\n";
		ret += "public static final double  LIGHTMAX = "
				+ getProperty("MaxEnvFLight(LIGHTMAX)") + ";\n";
		ret += "// MinEnvFLight(LIGHTMIN)\n";
		ret += "public static final double  LIGHTMIN = "
				+ getProperty("MinEnvFLight(LIGHTMIN)") + ";\n";
		ret += "// MaxEnvFLight(LIGHT_MAX)\n";
		ret += "public static final double  LIGHT_MAX = "
				+ getProperty("MaxEnvFLight(LIGHT_MAX)") + ";\n";
		ret += "// MinEnvFLight(LIGHT_MIN)\n";
		ret += "public static final double  LIGHT_MIN = "
				+ getProperty("MinEnvFLight(LIGHT_MIN)") + ";\n";
		ret += "// PotentialCondition(E0)\n";
		ret += "public static final double  E0 = "
				+ getProperty("PotentialCondition(E0)") + ";\n";
		ret += "\n";
		ret += "// OUTPUT OPTIONS\n";
		ret += "\n";
		ret += "// data\n";
		ret += "// Flag_topo_disp\n";
		ret += "public static final boolean[]  FLAG_TOPO_DISP = "
				+ printBoolList(getProperty("Flag_topo_disp")) + ";\n";
		ret += "// Flag_topo_disp_total\n";
		ret += "public static final boolean  FLAG_TOPO_DISP_S = "
				+ printBool(getProperty("Flag_topo_disp_total")) + ";\n";
		ret += "// Flag_topo_disp_living\n";
		ret += "public static final boolean[]  FLAG_TOPO_DISP_L = "
				+ printBoolList(getProperty("Flag_topo_disp_living")) + ";\n";
		ret += "// Flag_topo_disp_living_aboveGU\n";
		ret += "public static final boolean[]  FLAG_TOPO_DISP_A = "
				+ printBoolList(getProperty("Flag_topo_disp_living_aboveGU"))
				+ ";\n";
		ret += "// Flag_hydro\n";
		ret += "public static final boolean  FLAG_HYDRO = "
				+ printBool(getProperty("Flag_hydro")) + ";\n";
		ret += "// Flag_demand_biomass_layer\n";
		ret += "public static final boolean  FLAG_LAYER_DISP = "
				+ printBool(getProperty("Flag_demand_biomass_layer")) + ";\n";
		ret += "// Flag_demand_biomass_plant\n";
		ret += "public static final boolean  FLAG_DEMAND_BIOMASS = "
				+ printBool(getProperty("Flag_demand_biomass_plant")) + ";\n";
		ret += "// Flag_biomass_repartition\n";
		ret += "public static final boolean[]  FLAG_BIOMASS_NUM = "
				+ printBoolList(getProperty("Flag_biomass_repartition"))
				+ ";\n";
		ret += "\n";
		ret += "// charts\n";
		ret += "// Flag_biomass_fig\n";
		ret += "public static final boolean  FLAG_BIOMASS_FIG = "
				+ printBool(getProperty("Flag_biomass_fig")) + ";\n";
		ret += "// Flag_biomassdemand_fig\n";
		ret += "public static final boolean  FLAG_BIOPROD_FIG = "
				+ printBool(getProperty("Flag_biomassdemand_fig")) + ";\n";
		ret += "// Flag_biomass_fig_accumulated\n";
		ret += "public static final boolean  FLAG_BIOMASS_FIG_A = "
				+ printBool(getProperty("Flag_biomass_fig_accumulated"))
				+ ";\n";
		ret += "// Flag_disp_LAI\n";
		ret += "public static final boolean  FLAG_DISP_LAI = "
				+ printBool(getProperty("Flag_disp_LAI")) + ";\n";
		ret += "// Flag_organ_biomass_fig_phy\n";
		ret += "public static final boolean[]  FLAG_BIOMASS_FIG_PHY = "
				+ printBoolList(getProperty("Flag_organ_biomass_fig_phy"))
				+ ";\n";
		ret += "// Flag_organ_size_fig_phy\n";
		ret += "public static final boolean[]  FLAG_SIZE_FIG_PHY = "
				+ printBoolList(getProperty("Flag_organ_size_fig_phy")) + ";\n";
		ret += "\n";
		ret += "// 3D\n";
		ret += "// Flag_geometry\n";
		ret += "public static final boolean  FLAG_GEOMETRY = "
				+ printBool(getProperty("Flag_geometry")) + ";\n";
		ret += "// Flag_geo_full\n";
		ret += "public static final boolean  FLAG_GEO_FULL = "
				+ printBool(getProperty("Flag_geo_full")) + ";\n";
		ret += "// Flag_geo_3D\n";
		ret += "public static final boolean  FLAG_GEO_3D = "
				+ printBool(getProperty("Flag_geo_3D")) + ";\n";
		ret += "// Flag_geo_leaf\n";
		ret += "public static final boolean  FLAG_GEO_LEAF = "
				+ printBool(getProperty("Flag_geo_leaf")) + ";\n";
		ret += "// Flag_geo_fruit\n";
		ret += "public static final boolean  FLAG_GEO_FRUIT = "
				+ printBool(getProperty("Flag_geo_fruit")) + ";\n";
		ret += "// Flag_geo_skeleton\n";
		ret += "public static final boolean[]  FLAG_GEO_SKELETON = "
				+ printBoolList(getProperty("Flag_geo_skeleton")) + ";\n";
		ret += "// Flag_geo_lig\n";
		ret += "public static final boolean  FLAG_GEO_LIG = "
				+ printBool(getProperty("Flag_geo_lig")) + ";\n";
		ret += "// Flag_window\n";
		ret += "public static final boolean  FLAG_WINDOWS = "
				+ printBool(getProperty("Flag_windows")) + ";\n";

		return ret;
	}

	private String preSolveRgg(String str) {
		String ret = "";
		String[] sp = str.split("\\s+");
		if (sp.length == GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE
				* GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE) {
			for (int i = 0; i < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; i++) {
				for (int j = i; j < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; j++) {
					ret += sp[i * GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE + j]
							+ " ";
				}
			}
		} else {
			ret = str;
		}
		return ret.replaceAll("\\s+$", "").replaceAll("^\\s+", "");
	}

	private String preSolveRgg(String str, int c, int l) {
		String ret = "";
		String[] sp = str.split("\\s+");
		for (int i = c * l; i < (c + 1) * l; i++) {
			ret += sp[i] + " ";
		}
		return ret.replaceAll("\\s+$", "").replaceAll("^\\s+", "");
	}

	@SuppressWarnings("unchecked")
	public void reloadRgg(HashMap<?, ?> hm) {
		setProperty("Phy_Age(Maxp)", preSolveRgg(hm.get("MAXP").toString()));
		setProperty("Sample_Size(Tr)", preSolveRgg(hm.get("TR").toString()));
		setProperty("Axillary_Num(Nu_A)", preSolveRgg(hm.get("NU_A").toString()));
		setProperty("Leaf_Num(Nu_B)", preSolveRgg(hm.get("NU_B").toString()));
		setProperty("Female_Num(Nu_Ff)", preSolveRgg(hm.get("NU_FF").toString()));
		setProperty("Male_Num(Nu_Fm)", preSolveRgg(hm.get("NU_FM").toString()));
		setProperty("Micro_Num(Nu_I)", preSolveRgg(hm.get("NU_I").toString()));
		setProperty("Flag_Nu_Ma_version", preSolveRgg(hm.get("FLAG_NU_MA").toString()));
		setProperty("Macro_Num(Nu_Ma)", preSolveRgg(hm.get("NU_MA").toString()));
		setProperty("Struc_Jump(st_j)", preSolveRgg(hm.get("ST_J").toString()));
		setProperty("Reiter_Order(b_o)", preSolveRgg(hm.get("B_O").toString()));
		setProperty("Bran_Control(br_a)", preSolveRgg(hm.get("BR_A").toString()));
		setProperty("Reiter_Control(re_a)", preSolveRgg(hm.get("RE_A").toString()));
		setProperty("Rest_Func(rs_A)", preSolveRgg(hm.get("RS_A").toString()));
		setProperty("Rest_Func(rs_B)", preSolveRgg(hm.get("RS_B").toString()));
		setProperty("rythm_Ratio(rt_a)", preSolveRgg(hm.get("RT_A").toString()));
		setProperty("branching_proba(a)", preSolveRgg(hm.get("A").toString()));
		setProperty("Growth_macro(b)", preSolveRgg(hm.get("B").toString()));
		setProperty("Growth_micro(bu)", preSolveRgg(hm.get("BU").toString()));
		setProperty("Survive(c)", preSolveRgg(hm.get("C").toString()));
		setProperty("Time_Expansion(t_exp)", preSolveRgg(hm.get("T_EXP_CONST").toString()));
		setProperty("Flag_Time_function_version", preSolveRgg(hm.get("FLAG_TIME_FUNCTION").toString()));
		setProperty("Flag_Time_expansion_version", preSolveRgg(hm.get("FLAG_TIME_EXPANSION").toString()));
		setProperty("Para_b(Bt_b)", preSolveRgg(hm.get("BT_P").toString()));
		setProperty("Portion_Remobilization(P_Remob)", preSolveRgg(hm.get("P_REMOB").toString()));
		setProperty("Speed_Remobilization(V_Remob)", preSolveRgg(hm.get("V_REMOB").toString()));
		setProperty("Allom_Internode(a_bI)", preSolveRgg(hm.get("A_BI").toString()));
		setProperty("(a_aI)", preSolveRgg(hm.get("A_AI").toString()));
		setProperty("Allom_Petiole(a_bP)", preSolveRgg(hm.get("A_BP").toString()));
		setProperty("(a_aP)", preSolveRgg(hm.get("A_AP").toString()));
		setProperty("Allom_Blade(a_bB)", preSolveRgg(hm.get("A_BB").toString()));
		setProperty("(a_aB)", preSolveRgg(hm.get("A_AB").toString()));
		setProperty("Allom_Thick(Th_B)", preSolveRgg(hm.get("TH_B").toString()));
		setProperty("Female_Density(Den_Ff)", preSolveRgg(hm.get("DEN_FF").toString()));
		setProperty("Male_Density(Den_Fm)", preSolveRgg(hm.get("DEN_FM").toString()));
		setProperty("Internode_Dens(Den_I)", preSolveRgg(hm.get("DEN_I").toString()));
		setProperty("Number_Primordia(N_PM)", preSolveRgg(hm.get("N_PM").toString()));
		setProperty("Sink_Primordia(S_PM)", preSolveRgg(hm.get("S_PM").toString()));
		setProperty("Delay_Branch(Cp_B)", preSolveRgg(hm.get("CP_B").toString()));
		setProperty("Flag_field", preSolveRgg(hm.get("FLAG_FIELD").toString()));
		setProperty("Resist_Blade(r_B)", preSolveRgg(hm.get("R_B").toString()));
		setProperty("Resist_Petiole(r_P)", preSolveRgg(hm.get("R_P").toString()));
		setProperty("Resist_Root(r_R)", preSolveRgg(hm.get("R_R").toString()));
		setProperty("Projected_area(Sp)", preSolveRgg(hm.get("SP").toString()));
		setProperty("Beer_law(rp)", preSolveRgg(hm.get("RP").toString()));
		setProperty("Beer_law(kp)", preSolveRgg(hm.get("KP").toString()));
		setProperty("Seed_Biomass(Q0)", preSolveRgg(hm.get("Q0").toString()));
		setProperty("Flow_Biomass(dQ0)", preSolveRgg(hm.get("DQ0").toString()));
		setProperty("Flag_TotalLayerDemand_Mode", preSolveRgg(hm.get("FLAG_TOTAL_LAYER_DEMAND").toString()));
		setProperty("Global_Sink_Layer(Slay)", preSolveRgg(hm.get("SLAY").toString()));
		setProperty("Global_Sink_Coefficient(Slope)", preSolveRgg(hm.get("SLOPE").toString()));
		setProperty("Layer_repartition_Coefficient(Lamda)",	preSolveRgg(hm.get("COFF_L").toString()));
		setProperty("Expansion_Mode", preSolveRgg(hm.get("EXPANSION_MODE").toString()));
		setProperty("Root_Demand_Mode", preSolveRgg(hm.get("ROOT_DEMAND_MODE").toString()));
		setProperty("ShortInter_Number(N_SI)", preSolveRgg(hm.get("N_SI").toString()));
		setProperty("ShortInter_RelaSink(kp_SI)", preSolveRgg(hm.get("KP_SI").toString()));
		setProperty("Leaf_shape(smb_L)", preSolveRgg(hm.get("LEAF_OBJECT").toString()));
		setProperty("Female_shape(smb_Ff)", preSolveRgg(hm.get("FEMALE_FRUIT_OBJECT").toString()));
		setProperty("Male_shape(smb_Fm)", preSolveRgg(hm.get("MALE_FRUIT_OBJECT").toString()));
		setProperty("Leaf_color(Color_B)", preSolveRgg(hm.get("COLOR_B").toString()));
		setProperty("Internode_color(Color_I)", preSolveRgg(hm.get("COLOR_I").toString()));
		setProperty("Female_color(Color_Ff)", preSolveRgg(hm.get("COLOR_FF").toString()));
		setProperty("Male_color(Color_Fm)", preSolveRgg(hm.get("COLOR_FM").toString()));
		setProperty("Phy_Color(Color_P)", preSolveRgg(hm.get("COLOR_P").toString()));
		setProperty("Leaf_texture_alive", preSolveRgg(hm.get("Leaf_texture_alive").toString()));
		setProperty("Internode_texture_alive", preSolveRgg(hm.get("Internode_texture_alive").toString()));
		setProperty("Female_texture_alive", preSolveRgg(hm.get("Female_texture_alive").toString()));
		setProperty("Male_texture_alive", preSolveRgg(hm.get("Male_texture_alive").toString()));
		setProperty("Leaf_texture_dead", preSolveRgg(hm.get("Leaf_texture_dead").toString()));
		setProperty("Internode_texture_dead", preSolveRgg(hm.get("Internode_texture_dead").toString()));
		setProperty("Female_texture_dead", preSolveRgg(hm.get("Female_texture_dead").toString()));
		setProperty("Male_texture_dead", preSolveRgg(hm.get("Male_texture_dead").toString()));
		setProperty("Angle_Male(Ang_Fm)", preSolveRgg(hm.get("ANG_FM").toString()));
		setProperty("Angle_Female(Ang_Ff)", preSolveRgg(hm.get("ANG_FF").toString()));
		setProperty("Angle_Blade(Ang_B)", preSolveRgg(hm.get("ANG_B").toString()));
		setProperty("Angle_Axillary(Ang_A)", preSolveRgg(hm.get("ANG_A").toString()));
		setProperty("Angle_Phyllotaxy(Ang_Ph)", preSolveRgg(hm.get("ANG_PH").toString()));
		setProperty("Young_Modulus(Ey)", preSolveRgg(hm.get("EY").toString()));
		setProperty("Force_point(fp)", preSolveRgg(hm.get("FP").toString()));
		setProperty("Terminal_Angle(Eng_Ang)", preSolveRgg(hm.get("ENG_ANG").toString()));
		setProperty("Righting_point(Eng_N)", preSolveRgg(hm.get("ENG_N").toString()));
		setProperty("InitialMin_angle(InitMin)", preSolveRgg(hm.get("INITMIN").toString()));
		setProperty("InitialMax_angle(InitMax)", preSolveRgg(hm.get("INITMAX").toString()));
		setProperty("InitialNum_angle(InitNum)", preSolveRgg(hm.get("INITNUM").toString()));
		setProperty("Flag_Bending_by_node",	preSolveRgg(hm.get("FLAG_BENDING_BY_NODE").toString()));
		setProperty("Max_Angle(wb)", preSolveRgg(hm.get("WB").toString()));
		setProperty("Openning_cycle(wbn)", preSolveRgg(hm.get("WBN").toString()));
		setProperty("Organ(Flag_organ_bending)", preSolveRgg(hm.get("FLAG_ORGAN_BENDING").toString()));
		setProperty("Bending_branch(Theta_B)", preSolveRgg(hm.get("THETA_B").toString()));
		setProperty("Bending_leaf(Theta_L)", preSolveRgg(hm.get("THETA_L").toString()));
		setProperty("Bending_fruit(Theta_F)", preSolveRgg(hm.get("THETA_F").toString()));
		setProperty("Flag_plagiotropic", preSolveRgg(hm.get("FLAG_PLAGIOTROPIC").toString()));
		setProperty("Leaf_direction(Flag)",	preSolveRgg(hm.get("FLAG_LEAF_DIRECTION").toString()));
		setProperty("Pruning(Flag_pruning)", preSolveRgg(hm.get("FLAG_PRUNING").toString()));
		setProperty("Delay(Pruning_delay)", preSolveRgg(hm.get("PRUNING_DELAY").toString()));
		setProperty("Leaf_display(Flag_leaf)", preSolveRgg(hm.get("FLAG_LEAF").toString()));
		setProperty("Fruit_display(Flag_fruit)", preSolveRgg(hm.get("FLAG_FRUIT").toString()));
		setProperty("Internode_Volume(VI)",	preSolveRgg(hm.get("VI").toString()));
		setProperty("Blade_Volume(VB)", preSolveRgg(hm.get("VB").toString()));
		setProperty("Fruit_Volume(VF)", preSolveRgg(hm.get("VF").toString()));
		setProperty("Internode_C(CI)", preSolveRgg(hm.get("CI").toString()));
		setProperty("Blade_C(CB)", preSolveRgg(hm.get("CB").toString()));
		setProperty("Fruit_C(CF)", preSolveRgg(hm.get("CF").toString()));
		setProperty("Flag_climate", preSolveRgg(hm.get("FLAG_CLIMATE").toString()));
		setProperty("SoilWaterEffect(K1)", preSolveRgg(hm.get("E_K1").toString()));
		setProperty("SoilWaterEffect(K2)", preSolveRgg(hm.get("E_K2").toString()));
		setProperty("Soil_budget_para(C1)", preSolveRgg(hm.get("C1").toString()));
		setProperty("Soil_budget_para(C2)",	preSolveRgg(hm.get("C2").toString()));
		setProperty("InitialSoilWater(QSW0)", preSolveRgg(hm.get("QSW0").toString()));
		setProperty("MaxPlantSoilWater(QSWMAX)", preSolveRgg(hm.get("QSWMAX").toString()));
		setProperty("MinPlantSoilWater(QSWMIN)", preSolveRgg(hm.get("QSWMIN").toString()));
		setProperty("MaxEnvFSoilWater(QSW_MAX)", preSolveRgg(hm.get("QSW_MAX").toString()));
		setProperty("MinEnvFSoilWater(QSW_MIN)", preSolveRgg(hm.get("QSW_MIN").toString()));
		setProperty("Flag_temperature", preSolveRgg(hm.get("FLAG_TEMPERATURE").toString()));
		setProperty("SumTEffect(Flag_sumt)", preSolveRgg(hm.get("FLAG_SUMT").toString()));
		setProperty("Temperature(E_ALPHA)", preSolveRgg(hm.get("E_ALPHA").toString()));
		setProperty("Temperature(E_BETA)", preSolveRgg(hm.get("E_BETA").toString()));
		setProperty("SumTEffect(KSUMT)", preSolveRgg(hm.get("KSUMT").toString()));
		setProperty("SumTEffect(THETA_BASE)", preSolveRgg(hm.get("THETA_BASE").toString()));
		setProperty("MaxPlantTemp(THETAMAX)", preSolveRgg(hm.get("THETAMAX").toString()));
		setProperty("MinPlantTemp(THETAMIN)", preSolveRgg(hm.get("THETAMIN").toString()));
		setProperty("MaxEnvFTemp(THETA_MAX)", preSolveRgg(hm.get("THETA_MAX").toString()));
		setProperty("MinEnvFTemp(THETA_MIN)", preSolveRgg(hm.get("THETA_MIN").toString()));
		setProperty("Flag_light", preSolveRgg(hm.get("FLAG_LIGHT").toString()));
		setProperty("Light(E_A)", preSolveRgg(hm.get("E_A").toString()));
		setProperty("Light(E_B)", preSolveRgg(hm.get("E_B").toString()));
		setProperty("MaxEnvFLight(LIGHTMAX)", preSolveRgg(hm.get("LIGHTMAX").toString()));
		setProperty("MinEnvFLight(LIGHTMIN)", preSolveRgg(hm.get("LIGHTMIN").toString()));
		setProperty("MaxEnvFLight(LIGHT_MAX)", preSolveRgg(hm.get("LIGHT_MAX").toString()));
		setProperty("MinEnvFLight(LIGHT_MIN)", preSolveRgg(hm.get("LIGHT_MIN").toString()));
		setProperty("PotentialCondition(E0)", preSolveRgg(hm.get("E0").toString()));
		setProperty("Flag_topo_disp", preSolveRgg(hm.get("FLAG_TOPO_DISP").toString()));
		setProperty("Flag_topo_disp_total",	preSolveRgg(hm.get("FLAG_TOPO_DISP_S").toString()));
		setProperty("Flag_topo_disp_living", preSolveRgg(hm.get("FLAG_TOPO_DISP_L").toString()));
		setProperty("Flag_topo_disp_living_aboveGU", preSolveRgg(hm.get("FLAG_TOPO_DISP_A").toString()));
		setProperty("Flag_hydro", preSolveRgg(hm.get("FLAG_HYDRO").toString()));
		setProperty("Flag_demand_biomass_layer", preSolveRgg(hm.get("FLAG_LAYER_DISP").toString()));
		setProperty("Flag_demand_biomass_plant", preSolveRgg(hm.get("FLAG_DEMAND_BIOMASS").toString()));
		setProperty("Flag_biomass_repartition", preSolveRgg(hm.get("FLAG_BIOMASS_NUM").toString()));
		setProperty("Flag_biomass_fig", preSolveRgg(hm.get("FLAG_BIOMASS_FIG").toString()));
		setProperty("Flag_biomassdemand_fig", preSolveRgg(hm.get("FLAG_BIOPROD_FIG").toString()));
		setProperty("Flag_biomass_fig_accumulated", preSolveRgg(hm.get("FLAG_BIOMASS_FIG_A").toString()));
		setProperty("Flag_disp_LAI", preSolveRgg(hm.get("FLAG_DISP_LAI").toString()));
		setProperty("Flag_organ_biomass_fig_phy", preSolveRgg(hm.get("FLAG_BIOMASS_FIG_PHY").toString()));
		setProperty("Flag_organ_size_fig_phy", preSolveRgg(hm.get("FLAG_SIZE_FIG_PHY").toString()));
		setProperty("Flag_geometry", preSolveRgg(hm.get("FLAG_GEOMETRY").toString()));
		setProperty("Flag_geo_full", preSolveRgg(hm.get("FLAG_GEO_FULL").toString()));
		setProperty("Flag_geo_3D", preSolveRgg(hm.get("FLAG_GEO_3D").toString()));
		setProperty("Flag_geo_leaf", preSolveRgg(hm.get("FLAG_GEO_LEAF").toString()));
		setProperty("Flag_geo_fruit", preSolveRgg(hm.get("FLAG_GEO_FRUIT").toString()));
		setProperty("Flag_geo_skeleton", preSolveRgg(hm.get("FLAG_GEO_SKELETON").toString()));
		setProperty("Flag_geo_lig", preSolveRgg(hm.get("FLAG_GEO_LIG").toString()));
		setProperty("Flag_windows", preSolveRgg(hm.get("FLAG_WINDOWS").toString()));
		setProperty("Para_a(Bt_a)", preSolveRgg(hm.get("BT_N").toString()));
		setProperty("Chr_Age(N)", preSolveRgg(hm.get("N").toString()));

		for (int age = 1; age <= GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; age++) {
			setProperty("Time_Function(Tu_O(1))__" + age,
					preSolveRgg(hm.get("TU_O_PA" + age).toString(), 0, 8));
			setProperty("Time_appearance(2)__" + age,
					preSolveRgg(hm.get("TU_O_PA" + age).toString(), 1, 8));
			setProperty("Time_disappearance(3)__" + age,
					preSolveRgg(hm.get("TU_O_PA" + age).toString(), 2, 8));
			setProperty("Time_busy(4)__" + age,
					preSolveRgg(hm.get("TU_O_PA" + age).toString(), 3, 8));
			setProperty("Time_idle(5)__" + age,
					preSolveRgg(hm.get("TU_O_PA" + age).toString(), 4, 8));
		}

		for (int age = 1; age < GreenLabAtributes.MAX_PHYSIOLOGICAL_AGE; age++) {
			setProperty("Blade__" + age,
					preSolveRgg(hm.get("T_FUNC_VAR_PA" + age).toString(), 0, 4));
			setProperty("Petiole__" + age,
					preSolveRgg(hm.get("T_FUNC_VAR_PA" + age).toString(), 1, 4));
			setProperty("Internode__" + age,
					preSolveRgg(hm.get("T_FUNC_VAR_PA" + age).toString(), 2, 4));
			setProperty("Femalefruit__" + age,
					preSolveRgg(hm.get("T_FUNC_VAR_PA" + age).toString(), 3, 4));
			setProperty("Malefruit__" + age,
					preSolveRgg(hm.get("T_FUNC_VAR_PA" + age).toString(), 4, 4));
			setProperty("Root__" + age,
					preSolveRgg(hm.get("T_FUNC_VAR_PA" + age).toString(), 5, 4));
		}
		{
			int age = 6;
			setProperty("Blade__" + age,
					preSolveRgg(hm.get("T_EXP_VAR").toString(), 0, 4));
			setProperty("Petiole__" + age,
					preSolveRgg(hm.get("T_EXP_VAR").toString(), 1, 4));
			setProperty("Internode__" + age,
					preSolveRgg(hm.get("T_EXP_VAR").toString(), 2, 4));
			setProperty("Femalefruit__" + age,
					preSolveRgg(hm.get("T_EXP_VAR").toString(), 3, 4));
			setProperty("Malefruit__" + age,
					preSolveRgg(hm.get("T_EXP_VAR").toString(), 4, 4));
			setProperty("Root__" + age,
					preSolveRgg(hm.get("T_EXP_VAR").toString(), 5, 4));
		}
		{
			setProperty("Sink_Blade(S_B)",
					preSolveRgg(hm.get("S_O").toString(), 0, 6));
			setProperty("Sink_Petiole(S_P)",
					preSolveRgg(hm.get("S_O").toString(), 1, 6));
			setProperty("Sink_Internode(S_I)",
					preSolveRgg(hm.get("S_O").toString(), 2, 6));
			setProperty("Sink_Female(S_Ff)",
					preSolveRgg(hm.get("S_O").toString(), 3, 6));
			setProperty("Sink_Male(S_Fm)",
					preSolveRgg(hm.get("S_O").toString(), 4, 6));
			setProperty("Sink_Layer(S_L)",
					preSolveRgg(hm.get("S_O").toString(), 5, 6));
			setProperty("Sink_Root(S_R)",
					preSolveRgg(hm.get("S_O").toString(), 6, 6));
		}
		backup = (HashMap<String, String>) properties.clone();
	}
}
