/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab.ui.elements;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;

import de.grogra.greenlab.GreenLabGUI;

/**
 * The JCheckBox for boolean Values
 * 
 * @author Cong Ding
 * 
 */
public class GreenLabBooleanCheckBox extends JCheckBox implements FocusListener {

	private static final long serialVersionUID = -5304051964100605282L;

	private String keyString = "defaultTooltip";

	public GreenLabBooleanCheckBox(String keyString) {
		super();
		this.setText("");
		this.keyString = keyString;
		addFocusListener(this);

		final String key = this.keyString;
		addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				GreenLabGUI.tooltipPanel.updateValue(key);
			}
		});
	}

	public void setTextValue(String i) {
		if (i.equals("1")) {
			this.setSelected(true);
		} else {
			this.setSelected(false);
		}
	}

	public String getTextValue() {
		if (this.isSelected()) {
			return "1";
		} else {
			return "0";
		}
	}

	public boolean getBoolean() {
		return this.isSelected();
	}

	@Override
	public void focusGained(FocusEvent arg0) {
		GreenLabGUI.tooltipPanel.updateValue(keyString);
	}

	@Override
	public void focusLost(FocusEvent arg0) {
		// GreenLabGUI.tooltipPanel.updateValue("emptyTooltip");
	}
}
