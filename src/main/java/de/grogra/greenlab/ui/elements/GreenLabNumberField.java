/*
 * Copyright (C) 2012 GroIMP Developer Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.greenlab.ui.elements;

import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

import de.grogra.greenlab.GreenLabGUI;
import de.grogra.greenlab.conf.GreenLabAtributes;

/**
 * 
 * Autor : Cong Ding Date: 14.02.2012, Michael Henke Date: 21.06.2004 Time:
 * 03:36:05
 * 
 */
public class GreenLabNumberField extends JTextField implements KeyListener,
		FocusListener {

	private static final long serialVersionUID = -4166975218962521347L;

	private static final int INT_SIZE = 4;
	private static final int DOUBLE_SIZE = 6;
	private static final double doubleStepSize = 0.1;
	private static final int intStepSize = 1;
	private static final String intStr = "integer";

	private double doubleMin = -Double.MAX_VALUE;
	private double doubleMax = Double.MAX_VALUE;
	private int intMin = Integer.MIN_VALUE;
	private int intMax = Integer.MAX_VALUE;

	private boolean isInt = false;
	private boolean checking = false;

	private String keyString = "defaultTooltip";

	public GreenLabNumberField(String keyString) {
		super(
				GreenLabGUI.getString(keyString + ".type").equals(intStr) ? INT_SIZE
						: DOUBLE_SIZE);
		init(keyString);
	}

	public GreenLabNumberField(String keyString, boolean NULL) {
		super(
				GreenLabGUI.getString(keyString + ".type").equals(intStr) ? INT_SIZE
						: DOUBLE_SIZE);
		init(keyString);
	}

	private void init(String keyString) {
		this.keyString = keyString;
		String type = GreenLabGUI.getString(keyString + ".type");
		if (type.equals("integer")) {
			isInt = true;
		} else {
			isInt = false;
		}
		String minString = GreenLabGUI.getString(keyString + ".range.min");
		String maxString = GreenLabGUI.getString(keyString + ".range.max");
		if (isInt) {
			if (!minString.equals("MIN")) {
				try {
					this.intMin = (int) NumberFormat.getInstance(Locale.US)
							.parse(minString).doubleValue();
				} catch (ParseException e1) {
				}
			}
			if (!maxString.equals("MAX")) {
				try {
					this.intMax = (int) NumberFormat.getInstance(Locale.US)
							.parse(maxString).doubleValue();
				} catch (ParseException e1) {
				}
			}
		} else {
			if (!minString.equals("MIN")) {
				try {
					this.doubleMin = NumberFormat.getInstance(Locale.US)
							.parse(minString).doubleValue();
				} catch (ParseException e1) {
				}
			}
			if (!maxString.equals("MAX")) {
				try {
					this.doubleMax = NumberFormat.getInstance(Locale.US)
							.parse(maxString).doubleValue();
				} catch (ParseException e1) {
				}
			}
		}
		addKeyListener(this);
		addFocusListener(this);

		final String key = this.keyString;
		addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				GreenLabGUI.tooltipPanel.updateValue(key);
			}
		});
	}

	public String getTextValue() {
		return getText();
	}

	public double getValue() {
		double retVal = 0;
		try {
			retVal = NumberFormat.getInstance(Locale.US).parse(getText()).doubleValue();
		} catch (NumberFormatException e) {
		} catch (ParseException e) {
		}
		return retVal;
	}

	public void setText(String text) {
		setValue(text);
	}

	public void setTextValue(String text) {
		super.setText(text);
	}

	public void setValue(double value) {
		DecimalFormat df = new DecimalFormat("0.0");
		df.setMaximumFractionDigits(GreenLabAtributes.DOUBLE_DIGITS);
		df.setMinimumFractionDigits(0);
		super.setText(df.format(value).toString().replaceAll(",", "."));
	}

	public void setValue(int value) {
		DecimalFormat df = new DecimalFormat("0.0");
		df.setMaximumFractionDigits(GreenLabAtributes.DOUBLE_DIGITS);
		df.setMinimumFractionDigits(0);
		super.setText(df.format(value).toString());
	}

	public void setValue(String value) {		
		if (isInt) {
			int retVal = 0;
			try {
				retVal = Integer.parseInt(value);
			} catch (NumberFormatException e) {
			}
			setValue(retVal);
		} else {
			double retVal = 0;
			try {
				retVal = NumberFormat.getInstance(Locale.US).parse(value).doubleValue();
			} catch (NumberFormatException e) {
			} catch (ParseException e) {
			}
			setValue(retVal);
		}
	}

	private void inc(double delta) {
		setValue(getValue() + delta);
	}

	private void dec(double delta) {
		setValue(getValue() - delta);
	}

	private void inc(int delta) {
		setValue(getValue() + delta);
	}

	private void dec(int delta) {
		setValue(getValue() - delta);
	}

	protected Document createDefaultModel() {
		return new DoubleNumberDocument();
	}

	protected class DoubleNumberDocument extends PlainDocument {

		private static final long serialVersionUID = 1L;

		public void insertString(int offs, String str, AttributeSet a)
				throws BadLocationException {
			char[] source = str.toCharArray();
			char[] result = new char[source.length];
			int j = 0;
			for (int i = 0; i < result.length; i++) {
				if (Character.isDigit(source[i])
						|| (source[i] == '-' && offs == 0 && getTextValue()
								.indexOf("-") < 0)
						|| (!isInt && source[i] == '.' && getTextValue()
								.indexOf(".") < 0)) {
					result[j++] = source[i];
				} else {
					Toolkit.getDefaultToolkit().beep();
				}
			}

			super.insertString(offs, new String(result, 0, j), a);
			if (!checking)
				checkBounds();
		}

		private void checkBounds() {
			checking = true;
			if (isInt) {
				if (getValue() > intMax) {
					setValue(intMax);
				}
				if (getValue() < intMin) {
					setValue(intMin);
				}
			} else {
				/*
				 * if (getTextValue().endsWith(".")) { setValue(getTextValue());
				 * setTextValue(getTextValue() + "."); } else {
				 * setValue(getTextValue()); }
				 */
				if (getValue() > doubleMax) {
					setValue(doubleMax);
				}
				if (getValue() < doubleMin) {
					setValue(doubleMin);
				}
			}
			checking = false;
		}
	}

	@Override
	public void keyPressed(KeyEvent event) {
		if (isInt) {
			switch (event.getKeyCode()) {
			case KeyEvent.VK_UP: {
				inc(intStepSize);
				break;
			}
			case KeyEvent.VK_DOWN: {
				dec(intStepSize);
				break;
			}
			case KeyEvent.VK_PAGE_UP: {
				inc(10 * intStepSize);
				break;
			}
			case KeyEvent.VK_PAGE_DOWN: {
				dec(10 * intStepSize);
				break;
			}
			}
		} else {
			switch (event.getKeyCode()) {
			case KeyEvent.VK_UP: {
				inc(doubleStepSize);
				break;
			}
			case KeyEvent.VK_DOWN: {
				dec(doubleStepSize);
				break;
			}
			case KeyEvent.VK_PAGE_UP: {
				inc(10 * doubleStepSize);
				break;
			}
			case KeyEvent.VK_PAGE_DOWN: {
				dec(10 * doubleStepSize);
				break;
			}
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent event) {
	}

	@Override
	public void keyTyped(KeyEvent event) {
		GreenLabGUI.tooltipPanel.updateValue(keyString);
	}

	@Override
	public void focusGained(FocusEvent arg0) {
		GreenLabGUI.tooltipPanel.updateValue(keyString);
	}

	@Override
	public void focusLost(FocusEvent arg0) {
		// GreenLabGUI.tooltipPanel.updateValue("emptyTooltip");
	}

}
